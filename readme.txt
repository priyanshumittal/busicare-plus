=== BusiCare Plus ===

Contributors: spicethemes
Requires at least: 4.5
Tested up to: 6.7.1
Stable tag: 1.3.3
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhance BusiCare WordPress Themes functionality.

== Description ==

BusiCare WordPress Theme is lightweight, elegant, fully responsive and translation-ready theme that allows you to create stunning blogs and websites. The theme is well suited for companies, law firms,ecommerce, finance, agency, travel, photography, recipes, design, arts, personal and any other creative websites and blogs. Theme is developed using Bootstrap 4 framework. It comes with a predesigned home page, good looking header designs and number of content sections that you can easily customize. It also has lots of customization options (banner, services, testimonial, etc) that will help you create a beautiful, unique website in no time. BusiCare is  compatible with popular plugins like WPML, Polylang, Woo-commerce  and Conact Form 7. BusiCare theme comes with various popular locales.(DEMO: https://demo.spicethemes.com/?theme=BusiCare%20Pro)


== Changelog ==

@Version 1.3.3
* Updated freemius directory.

@Version 1.3.2
* Updated font-awesome library and freemius directory.

@Version 1.3.1
* Added Rank Math,Seo Yoast and NavXT Breadcrumbs Feature.

@Version 1.3
* Updated freemius directory.

@Version 1.2.3
* Added google font locally feature.

@Version 1.2.2
* Added Sidebar Layout settings in customizer for blog/archive, single post, and page.

@Version 1.2.1.1
* Updated Freemius snippet.

@Version 1.2.1
* Updated Freemius product slug.

@Version 1.2
* Added freemius directory.

@Version 1.1.2
* Fixed Preloader & removed unnecessary code.

@Version 1.1.1
* Fixed the issues with PHP 8.

@Version 1.1
* Added One Click Demo Import feature.
* Updated Option's page link, style and strings.
* Fixed some style issues.

@Version 1.0
* Fixed data migration issues lite to plus.
* Updated options page link and strings.
* Fixed some style isssues.

@Version 0.2
* Fixed customizer issue with WordPress 5.8.

@Version 0.1
* Initial Release

== External resources ==

Owl Carousel:
Copyright: (c) David Deutsch
License: MIT license
Source: https://cdnjs.com/libraries/OwlCarousel2/2.2.1

Alpha color picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Repeater Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT license
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-repeater

Custom control - Image Radio Button Custom Control:
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

== Images ==

* Image used in Slider, License CC0 Public Domain
1. https://stocksnap.io/photo/woman-working-8LHTBAZW32
2. https://pxhere.com/en/photo/1046491
3. https://pxhere.com/en/photo/1273017

* Image used in CTA 2, License CC0 Public Domain
https://pxhere.com/en/photo/1565417

* Image used in Project, License CC0 Public Domain
1. https://pxhere.com/en/photo/1629588
2. https://pxhere.com/en/photo/1076173
3. https://pxhere.com/en/photo/1449173

* Image used in Funfact, License CC0 Public Domain
https://pxhere.com/en/photo/1548825

* Image used in Testimonial, License CC0 Public Domain
1. https://pxhere.com/en/photo/1419926
2. https://pxhere.com/en/photo/642874
3. https://pxhere.com/en/photo/1602577
4. https://pxhere.com/en/photo/1177664

* Image used in Team, License CC0 Public Domain
1. https://pxhere.com/en/photo/645506
2. https://pxhere.com/en/photo/1435981
3. https://pxhere.com/en/photo/527825
4. https://pxhere.com/en/photo/1052708

* Images on /images folder
Copyright (C) 2021-2025, spicethemes and available as [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html)
