<?php
error_reporting(0);

// Register and load the widget
function busicare_plus_header_topbar_info_widget() {
    register_widget('busicare_plus_header_topbar_info_widget');
}

add_action('widgets_init', 'busicare_plus_header_topbar_info_widget');

// Creating the widget
class busicare_plus_header_topbar_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'busicare_header_topbar_info_widget', // Base ID
                __('BusiCare: Header info widget', 'busicare-plus'), // Widget Name
                array(
                    'classname' => 'busicare_plus_header_topbar_info_widget',
                    'phone_number' => __('Topbar header info widget.', 'busicare-plus'),
                ),
                array(
                    'width' => 600,
                )
        );
    }

    public function widget($args, $instance) {

        //echo $args['before_widget']; 

        echo $args['before_widget'];
        ?>
        <ul class="head-contact-info">
            <li>
                <?php if (!empty($instance['busicare_phone_icon'])) { ?>
                    <i class="fa <?php echo $instance['busicare_phone_icon']; ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-phone"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['phone_number'])) {
                    echo $instance['phone_number'];
                } else {
                    echo $instance['phone_number'];
                }
                ?></li>
            <li>
                <?php if (!empty($instance['busicare_email_icon'])) { ?>
                    <i class="fa <?php echo $instance['busicare_email_icon']; ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-envelope"></i>
                <?php } ?>	
                <a href="mailto:<?php echo esc_attr($instance['busicare_email_id']);?>"> <?php
                if (!empty($instance['busicare_email_id'])) {
                    echo $instance['busicare_email_id'];
                }
                ?></a></li> 
        </ul>
                    <?php
                    echo $args['after_widget'];
                }

                // Widget Backend
                public function form($instance) {

                    if (isset($instance['busicare_phone_icon'])) {
                        $busicare_phone_icon = $instance['busicare_phone_icon'];
                    } else {
                        $busicare_phone_icon = 'fa fa-phone';
                    }

                    if (isset($instance['phone_number'])) {
                        $phone_number = $instance['phone_number'];
                    } else {
                        $phone_number = __('+15 718-999-3939', 'busicare-plus');
                    }

                    if (isset($instance['busicare_email_icon'])) {
                        $busicare_email_icon = $instance['busicare_email_icon'];
                    } else {
                        $busicare_email_icon = 'fa-solid fa-envelope';
                    }

                    if (isset($instance['busicare_email_id'])) {
                        $busicare_email_id = $instance['busicare_email_id'];
                    } else {
                        $busicare_email_id = __('support@busicare.com', 'busicare-plus');
                    }

                    // Widget admin form
                    ?>

        <label for="<?php echo $this->get_field_id('busicare_phone_icon'); ?>"><?php _e('Font Awesome icon', 'busicare-plus'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('busicare_phone_icon'); ?>" name="<?php echo $this->get_field_name('busicare_phone_icon'); ?>" type="text" value="<?php if ($busicare_phone_icon) echo esc_attr($busicare_phone_icon);
        else echo 'fa fa-phone'; ?>" />
        <span><?php _e('Link to get Font Awesome icons', 'busicare-plus'); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php _e('fa-icon', 'busicare-plus'); ?></a></span>
        <br><br>

        <label for="<?php echo $this->get_field_id('phone_number'); ?>"><?php _e('Phone', 'busicare-plus'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('phone_number'); ?>" name="<?php echo $this->get_field_name('phone_number'); ?>" type="text" value="<?php if ($phone_number) echo esc_attr($phone_number);
        else _e('+15 718-999-3939', 'busicare-plus'); ?>" /><br><br>

        <label for="<?php echo $this->get_field_id('busicare_email_icon'); ?>"><?php _e('Font Awesome icon', 'busicare-plus'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('busicare_email_icon'); ?>" name="<?php echo $this->get_field_name('busicare_email_icon'); ?>" type="text" value="<?php if ($busicare_email_icon) echo esc_attr($busicare_email_icon);
        else echo 'fa fa-phone'; ?>" />
        <span><?php _e('Link to get Font Awesome icons', 'busicare-plus'); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php _e('fa-icon', 'busicare-plus'); ?></a></span><br><br>

        <label for="<?php echo $this->get_field_id('busicare_email_id'); ?>"><?php _e('Email', 'busicare-plus'); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id('busicare_email_id'); ?>" name="<?php echo $this->get_field_name('busicare_email_id'); ?>" type="text" value="<?php if ($busicare_email_id) echo esc_attr($busicare_email_id);
        else _e('support@busicare.com', 'busicare-plus'); ?>" /><br><br>



        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {

        $instance = array();
        $instance['busicare_phone_icon'] = (!empty($new_instance['busicare_phone_icon']) ) ? strip_tags($new_instance['busicare_phone_icon']) : '';
        $instance['phone_number'] = (!empty($new_instance['phone_number']) ) ? $new_instance['phone_number'] : '';
        $instance['busicare_email_icon'] = (!empty($new_instance['busicare_email_icon']) ) ? strip_tags($new_instance['busicare_email_icon']) : '';
        $instance['busicare_email_id'] = (!empty($new_instance['busicare_email_id']) ) ? $new_instance['busicare_email_id'] : '';

        return $instance;
    }

}
