<?php

add_action('widgets_init', 'busicare_plus_widgets_init');

function busicare_plus_widgets_init() {

    /* sidebar */

    register_sidebar( array(
        'name' => esc_html__( 'Footer Bar 2', 'busicare-plus' ),
        'id' => 'footer-bar-2',
        'description' => esc_html__( 'Footer Bar 2', 'busicare-plus' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    // Header Social Icon Sidebar
    register_sidebar(array(
        'name' => esc_html__('Top header sidebar left area', 'busicare-plus'),
        'id' => 'home-header-sidebar_left',
        'description' => esc_html__('Social media menu lateral area', 'busicare-plus'),
        'before_widget' => '<aside id="%1$s" class="widget right-widgets %2$s">',
        'after_widget' => '</aside>',
    ));

    // Subscribe Sidebar
    register_sidebar(array(
        'name' => esc_html__('Top header sidebar Right area', 'busicare-plus'),
        'id' => 'home-header-sidebar_right',
        'description' => esc_html__('Subscriber section widget area', 'busicare-plus'),
        'before_widget' => '<aside id="%1$s" class="widget left-widgets %2$s">',
        'after_widget' => '</aside>',
    ));
    //register Menu sidebar
    register_sidebar( array(
    'name' => esc_html__('After Menu widget area', 'busicare-plus' ),
    'id' => 'menu-widget-area',
    'description' => esc_html__( 'After Menu widget area', 'busicare-plus' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
    ) );

    //register Slider Widgets
    register_sidebar( array(
    'name' => esc_html__('Slider widgets', 'busicare-plus' ),
    'id' => 'slider-widget-area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
    ) );

}
