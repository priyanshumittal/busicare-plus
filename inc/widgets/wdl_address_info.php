<?php

// Register and load the widget

function busicare_plus_address_info_widget() {
    register_widget('busicare_plus_address_info_widget');
}

add_action('widgets_init', 'busicare_plus_address_info_widget');

// Creating the widget
class busicare_plus_address_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'busicare_plus_address_info_widget', // Base ID
                __('BusiCare: Address Info', 'busicare-plus'), // Widget Name
                array(
                    'classname' => 'busicare_plus_address_info_widget',
                    'description' => __('BusiCare Address Info widget', 'busicare-plus'),
                ),
                array(
                    'width' => 300,
                )
        );
    }

    public function widget($args, $instance) {
        echo $args['before_widget'];
        ?>
        <?php if (!empty($instance['social_add_info'])) { ?>
            <em class="address-info"><i class="fa-solid fa-location-dot"></i><?php echo $instance['social_add_info']; ?></em>
        <?php } ?>

        <?php
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance) {

        if (isset($instance['social_add_info'])) {
            $social_add_info = $instance['social_add_info'];
        } else {
            $social_add_info = '1010 New York,NY 10018 US';
        }
        // Widget admin form
        ?>
        <!-- address -->
        <h4 for="<?php echo $this->get_field_id('social_add_info'); ?>"><?php _e('Address', 'busicare-plus'); ?></h4>
        <textarea class="widefat" id="<?php echo $this->get_field_id('social_add_info'); ?>" name="<?php echo $this->get_field_name('social_add_info'); ?>" type="text" ><?php if ($social_add_info) echo esc_attr($social_add_info); ?></textarea> 
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {

        $instance = array();

        $instance['social_add_info'] = (!empty($new_instance['social_add_info']) ) ? strip_tags($new_instance['social_add_info']) : '';
        return $instance;
    }

}
