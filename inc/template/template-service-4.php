<?php 
/**
 * Template Name: Service Four
 */
get_header();?>
<!-- Service Section -->
<?php if ( $post->post_content!=="" ) { ?>
	<section class="section-module service section-space-page">
		<div class="container<?php echo busicare_container();?>">
			<div class="row">
				<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
				</div>	
			</div>
		</div>
	</section>
<?php } ?>
<div class="clearfix"></div>
<!-- /End of Service Section -->

<?php 
$services_enable = get_theme_mod('services_enable',true);
$service_testimonial_enable = get_theme_mod('service_testimonial_enable',true);
$service_cta2_enable = get_theme_mod('service_cta2_enable',true);
$service_client_enable = get_theme_mod('service_client_enable',true); ?>

<?php if($services_enable == true) { 
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/service-content.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php if($service_cta2_enable == true) { 
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/cta2-content.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php if($service_client_enable == true) {
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');	
?>
<?php } ?>
<div class="clearfix"></div>

<?php get_footer();?>