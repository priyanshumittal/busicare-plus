<?php
/**
 * Template Name: Contact Three
 */
get_header();
if ($post->post_content !== "") { ?>
    <!-- Contact Section -->
    <section class="section-module contact section-space-page">
        <div class="container<?php echo busicare_container();?>">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <?php
                    the_post();
                    the_content();
                    ?>			
                </div>	
            </div>
        </div>
    </section>
    <!-- /End of Contact Section -->
<?php } ?>

<?php
$contact_cf7_title = get_theme_mod('contact_cf7_title', __('Get In Touch', 'busicare-plus'));
$contact_info_title = get_theme_mod('contact_info_title', __('Get In Touch With Us', 'busicare-plus'));
$map_heading = get_theme_mod('contact_page_map_heading', __('Find Us On The Map', 'busicare-plus'));
$form_heading = get_theme_mod('contact_page_form_heading', __('Drop Us A Line', 'busicare-plus'));
$contact_client_enable = get_theme_mod('contact_client_enable', true);
$contact_template_viewmore_btn_link_target = get_theme_mod('contact_template_viewmore_btn_link_target');

$contact_data = get_theme_mod('busicare_contact_content');
if (empty($contact_data)) {
    $contact_data =busicare_plus_starter_contact_content_json();
}

$busicare_social_links = get_theme_mod('busicare_social_links');
if (empty($busicare_social_links)) {
    $busicare_social_links = busicare_plus_starter_contact_social_json();
}
?>

<!-- Contact Details Section -->
<section class="section-space contact-info bg-default">
    <div class="container<?php echo busicare_container();?>">
        <div class="row">
            <?php
            $contact_data = json_decode($contact_data);
            if (!empty($contact_data)) {
                foreach ($contact_data as $contact_item) {

                    $contact_title = !empty($contact_item->title) ? apply_filters('busicare_translate_single_string', $contact_item->title, 'Contact section') : '';
                    $contact_text = !empty($contact_item->text) ? apply_filters('busicare_translate_single_string', $contact_item->text, 'Contact section') : '';
                    $contact_icon = !empty($contact_item->icon_value) ? apply_filters('busicare_translate_single_string', $contact_item->icon_value, 'Contact section') : '';
            ?>
            <div class="col-md-4">
                 <div class="contact-detail-area">
                       <span><i class="fa <?php echo $contact_icon; ?>"></i></span>
                       <h5><?php echo $contact_title; ?></h5>
                      <address><p class="detail-contact-field"><?php echo $contact_text; ?></p></address>
                 </div>
            </div>
            <?php
              }
            } ?>            
        </div>
    </div>
</section>
<!-- /End of Contact Info Section -->

<!-- Contact Details Section -->
<section class="section-space-page contact">
    <div class="container<?php echo busicare_container();?>">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="contant-form">
                    <?php if($contact_cf7_title != ''){ ?><div class="title"><h4><?php echo $contact_cf7_title; ?></h4></div><?php } ?>
                    <?php
                    if (get_theme_mod('contact_form_shortcode')) {
                        echo do_shortcode(get_theme_mod('contact_form_shortcode'));
                    }
                    ?>  

                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End of Contact Details Section -->

<!--Contact map section-->
<section class="contact-form-map">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div id="google-map">
                    <!--<iframe src="https://snazzymaps.com/embed/9272" width="100%" height="500px" frameborder="0" style="border:none;"></iframe>-->
                    <?php echo do_shortcode(get_theme_mod('contact_google_map_shortcode')); ?>
                </div>
            </div>

        </div>
    </div>
</section>
<!--/Contact map section-->

<?php
#sponsors section
if ($contact_client_enable == true) {
    include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}

get_footer();
?>