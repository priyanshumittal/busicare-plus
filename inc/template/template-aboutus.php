<?php 
/**
 * Template Name: About Us
 */
get_header();
if ( $post->post_content!=="" ) { ?>
<section class="section-module about section-space-page">
	<div class="container<?php echo busicare_container();?>">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
			</div>	
		</div>
	</div>
</section>
<?php } ?>
<div class="clearfix"></div>

<?php 
$about_testimonial_enable = get_theme_mod('about_testimonial_enable',true);
$about_team_enable = get_theme_mod('about_team_enable',true);
$about_client_enable = get_theme_mod('about_client_enable',true); ?>

<?php if($about_testimonial_enable == true) { 
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/testimonial-content.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php if($about_team_enable == true) { 
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/team-content1.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php if($about_client_enable == true) {
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
?>
	
<?php } ?>
<div class="clearfix"></div>
<?php get_footer();?>