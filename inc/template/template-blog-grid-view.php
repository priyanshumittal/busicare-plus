<?php
/**
 * Template Name: Blog grid view
 */
get_header();
$blog_sponsors_enable = get_theme_mod('blog_sponsors_enable', true);
$blog_temp_type = get_theme_mod('busicare_blog_content', 'excerpt');
?>
<section class="section-module blog section-space-page grid-view <?php
if ($blog_temp_type == 'content') {
    echo 'blog-grid-view-full';
}
?>">
    <div class="container<?php echo busicare_blog_post_container();?>">
        <?php if (get_theme_mod('post_nav_style_setting', 'pagination') == 'pagination') { ?>
            <div class="row">
                <?php
                if (get_query_var('paged')) {
                    $paged = get_query_var('paged');
                } elseif (get_query_var('page')) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $args = array('post_type' => 'post', 'paged' => $paged);
                $loop = new WP_Query($args);
                if ($loop->have_posts()):
                    while ($loop->have_posts()): $loop->the_post();
                        include(BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-blog-grid-view-content.php');
                    endwhile;
                endif;
                ?>
            </div>
            <?php
            // pagination function
            echo '<div class="row justify-content-center">';
            $obj = new busicare_plus_pagination();
            $obj->busicare_plus_page($loop);
            echo '</div>';
        } else {
            echo do_shortcode('[ajax_posts]');
        }
        ?>
    </div>
</section>
<?php
if ($blog_sponsors_enable == true) {
    include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}
get_footer();
?>