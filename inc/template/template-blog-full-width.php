<?php
/**
 * Template Name: Blog Full Width
 */
get_header();
$blog_sponsors_enable = get_theme_mod('blog_sponsors_enable', true);
?>
<section class="section-module blog section-space-page">	
    <div class="container<?php echo busicare_blog_post_container();?>">
        <div class="row">	
            <div class="col-lg-12 col-md-12 col-sm-12 standard-view">
                <div class="blog">
                    <?php
                    if (get_theme_mod('post_nav_style_setting', 'pagination') == "pagination") {
                        if (get_query_var('paged')) {
                            $paged = get_query_var('paged');
                        } elseif (get_query_var('page')) {
                            $paged = get_query_var('page');
                        } else {
                            $paged = 1;
                        }
                        $args = array('post_type' => 'post', 'paged' => $paged);
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()):
                            while ($loop->have_posts()): $loop->the_post();
                                include(BUSICAREP_PLUGIN_DIR.'/inc/template-parts/content-blog-template.php');
                            endwhile;
                        else:
                            include(BUSICAREP_PLUGIN_DIR.'/inc/template-parts/content-blog-template-none.php');
                        endif;
                        // pagination function
                        echo '<div class="row justify-content-center">';
                        $obj = new busicare_plus_pagination();
                        $obj->busicare_plus_page($loop);
                        echo '</div>';
                    }
                    if (get_theme_mod('post_nav_style_setting', 'pagination') != "pagination") {
                        echo do_shortcode('[ajax_posts]');
                    }
                    ?>
                </div>
            </div>
        </div>			
    </div>
</section>
<?php
if ($blog_sponsors_enable == true) {
    include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}
get_footer();
?>