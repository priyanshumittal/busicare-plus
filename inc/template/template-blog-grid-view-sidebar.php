<?php
/**
 * Template Name: Blog grid view sidebar
 */
get_header();
$blog_sponsors_enable = get_theme_mod('blog_sponsors_enable', true);
?>
<?php $exc_length = get_theme_mod('busicare_blog_content', 'excerpt'); ?>

<section class="section-module blog section-space-page grid-view <?php if ($exc_length != 'excerpt') {
    echo 'full-width';
} ?>">
    <div class="container<?php echo busicare_blog_post_container();?>">
        <div class="row">			
            <div class="col-lg-8 col-md-7 col-sm-12 ">
                    <?php if (get_theme_mod('post_nav_style_setting', 'pagination') == 'pagination') { ?>	
                    <div class="row">
                        <?php
                        if (get_query_var('paged')) {
                            $paged = get_query_var('paged');
                        } elseif (get_query_var('page')) {
                            $paged = get_query_var('page');
                        } else {
                            $paged = 1;
                        }
                        $args = array('post_type' => 'post', 'paged' => $paged);
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()):
                            while ($loop->have_posts()): $loop->the_post();
                                include(BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-blog-grid-content.php');
                            endwhile;
                        endif;
                        ?>
                    </div>
                    <?php
                    // pagination function
                    echo '<div class="row justify-content-center">';
                    $obj = new busicare_plus_pagination();
                    $obj->busicare_plus_page($loop);
                    echo '</div>';
                } else {
                    echo
                    do_shortcode('[ajax_posts]');
                }
                ?> 
            </div>	
            <!--Sidebar Widgets-->
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="sidebar s-l-space">
<?php dynamic_sidebar('sidebar-1'); ?>	
                </div>
            </div>
            <!--/Sidebar Widgets-->	
        </div>

    </div>
</section>
<?php
if ($blog_sponsors_enable == true) {
   include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}
get_footer();
?>