<?php 
/**
 * Template Name: Four Column Gallery Style Portfolio
 */
get_header();?>
<!-- Portfolio Section -->
<?php include_once(BUSICAREP_PLUGIN_DIR.'/inc/template-parts/content-portfolio-gallery.php');?>
<!-- /Portfolio Section -->
<?php get_footer(); ?>