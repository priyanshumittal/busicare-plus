<?php 
/**
 * Template Name: Team Grid Style Four
*/
get_header();?>
<?php if ( $post->post_content!=="" ) { ?>
<section class="page-section-space about-section">
	<div class="container<?php echo busicare_container();?>">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
			</div>	
		</div>
	</div>
</section>
<?php } 
if(get_theme_mod('team_enable',true) == true){ 
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/team-content4.php');
} 

if(get_theme_mod('testimonial_client_enable',true) == true) {
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');	
}

get_footer();?>