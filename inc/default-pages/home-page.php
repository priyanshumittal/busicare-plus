<?php
	//post status and options
	$busicare_plus_post = array(
		  'comment_status' => 'closed',
		  'ping_status' =>  'closed' ,
		  'post_author' => 1,
		  'post_date' => date('Y-m-d H:i:s'),
		  'post_name' => 'Home',
		  'post_status' => 'publish' ,
		  'post_title' => 'Home',
		  'post_type' => 'page',
	);  
	//insert page and save the id
	$busicare_plus_newvalue = wp_insert_post( $busicare_plus_post, false );
	if ( $busicare_plus_newvalue && ! is_wp_error( $busicare_plus_newvalue ) ){
		update_post_meta( $busicare_plus_newvalue, '_wp_page_template', 'template-business.php' );
		
		// Use a static front page
		$busicare_plus_page = get_page_by_title('Home');
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $busicare_plus_page->ID );
		
	}
?>