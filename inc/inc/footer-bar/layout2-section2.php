<div class="col-lg-6 col-md-6 col-sm-6 right-info">
<?php
$foot_section_2 = get_theme_mod('footer_bar_sec2','none');
switch ( $foot_section_2 )
      {
        case 'none':
        break;

        case 'footer_menu':
        echo busicare_plus_footer_bar_menu();
        break;

        case 'custom_text':
        echo get_theme_mod('footer_copyright_2','<span class="copyright">'.__( 'Copyright 2020 <a href="#"> Spicethemes</a> All right reserved', 'busicare-plus' ).'</span>');
        break;

        case 'widget':
        busicare_plus_footer_widget_area('footer-bar-2');
        break;
      }
?>
</div>	