<div class="col-lg-6 col-md-6 col-sm-6 left">
<?php
 $foot_section_1 = get_theme_mod('footer_bar_sec1','custom_text');
switch ( $foot_section_1 )
      {
        case 'none':
        break;

        case 'footer_menu':
        echo busicare_plus_footer_bar_menu();
        break;

        case 'custom_text':
        echo get_theme_mod('footer_copyright','<span class="copyright">'.__( 'Copyright 2020 <a href="#"> Spicethemes</a> All right reserved', 'busicare-plus' ).'</span>');
        break;

        case 'widget':
        busicare_plus_footer_widget_area('footer-bar-1');
        break;
      }
?>
</div>	