<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package BusiCare
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses busicare_header_style()
 */
function busicare_plus_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'busicare_custom_header_args', array(
		'default-image'          => BUSICAREP_PLUGIN_URL.'/assets/images/blog/breadcrum.jpg',
		
		'width'                  => 1903,
		'height'                 => 350,
		'flex-height'            => true,

	) ) );
}
add_action( 'after_setup_theme', 'busicare_plus_custom_header_setup' );

