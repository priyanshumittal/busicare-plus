<div class="<?php if(get_theme_mod('sticky_header_clr_enable',false)==true):?>clr <?php endif;?><?php if(get_theme_mod('sticky_header_enable',false)===true):?>header-sticky<?php endif;?> <?php if(get_theme_mod('sticky_header_animation','')=='shrink'): echo 'shrink'; endif;?> <?php echo get_theme_mod('header_logo_placing','left');?>">
  <div class="header-logo index5">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
  				<?php the_custom_logo(); do_action('busicare_sticky_header_logo');?>
				<?php if((get_option('blogname')!='') || (get_option('blogdescription')!='')):?>
					<div class="custom-logo-link-url"> 
						<?php if(get_option('blogname')!=''):?>
			    			<h2 class="site-title"><a class="site-title-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
			    			</h2>
			    		<?php endif;

						$description = get_bloginfo( 'description', 'display' );
						if(get_option('blogdescription')!='')
						{
						if ( $description || is_customize_preview() ) : ?>
							<p class="site-description"><?php echo esc_html($description); ?></p>
						<?php endif; 
						}?>
					</div>
				<?php endif;?>
  			</div>
  		</div>
  	</div>
  </div>
	
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light navbar5">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarNavDropdown">

				<!-- Right Nav -->
				<?php include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/menu-search.php');?>
				
			</div>
			
		</div>
	</nav>
</div>
	<!--/End of Navbar -->
 <!--/End Header section-->