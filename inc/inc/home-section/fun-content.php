<!--Funfact Section-->
<?php
$home_fun_title = get_theme_mod('home_fun_section_title', __('<span>Doing the right thing,</span><br> at the right time.', 'busicare-plus'));
$funfact_data = get_theme_mod('busicare_funfact_content');
if (empty($funfact_data)) {
    $funfact_data = json_encode(array(
        array(
            'title' => '4050',
            'text' => esc_html__('Satisfied Clients', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b56',
        ),
        array(
            'title' => '150',
            'text' => esc_html__('Finish Projects', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b66',
        ),
        array(
            'title' => '90%',
            'text' => esc_html__('Business Growth', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b86',
        ),
        array(
            'title' => '27',
            'text' => esc_html__('Consultants', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b87',
        ),
    ));
}

$fun_callout_background = get_theme_mod('funfact_callout_background', BUSICAREP_PLUGIN_URL .'inc/images/bg/funfact-bg.jpg');
if ($fun_callout_background != '') {
    ?>
    <section class="section-space funfact bg-default"  style="background-image:url('<?php echo esc_url($fun_callout_background); ?>'); background-repeat: no-repeat; width: 100%; background-size: cover;">
    <?php
    } else {
        ?>
        <section class="section-space funfact">
            <?php
        }
        ?>
            
            <?php 
            $funfact_overlay_section_color = get_theme_mod('funfact_overlay_section_color', 'rgba(0, 11, 24, 0.8)');
$funfact_image_overlay = get_theme_mod('funfact_image_overlay', true);
            if($funfact_image_overlay != false) 
        { ?>
                        <div class="overlay" style="background-color: <?php echo $funfact_overlay_section_color; ?>"></div>
                <?php } ?>
        <!--<div class="overlay"></div>-->
        <div class="busicare-fun-container container">
            <div class="row section-header funfact-section">
                <?php if (!empty($home_fun_title)): ?>
                <div class="funfact-icon">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                </div>
                 <?php endif; ?>


                <div class="funfact-block">	
                    <?php if (!empty($home_fun_title)): ?>
                        <div class="funfact-text">
                            <!--<h2 class="title"><span style="color:#22a2c4">Doing the right thing,<br></span> at the right time.</h2>-->
                            <h2 class="title"><?php echo $home_fun_title; ?></h2>
                        </div>
                        <?php
                    endif;

                    $funfact_data = json_decode($funfact_data);
                    if (!empty($funfact_data)) {


                        foreach ($funfact_data as $funfact_iteam) {
                            $funfact_title = !empty($funfact_iteam->title) ? apply_filters('busicare_translate_single_string', $funfact_iteam->title, 'Funfact section') : '';
                            $funfact_text = !empty($funfact_iteam->text) ? apply_filters('busicare_translate_single_string', $funfact_iteam->text, 'Funfact section') : '';
                            ?>
                            <div class="funfact-inner counter">
                                <?php if($funfact_title != ''){ ?>
                                <h2 class="funfact-title counter-value text-white" data-count="<?php echo $funfact_title; ?>"><?php echo $funfact_title; ?></h2>
                                <?php } ?>
                                <?php if($funfact_text != ''){ ?>
                                <p class="description text-white"><?php echo $funfact_text; ?></p>
                                <?php } ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>	 
        </div>
    </section>
    <!--/Funfact Section-->