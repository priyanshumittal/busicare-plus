<?php 
	$home_cta2_title = get_theme_mod('home_cta2_title', __('<span>Easy & Simple</span> - No Coding Required!','busicare-plus'));
	$home_cta2_desc = get_theme_mod('home_cta2_desc',__('It is a long established fact that a reader will be distracted by the readable content of a page when looking <br> at its layout. The point of using Lorem ipsum dolor sit amet elit.','busicare-plus'));
	$home_cta2_btn1_text = get_theme_mod('home_cta2_btn1_text',__('Purchase Now','busicare-plus'));
	$home_cta2_btn2_text = get_theme_mod('home_cta2_btn2_text',__('Get In Touch','busicare-plus'));
	$home_cta2_btn1_link  = get_theme_mod('home_cta2_btn1_link','#');
	$home_cta2_btn2_link  = get_theme_mod('home_cta2_btn2_link','#');
	$home_cta2_btn1_link_target = get_theme_mod('home_cta2_btn1_link_target',false);
	$home_cta2_btn2_link_target = get_theme_mod('home_cta2_btn2_link_target',false);
	?>	
	<!--Call to Action-->
	<?php $callout_cta2_background = get_theme_mod('cta2_background',BUSICAREP_PLUGIN_URL .'inc/images/bg/bg-img.jpg'); 	
 	if($callout_cta2_background != '') 
 	{ ?>
		<section class="section-space cta-2"  style="background-image:url('<?php echo esc_url($callout_cta2_background);?>'); background-repeat: no-repeat; background-position: top left; width: 100%;
		    background-size: cover;">
			<?php 
	} 
	else 
	{ ?>
		<section class="section-space cta-2">
		<?php 
	} 
			$cta2_overlay_section_color = get_theme_mod('cta2_overlay_section_color','rgba(0, 11, 24, 0.80)');
			$cta2_image_overlay = get_theme_mod('cta2_image_overlay',true);
			?>
		    <?php if($cta2_image_overlay != false) 
		    { ?>
				<div class="overlay" style="background-color:<?php echo $cta2_overlay_section_color; ?>"></div>
			<?php } ?>
			<?php
			if(!empty($home_cta2_title) || (!empty($home_cta2_desc)) || (!empty($home_cta2_btn1_text)) || (!empty($home_cta2_btn1_text))  || (!empty($home_cta2_btn2_text)) ):?>
			<div class=" busicare-cta2-container container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section-header cta-block text-center">
							<?php if(!empty($home_cta2_title)):?><h2 class="title cta-2-title"><?php echo $home_cta2_title; ?></h2><?php endif;?>
							<?php if(!empty($home_cta2_desc)):?><p><?php echo $home_cta2_desc;?></p><?php endif;?>
							<?php if($home_cta2_btn1_text!='' || $home_cta2_btn2_text!='')
								{ ?>
                                                        <div class="cta-btn">	
								<?php if($home_cta2_btn1_text != ''){ ?>
									<a class="btn-small btn-default"  href="<?php if($home_cta2_btn1_link !='' ) {echo $home_cta2_btn1_link;} ?>"  <?php if($home_cta2_btn1_link_target== true) { echo "target='_blank'"; }  ?>><?php echo $home_cta2_btn1_text; ?></a>
                                                                <?php } if($home_cta2_btn2_text != ''){  ?>
									<a class="btn-small btn-light" href="<?php if($home_cta2_btn2_link !='' ) {echo $home_cta2_btn2_link;} ?>" <?php if($home_cta2_btn2_link_target== true) { echo "target='_blank'"; }  ?>><?php echo $home_cta2_btn2_text; ?></a>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
					</div>					
				</div>
			</div>
		<?php endif;?>
		</section>