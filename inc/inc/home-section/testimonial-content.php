<?php
if (get_page_template_slug() == 'template-service.php' || get_page_template_slug() == 'template-testimonial-1.php') {
    busicare_plus_testimonial_design('testimonial-carousel', 'carousel');
} elseif (get_page_template_slug() == 'template-service.php' || get_page_template_slug() == 'template-testimonial-4.php') {
    busicare_plus_testimonial_design('testimonial-carousel', 'grid');
} elseif (get_page_template_slug() == 'template-service-2.php' || get_page_template_slug() == 'template-testimonial-2.php') {
    busicare_plus_testimonial_design('testimonial-carousel2', 'carousel');
} elseif (get_page_template_slug() == 'template-service-2.php' || get_page_template_slug() == 'template-testimonial-5.php') {
    busicare_plus_testimonial_design('testimonial-carousel2', 'grid');
} elseif (get_page_template_slug() == 'template-service-3.php' || get_page_template_slug() == 'template-testimonial-3.php') {
    busicare_plus_testimonial_design('testimonial-carousel4', 'carousel');
} elseif (get_page_template_slug() == 'template-service-3.php' || get_page_template_slug() == 'template-testimonial-6.php') {
    busicare_plus_testimonial_design('testimonial-carousel4', 'grid');
}  elseif (get_theme_mod('home_testimonial_design_layout', 1) == 1) {
    busicare_plus_testimonial_design('testimonial-carousel', 'carousel');
} elseif (get_theme_mod('home_testimonial_design_layout', 1) == 2) {
    busicare_plus_testimonial_design('testimonial-carousel2', 'carousel');
} elseif (get_theme_mod('home_testimonial_design_layout', 1) == 3) {
    busicare_plus_testimonial_design('testimonial-carousel4', 'carousel');
} 
?>