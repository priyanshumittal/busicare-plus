<?php
$team_options = get_theme_mod('busicare_team_content');
if (empty($team_options)) {
    $team_options = busicare_plus_starter_team_json();
}
$team_animation_speed = get_theme_mod('team_animation_speed', 3000);
$team_smooth_speed = get_theme_mod('team_smooth_speed', 1000);
$team_nav_style = get_theme_mod('team_nav_style', 'bullets');
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$teamsettings = array('teamcarouselid' => '#team-carousel', 'team_animation_speed' => $team_animation_speed, 'team_smooth_speed' => $team_smooth_speed, 'team_nav_style' => $team_nav_style, 'rtl' => $isRTL);
wp_register_script('busicare-team', BUSICAREP_PLUGIN_URL . '/inc/js/front-page/team.js', array('jquery'));
wp_localize_script('busicare-team', 'team_settings', $teamsettings);
wp_enqueue_script('busicare-team');
?>
<section class="section-space team-group">
    <div class="busicare-team-container container">
        <?php
        $home_team_section_title = get_theme_mod('home_team_section_title', __('The Team', 'busicare-plus'));
        $home_team_section_discription = get_theme_mod('home_team_section_discription', __('Meet Our Experts', 'busicare-plus'));
        if (($home_team_section_title) || ($home_team_section_discription) != '') {
            ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if (!empty($home_team_section_title)): ?>
                            <h2 class="section-title"><?php echo $home_team_section_title; ?></h2>
                            <div class="title_seprater"></div>
                            <?php
                        endif;

                        if (!empty($home_team_section_discription)):
                            ?>
                            <h5 class="section-subtitle"><?php echo $home_team_section_discription; ?></h5>
                        <?php endif; ?>
                    </div>
                </div>						
            </div>
        <?php } ?>
        <div class="row">
            <?php if(get_page_template_slug()!="template-team-content-5.php"):?>
            <div id="team-carousel" class="owl-carousel owl-theme col-lg-12">
                <?php endif;
                $team_options = json_decode($team_options);
                
                if ($team_options != '') {
                    foreach ($team_options as $team_item) {
                        $image = !empty($team_item->image_url) ? apply_filters('busicare_translate_single_string', $team_item->image_url, 'Team section') : '';
                        $title = !empty($team_item->membername) ? apply_filters('busicare_translate_single_string', $team_item->membername, 'Team section') : '';
                        $subtitle = !empty($team_item->designation) ? apply_filters('busicare_translate_single_string', $team_item->designation, 'Team section') : '';
                        $link = !empty($team_item->link) ? apply_filters('busicare_translate_single_string', $team_item->link, 'Team section') : '';
                        $open_new_tab = $team_item->open_new_tab;
                        ?>
                        <div <?php if(get_page_template_slug()=="template-team-content-5.php") { ?>class="col-lg-4 col-md-6 col-sm-12"<?php } else { ?> class="item" <?php } ?>>
                            <div class="team-grid text-center">
                                <div class="img-holder">
                                    <?php if (!empty($image)) : ?>
                                        <?php
                                        if (!empty($link)) :
                                            $link_html = '<a href="' . esc_url($link) . '"';
                                            if (function_exists('busicare_is_external_url')) {
                                                $link_html .= busicare_is_external_url($link);
                                            }
                                            $link_html .= '>';
                                            echo $link_html;
                                        endif;
                                        echo '<img class="img-fluid" src="' . esc_url($image) . '"';
                                        if (!empty($title)) {
                                            echo 'alt="' . esc_attr($title) . '" title="' . esc_attr($title) . '"';
                                        }
                                        echo '/>';
                                        if (!empty($link)) {
                                            echo '</a>';
                                        }
                                        ?>
                                    <?php endif; ?>

                                    <?php
                                    $icons = html_entity_decode($team_item->social_repeater);
                                    $icons_decoded = json_decode($icons, true);
                                    $socails_counts = $icons_decoded;
                                    if (!empty($socails_counts)) :
                                        ?> <div class="social-group"> <?php if (!empty($icons_decoded)) : ?>
                                                <ul class="custom-social-icons">
                                                    <?php
                                                    foreach ($icons_decoded as $value) {
                                                        $social_icon = !empty($value['icon']) ? apply_filters('busicare_translate_single_string', $value['icon'], 'Team section') : '';
                                                        $social_link = !empty($value['link']) ? apply_filters('busicare_translate_single_string', $value['link'], 'Team section') : '';
                                                        if (!empty($social_icon)) {
                                                            ?>							
                                                            <li><a <?php
                                                                if ($open_new_tab == 'yes') {
                                                                    echo 'target="_blank"';
                                                                }
                                                                ?> href="<?php echo esc_url($social_link); ?>" class="btn btn-just-icon btn-simple">
                                                                    <i class="fa <?php echo esc_attr($social_icon); ?> ">
                                                                    </i>
                                                                </a>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                            <?php
                                        endif;
                                    endif;
                                    ?>


                                </div>
                                <?php if ($title != '' || $subtitle != ''): ?>
                                    <figcaption class="details">
                                        <?php if (!empty($title)) : ?>
                                            <?php if (!empty($link)) : ?>
                                                <a href="<?php echo esc_url($link); ?>" <?php
                                                if ($open_new_tab == 'yes') {
                                                    echo 'target="_blank"';
                                                }
                                                ?>>
                                                   <?php endif; ?>
                                                <h4 class="name"><?php echo esc_html($title); ?></h4>
                                                <?php if (!empty($link)) : ?>	
                                                </a>
                                            <?php endif; ?>	
                                        <?php endif; ?>
                                        <?php if (!empty($subtitle)) : ?>
                                            <span class="position"><?php echo esc_html($subtitle); ?></span>
                                        <?php endif; ?>
                                    </figcaption>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="item">					
                        <div class="team-grid text-center">
                            <div class="img-holder">
                                <img src="<?php echo BUSICAREP_PLUGIN_URL.'/inc/images/team/team1.jpg'; ?>" class="img-fluid" alt="Danial Wilson">
                                <div class="social-group">
                                    <ul class="custom-social-icons">
                                        <li><a class="facebook" href="#"><i class="fa-brands fa-facebook-f"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa-brands fa-x-twitter"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fa-brands fa-linkedin"></i></a></li>
                                        <li><a class="behance" href="#"><i class="fa fa-behance "></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <figcaption class="details">
                                <h4 class="name"><?php echo __('Danial Wilson', 'busicare-plus'); ?></h4>
                                <span class="position"><?php echo __('Senior Manager', 'busicare-plus'); ?></span>
                            </figcaption>
                        </div>
                    </div>
                    <div class="item">					
                        <div class="team-grid text-center">
                            <div class="img-holder">
                                <img src="<?php echo BUSICAREP_PLUGIN_URL.'/inc/images/team/team2.jpg'; ?>" class="img-fluid" alt="Amanda Smith">
                                <div class="social-group">
                                    <ul class="custom-social-icons">
                                        <li><a class="facebook" href="#"><i class="fa-brands fa-facebook-f"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa-brands fa-x-twitter"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fa-brands fa-linkedin"></i></a></li>
                                        <li><a class="behance" href="#"><i class="fa fa-behance "></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <figcaption class="details">
                                <h4 class="name"><?php echo __('Amanda Smith', 'busicare-plus'); ?></h4>
                                <span class="position"><?php echo __('Founder & CEO', 'busicare-plus'); ?></span>
                            </figcaption>
                        </div>
                    </div>
                    <div class="item">					
                        <div class="team-grid text-center">
                            <div class="img-holder">
                                <img src="<?php echo BUSICAREP_PLUGIN_URL.'/inc/images/team/team3.jpg'; ?>" class="img-fluid" alt="Victoria Wills">
                                <div class="social-group">
                                    <ul class="custom-social-icons">
                                        <li><a class="facebook" href="#"><i class="fa-brands fa-facebook-f"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa-brands fa-x-twitter"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fa-brands fa-linkedin"></i></a></li>
                                        <li><a class="behance" href="#"><i class="fa fa-behance "></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <figcaption class="details">
                                <h4 class="name"><?php echo __('Victoria Wills', 'busicare-plus'); ?></h4>
                                <span class="position"><?php echo __('Web Master', 'busicare-plus'); ?></span>
                            </figcaption>
                        </div>
                    </div>
                    <div class="item">					
                        <div class="team-grid text-center">
                            <div class="img-holder">
                                <img src="<?php echo BUSICAREP_PLUGIN_URL.'/inc/images/team/team4.jpg'; ?>" class="img-fluid" alt="Travis Marcus">
                                <div class="social-group">
                                    <ul class="custom-social-icons">
                                        <li><a class="facebook" href="#"><i class="fa-brands fa-facebook-f"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa-brands fa-x-twitter"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fa-brands fa-linkedin"></i></a></li>
                                        <li><a class="behance" href="#"><i class="fa fa-behance "></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <figcaption class="details">
                                <h4 class="name"><?php echo __('Travis Marcus', 'busicare-plus'); ?></h4>
                                <span class="position"><?php echo __('UI Developer', 'busicare-plus'); ?></span>
                            </figcaption>
                        </div>
                    </div>
                    <div class="item">					
                        <div class="team-grid text-center">
                            <div class="img-holder">
                                <img src="<?php echo BUSICAREP_PLUGIN_URL.'/inc/images/team/team5.jpg'; ?>" class="img-fluid" alt="Adriel Harlyn">	
                                <div class="social-group">
                                    <ul class="custom-social-icons">
                                        <li><a class="facebook" href="#"><i class="fa-brands fa-facebook-f"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa-brands fa-x-twitter"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fa-brands fa-linkedin"></i></a></li>
                                        <li><a class="skype" href="#"><i class="fa fa-skype"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <figcaption class="details">
                                <h4 class="name"><?php echo __('Adriel Harlyn', 'busicare-plus'); ?></h4>
                                <span class="position"><?php echo __('Founder', 'busicare-plus'); ?></span>
                            </figcaption>
                        </div>
                    </div>	
                <?php } ?>
            <?php if(get_page_template_slug()!="template-team-content-5.php"):?>
            </div>
        <?php endif;?>
        </div>
    </div>	
</section>	