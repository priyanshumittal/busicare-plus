<?php
$theme = wp_get_theme();
if('BusiCare Dark' == $theme->name) {$bc_service_design=2;}
else{$bc_service_design=1;}
if (get_page_template_slug() == 'template-service.php') {
    $busicare_service_layout = 1;
} elseif (get_page_template_slug() == 'template-service-2.php') {
    $busicare_service_layout = 2;
} elseif (get_page_template_slug() == 'template-service-3.php') {
    $busicare_service_layout = 3;
} elseif (get_page_template_slug() == 'template-service-4.php') {
    $busicare_service_layout = 4;
} else {
    $busicare_service_layout = get_theme_mod('home_serive_design_layout', $bc_service_design);
}

switch ($busicare_service_layout) {
    case 1:
        $service_section_classes = 'services';
        $service_article_classes = 'text-center';
        break;

    case 2:
        $service_section_classes = 'services2';
        $service_article_classes = '';
        break;

    case 3:
        $service_section_classes = 'services3';
        $service_article_classes = 'text-center';
        break;

    case 4:
        $service_section_classes = 'services4';
        $service_article_classes = '';
        break;
}
$service_data = get_theme_mod('busicare_service_content');
if (empty($service_data)) {
    $service_data = busicare_plus_starter_service_json();
}
$busicare_service_section_title = get_theme_mod('home_service_section_title', __('Why Choose Us?', 'busicare-plus'));
$busicare_service_section_discription = get_theme_mod('home_service_section_discription', __('Our Services', 'busicare-plus'));
?>
<section class="section-space <?php echo $service_section_classes; ?>">
    <div class="busicare-service-container container">
        <?php if ($busicare_service_section_discription != '' || $busicare_service_section_title != '') {
            ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-header">
                        <?php if($busicare_service_section_title != ''){ ?>
                        <h2 class="section-title"><?php echo $busicare_service_section_title; ?></h2><div class="title_seprater"></div>
                        <?php } ?>
                        <?php if($busicare_service_section_discription != ''){ ?>
                        <h5 class="section-subtitle"><?php echo $busicare_service_section_discription; ?></h5>
                        <?php }?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <?php
            $service_data = json_decode($service_data);
            if (!empty($service_data)) {
                foreach ($service_data as $service_team) {
                    $service_icon = !empty($service_team->icon_value) ? apply_filters('busicare_translate_single_string', $service_team->icon_value, 'Service section') : '';
                    $service_image = !empty($service_team->image_url) ? apply_filters('busicare_translate_single_string', $service_team->image_url, 'Service section') : '';
                    $service_title = !empty($service_team->title) ? apply_filters('busicare_translate_single_string', $service_team->title, 'Service section') : '';
                    $service_desc = !empty($service_team->text) ? apply_filters('busicare_translate_single_string', $service_team->text, 'Service section') : '';
                    $service_link = !empty($service_team->link) ? apply_filters('busicare_translate_single_string', $service_team->link, 'Service section') : '';
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">               
                        <article class="post <?php echo $service_article_classes; ?>">
                            <?php
                            if ($service_team->choice == 'customizer_repeater_icon') {
                                if ($service_icon != '') {
                                    ?>
                                    <figure class="post-thumbnail">
                                        <?php if ($service_link != '') { ?>
                                            <a href="<?php echo esc_url($service_link); ?>" <?php if ($service_team->open_new_tab == 'yes') {
                                                echo "target='_blank'";
                                            } ?> >
                                                <i class="fa <?php echo $service_icon; ?>"></i>
                                            </a>
                                        <?php } else { ?>
                                            <a><i class="fa <?php echo $service_icon; ?>"></i></a>
                                    <?php } ?>
                                    </figure>
                                <?php
                                }
                            } else if ($service_team->choice == 'customizer_repeater_image') {
                                if ($service_image != '') {
                                    ?>
                                    <figure class="post-thumbnail"> 
                                            <?php if ($service_link != '') { ?>
                                            <a <?php if ($service_team->open_new_tab == 'yes') {
                                                    echo "target='_blank'";
                                                } ?> href="<?php echo esc_url($service_link); ?>">
                                        <?php } ?>
                                            <img class='img-fluid' src="<?php echo $service_image; ?>">
                                    <?php if ($service_link != '') { ?>
                                            </a>
                                    <?php } ?>
                                    </figure>
                                <?php
                                }
                            }
                            if ($service_title != "") {
                                ?>
                                <div class="entry-header">
                                    <h5 class="entry-title">
                                <?php if ($service_link != '') { ?>
                                            <a href="<?php echo esc_url($service_link); ?>" <?php if ($service_team->open_new_tab == 'yes') {
                        echo "target='_blank'";
                    } ?>><?php } echo $service_title;
                if ($service_link != '') { ?></a>
            <?php } ?>
                                    </h5>
                                </div>
            <?php
        }
        if ($service_desc != ""):
            ?>
                                <div class="entry-content">
                                    <p><?php echo $service_desc; ?></p>
                                </div>
        <?php endif; ?>
                        </article>
                    </div>
        <?php
    }
}
?>
        </div>
    </div>
</section>  <div class="clearfix"></div>