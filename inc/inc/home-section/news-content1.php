<?php
$index_news_link = get_theme_mod('home_blog_more_btn_link', __('#', 'busicare-plus'));
$index_more_btn = get_theme_mod('home_blog_more_btn', __('View More', 'busicare-plus'));
if (empty($index_news_link)) {
    $index_news_link = '#';
}
?>
<!-- Latest News section -->
<section class="section-space blog home-blog">
    <div class="busicare-newz container">
        <?php
        $home_news_section_title = get_theme_mod('home_news_section_title', __('Our Latest News', 'busicare-plus'));
        $home_news_section_discription = get_theme_mod('home_news_section_discription', __('From our blog', 'busicare-plus'));
        $home_meta_section_settings = get_theme_mod('home_meta_section_settings', true);
        if (($home_news_section_title) || ($home_news_section_discription) != '') {
            ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if ($home_news_section_title) { ?>
                            <h2 class="section-title"><?php echo $home_news_section_title; ?></h2>
                            <div class="title_seprater"></div>
                        <?php } ?>
                        <?php if ($home_news_section_discription) { ?>
                            <h5 class="section-subtitle"><?php echo $home_news_section_discription; ?></h5>
                        <?php } ?>
                    </div>
                </div>						
            </div>
            <!-- /Section Title -->
        <?php } ?>
        <div class="row">
            <?php
            $news_class=12/get_theme_mod('busicare_homeblog_layout', 3);
            $no_of_post = get_theme_mod('busicare_homeblog_counts', 3);
            $args = array('post_type' => 'post', 'post__not_in' => get_option("sticky_posts"), 'posts_per_page' => $no_of_post);
            query_posts($args);
            if (query_posts($args)) {
                while (have_posts()):the_post();
                    {
                        ?>
                        <div class="col-lg-<?php echo $news_class; ?> col-md-6 col-sm-12">
                            <article class="post">	
                                <?php if (has_post_thumbnail()) { ?>
                                    <figure class="post-thumbnail">
                                        <?php $defalt_arg = array('class' => "img-fluid"); ?>
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('', $defalt_arg); ?></a>							
                                    </figure>	
                                <?php } ?>
                                <div class="post-content">

                                    <?php if ($home_meta_section_settings == true) { ?>
                                        <div class="entry-date <?php
                                        if (!has_post_thumbnail()) {
                                            echo 'remove-image';
                                        }
                                        ?>">
                                            <a href="<?php echo esc_url(home_url()); ?>/<?php echo esc_html(date('Y/m', strtotime(get_the_date()))); ?>"><time><?php echo esc_html(get_the_date('M d, Y')); ?></time></a>
                                        </div>
                                    <?php } ?>


                                    <?php if ($home_meta_section_settings == true) { ?>
                                        <div class="entry-meta">
                                            <span class="author"><?php
                                                echo __('By', 'busicare-plus');
                                                echo '&nbsp;';
                                                ?><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author(); ?></a></span>
                                            <?php
                                            $cat_list = get_the_category_list();
                                            if (!empty($cat_list)) {
                                                ?>
                                                <span class="cat-links"><?php
                                                    echo 'in';
                                                    the_category(', ');
                                                    ?></span>
                                            <?php } ?>
                                        </div>	
                                    <?php } ?>

                                    <header class="entry-header">
                                        <h3 class="entry-title">
                                            <a class="home-blog-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h3>
                                    </header>	

                                    <div class="entry-content">
                                        <?php the_excerpt(); ?>
                                        <p><a href="<?php the_permalink(); ?>" class="more-link"><?php echo get_theme_mod('home_news_button_title', __('Read More', 'busicare-plus')); ?><i class="fa <?php if(is_rtl()){echo 'fa-long-arrow-left';} else{ echo 'fa-long-arrow-right';}?>"></i></a></p>
                                    </div>	
                                </div>			
                            </article>
                        </div>
                        <?php
                    }
                endwhile;
            }
            ?>
        </div>

        <?php if (!empty($index_more_btn)): ?>
            <div class="row index_extend_class">
                <div class="mx-auto mt-5">
                    <a href="<?php echo $index_news_link; ?>" class="btn-small btn-default-dark business-view-more-post" <?php
                       if (get_theme_mod('home_blog_more_btn_link_target', false) == true) {
                           echo "target='_blank'";
                       };
                       ?>><?php echo get_theme_mod('home_blog_more_btn', __('View More', 'busicare-plus')); ?></a>
                </div>
            </div>
        <?php endif; ?>



    </div>
</section>