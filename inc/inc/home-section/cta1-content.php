<!--Cta Section-->
<?php
$home_cta1_title = get_theme_mod('home_cta1_title', __('Looking for a first-class business consultant?', 'busicare-plus'));
$home_cta1_btn_text = get_theme_mod('home_cta1_btn_text', __('Get In Touch', 'busicare-plus'));
$cta1_button_link = get_theme_mod('home_cta1_btn_link', '#');
$home_cta1_btn_link_target = get_theme_mod('home_cta1_btn_link_target', false);
?>	
<!--Call to Action-->
<?php
$callout_cta1_background = get_theme_mod('callout_cta1_background', '');
if ($callout_cta1_background != '') {
    ?>
    <section class="cta_main"  style="background-color: <?php echo esc_url($callout_cta1_background); ?>">
        <?php
    } else {
        ?>
        <section class="cta_main" >
            <?php
        }
        ?>
        <?php if (!empty($home_cta1_title) || (!empty($home_cta1_btn_text))): ?>
            <div class="busicare-cta1-container container">
                <div class="row cta_content">
                    <div class="col-lg-8 col-md-6 col-sm-12">
                        <?php if (!empty($home_cta1_title)): ?><h1><?php echo $home_cta1_title; ?></h1><?php endif; ?>
                    </div>	
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <?php if ($home_cta1_btn_text != '') {
                            ?>
                            <a class="btn-small btn-light cta_btn" <?php if ($cta1_button_link != '') { ?> href="<?php echo $cta1_button_link; ?>" <?php
                                                                           if ($home_cta1_btn_link_target == true) {
                                                                               echo "target='_blank'";
                                                                           }
                                                                       }else{echo "href='#'";}
                                                                       ?>><?php echo $home_cta1_btn_text; ?></a>

                        <?php } ?>
                    </div>


                </div>
            </div>
        <?php endif; ?>
    </section>
    <!--Cta Section-->