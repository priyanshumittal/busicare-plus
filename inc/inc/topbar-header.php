<?php if( is_active_sidebar('home-header-sidebar_left') || is_active_sidebar('home-header-sidebar_right')) {?>
<header class="header-sidebar">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-7 col-sm-12">
				<?php 
				if( is_active_sidebar('home-header-sidebar_left') ) 
				{
					dynamic_sidebar( 'home-header-sidebar_left' ); 
				} 
				?>
			</div>
			<div class="col-lg-4 col-md-5 col-sm-12">
				<?php 
				if( is_active_sidebar('home-header-sidebar_right') ) {
				dynamic_sidebar( 'home-header-sidebar_right' );
				} 
				?>
			</div>
		</div>
	</div>
</header>
<?php } ?>