<?php
/**
 * 
 * @package busicare PRO
 * @since   busicare PRO 3.0
 */

/**
 * Hide advanced footer markup if:
 *
 * - User is not logged in. [AND]
 * - All widgets are not active.
 */
if ( ! is_user_logged_in() ) {
	if (
		! is_active_sidebar( 'footer-sidebar-1' ) &&
		! is_active_sidebar( 'footer-sidebar-2' ) &&
		! is_active_sidebar( 'footer-sidebar-3' ) &&
		! is_active_sidebar( 'footer-sidebar-4' )
	) {
		return;
	}
}


?>
<div class="row footer-sidebar footer-typo">
	<div class="col-lg-12 col-md-12 col-sm-12">
<?php busicare_plus_footer_widget_area('footer-sidebar-1');?>
	</div>
</div>
