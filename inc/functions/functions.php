<?php
//  BusiCare plus customizer register function
if (!function_exists('busicare_plus_customize_register')) :

$repeater_path = trailingslashit(BUSICAREP_PLUGIN_DIR) . '/inc/functions/customizer-repeater/functions.php';
if (file_exists($repeater_path)) {
    require_once( $repeater_path );
}

$page_editor_path = trailingslashit(BUSICAREP_PLUGIN_DIR) . '/inc/functions/customizer-page-editor/customizer-page-editor.php';
if (file_exists($page_editor_path)) {
    require_once( $page_editor_path );
}

    function busicare_plus_customize_register($wp_customize) {

        $sections_customizer_data = array('slider', 'services', 'fun', 'portfolio', 'testimonial', 'news', 'cta1', 'cta2', 'team', 'client', 'wooproduct');
        $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

        if (!empty($sections_customizer_data)) {
            foreach ($sections_customizer_data as $section_customizer_data) {
                require('customizer/' . $section_customizer_data . '-section.php');
            }
        }

        //  customizer setting files including
        require_once ( 'customizer/customizer_layout_manager.php' );
        require_once ( 'customizer/customizer_theme_style.php' );
    }
    add_action('customize_register', 'busicare_plus_customize_register');
endif;
            
//  home sections file including
             
$sections_data = array('slider','cta1', 'services', 'cta2',  'portfolio', 'fun', 'news', 'testimonial', 'wooproduct', 'team', 'client');
                
        if (!empty($sections_data))
            { 
                foreach($sections_data as $section_data)
                    { 
                        require_once('sections/'.$section_data.'-section.php');
                    }   
            }
                
//Admin customizer preview
if ( ! function_exists( 'busicare_customizer_preview_scripts' ) ) {
    function busicare_customizer_preview_scripts() {
        wp_enqueue_script( 'busicare-customizer-preview', BUSICAREP_PLUGIN_URL . 'inc/js/customizer-preview.js', array( 'customize-preview', 'jquery' ) );
    }
}
add_action( 'customize_preview_init', 'busicare_customizer_preview_scripts' );

require_once('custom-style/custom-css.php');
require_once('pagination/busicare_pagination.php');
require_once('breadcrumbs/breadcrumbs.php');
// Adding customizer files
 require_once ('customizer/helper-function.php');
 require_once ('customizer/custom-control.php');
 require_once ('layout-functions.php');
 require_once ('customizer/customizer_sections_setting.php' );
 //require_once ('customizer/blog-options.php' );
 require_once ('customizer/single-blog-options.php' );
 require_once ('customizer/template.php');
 require_once ('customizer/footer-options.php' );
 require_once ('customizer/general_settings.php' );
//Breadcrumb File
require_once ('compatibility/class-breadcrumb-trail.php');
require_once ('customizer/customizer_typography.php' );
require_once ('customizer/customizer_color_back_settings.php');

require_once('customizer/repeater-default-value.php');

//Alpha Color Control
require_once('customizer/customizer-alpha-color-picker/class-busicare-customize-alpha-color-control.php');

//Customizer Page Editor
require_once('customizer/customizer-page-editor/class/class-busicare-page-editor.php');

//Customizer Subscriber tabs
require_once('customizer/customizer-tabs/class/class-busicare-customize-control-tabs.php');

//Subscriber Info
require_once('customizer/customizer-subscribe-info/class-busicare-subscribe-info.php');

//Plugin Install
require_once('plugin-install/class-busicare-plugin-install-helper.php');

// Adding hooks files
require_once('hooks/functions.php');
require_once('hooks/busicare-hooks-settings.php');
require_once('hooks/busicare-hooks.php');


// Theme title
if (!function_exists('busicare_plus_head_title')) {

    function busicare_plus_head_title($title, $sep) {
        global $paged, $page;

        if (is_feed())
            return $title;

        // Add the site name
        $title .= get_bloginfo('name');

        // Add the site description for the home / front page
        $site_description = get_bloginfo('description');
        if ($site_description && ( is_home() || is_front_page() ))
            $title = "$title $sep $site_description";

        // Add a page number if necessary.
        if (( $paged >= 2 || $page >= 2 ) && !is_404())
            $title = "$title $sep " . sprintf(esc_html__('Page', 'busicare-plus'), max($paged, $page));

        return $title;
    }

}
add_filter('wp_title', 'busicare_plus_head_title', 10, 2);
function busicare_plus_customizer_live_preview() {
    wp_enqueue_script(
            'busicare-customizer-preview', BUSICAREP_PLUGIN_URL . '/inc/js/customizer.js', array(
        'jquery',
        'customize-preview',
            ), 999, true
    );
}

add_action('customize_preview_init', 'busicare_plus_customizer_live_preview');

add_action("customize_register", "busicare_plus_remove_defult_setting_customize_register");

function busicare_plus_remove_defult_setting_customize_register($wp_customize) {
    $wp_customize->remove_control("header_image");
}

add_action('customize_register', 'busicare_register_custom_controls');


// custom background
function busicare_plus_custom_background_function() {
    $page_bg_image_url = get_theme_mod('predefined_back_image', 'bg-img1.png');
    if ($page_bg_image_url != '') {
        echo '<style>body.boxed{ background-image:url("'.BUSICAREP_PLUGIN_URL.'/inc/images/bg-pattern/' . $page_bg_image_url . '");}</style>';
    }
}

add_action('wp_head', 'busicare_plus_custom_background_function', 10, 0);

function busicare_plus_alpha_remove_class($wp_classes) {
    unset($wp_classes[array_search("blog", $wp_classes)]);

    return $wp_classes;
}

add_filter('body_class', 'busicare_plus_alpha_remove_class');

//Post Navigation Menu
if (get_theme_mod('post_nav_style_setting', 'pagination') != "pagination") {
    /*
     * initial posts dispaly
     */

    function busicare_plus_script_load_more($args = array()) {
    //initial posts load
    global $template;
    $row='';
    $row_template = array("template-blog-grid-view-sidebar.php", "template-blog-grid-view.php", "template-blog-masonry-two-column.php", "template-blog-masonry-three-column.php", "template-blog-masonry-four-column.php");
    if (in_array(basename($template), $row_template))   
    {
        $row='row';
    }
        echo '<div id="ajax-content" class="'.$row.' content-area">';
            busicare_plus_ajax_script_load_more($args);
        echo '</div>';
        echo '<span id="ajax-content2" >';
        echo '</span>';
        echo '<a href="#" id="loadMore" class="'.get_theme_mod('post_nav_style_setting','pagination').'='.basename($template).'" data-page="1" data-url="'.admin_url("admin-ajax.php").'" >Load More</a>';
        //echo basename($template);
    }

    /*
     * create short code.
     */
    add_shortcode('ajax_posts', 'busicare_plus_script_load_more');

    /*
     * load more script call back
     */

    function busicare_plus_ajax_script_load_more($args) {
        global $template;
        //init ajax
        $ajax = false;
        //check ajax call or not
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $ajax = true;
        }
        //number of posts per page default
        $num = get_option('posts_per_page');
        //page number
        $paged =  empty($_POST['page']) ? 0 : $_POST['page'] + 1;
        //args
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $num,
            'paged' => $paged
        );

        $page_template = empty($_POST['ajaxPage_template']) ? '' : $_POST['ajaxPage_template'];        
        //query
        $query = new WP_Query($args);
        //check
        if ($query->have_posts()):
            //loop articales
            while ($query->have_posts()): $query->the_post();
                //include articles template

                if (($page_template == 'template-blog-full-width.php') || (basename($template) == 'template-blog-full-width.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-content.php');
                } elseif (($page_template == 'template-blog-grid-view-sidebar.php') || (basename($template) == 'template-blog-grid-view-sidebar.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-blog-grid-content.php');
                } elseif (($page_template == 'template-blog-grid-view.php') || (basename($template) == 'template-blog-grid-view.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-blog-grid-view-content.php');
                } elseif (($page_template == 'template-blog-left-sidebar.php') || (basename($template) == 'template-blog-left-sidebar.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-content.php');
                } elseif (($page_template == 'template-blog-list-view-sidebar.php') || (basename($template) == 'template-blog-list-view-sidebar.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-list-view-sidebar-content.php');
                } elseif (($page_template == 'template-blog-list-view.php') || (basename($template) == 'template-blog-list-view.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-list-view-content.php');
                } elseif (($page_template == 'template-blog-right-sidebar.php') || (basename($template) == 'template-blog-right-sidebar.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-content.php');
                } elseif (($page_template == 'template-blog-masonry-two-column.php') || (basename($template) == 'template-blog-masonry-two-column.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-masonary-content.php');
                } elseif (($page_template == 'template-blog-masonry-three-column.php') || (basename($template) == 'template-blog-masonry-three-column.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-masonary-three-col-content.php');
                } elseif (($page_template == 'template-blog-masonry-four-column.php') || (basename($template) == 'template-blog-masonry-four-column.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/ajax-masonary-four-col-content.php');
                } elseif (($page_template == 'home.php') || (basename($template) == 'home.php')) {
                    include (BUSICAREP_PLUGIN_DIR.'/inc/template-parts/content.php');
                }

            endwhile;
        else:
            echo 0;
        endif;
        //reset post data
        wp_reset_postdata();
        //check ajax call
        if ($ajax)
            die();
    }

    /*
     * load more script ajax hooks
     */
    add_action('wp_ajax_nopriv_busicare_plus_ajax_script_load_more', 'busicare_plus_ajax_script_load_more');
    add_action('wp_ajax_busicare_plus_ajax_script_load_more', 'busicare_plus_ajax_script_load_more');

    /*
     * enqueue js script
     */
    add_action('wp_enqueue_scripts', 'busicare_plus_ajax_enqueue_script');
    /*
     * enqueue js script call back
     */

    function busicare_plus_ajax_enqueue_script() {
        global $template;
        if ((basename($template) == 'template-blog-full-width.php') || (basename($template) == 'template-blog-grid-view-sidebar.php') || (basename($template) == 'template-blog-grid-view.php') || (basename($template) == 'template-blog-left-sidebar.php') || (basename($template) == 'template-blog-list-view-sidebar.php') || (basename($template) == 'template-blog-list-view.php') || (basename($template) == 'template-blog-right-sidebar.php') || (basename($template) == 'template-blog-masonry-two-column.php') || (basename($template) == 'template-blog-masonry-three-column.php') || (basename($template) == 'template-blog-masonry-four-column.php') || (basename($template) == 'home.php')) {
            wp_enqueue_script('script_ajax',BUSICAREP_PLUGIN_URL.'/inc/js/script_ajax.js', array('jquery'), '1.0', true);
        }
    }

}

//Woocommerce single product 
if (!function_exists('busicare_plus_template_single_title')) {
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
    add_action('woocommerce_single_product_summary', 'busicare_plus_template_single_title', 5);

    function busicare_plus_template_single_title() {
        the_title('<h2 class="product_title entry-title">', '</h2>');
    }

}


add_filter('woocommerce_pagination_args', 'busicare_plus_filter_function_woocommerce_arr', 12);

function busicare_plus_filter_function_woocommerce_arr($array) {

    $array = array(// WPCS: XSS ok.
        'prev_text' => (is_rtl()) ? '&rarr;' : '&larr;',
        'next_text' => (is_rtl()) ? '&larr;' : '&rarr;',
        'type' => 'list',
    );

    return $array;
}

function busicare_slide_sidebars() {
    ?>
    <div style="display: none">
        <?php
        if ( is_customize_preview() ) {
            dynamic_sidebar( 'slider-widget-area' );

        }
        ?>
    </div>
    <?php
}
add_action( 'busicare_slider_sidebar','busicare_slide_sidebars' );

function busicare_plus_container_style(){?>
    <style type="text/css">
        .container.container_default{max-width: <?php echo get_theme_mod('container_width_pattern',1140);?>px;}
        .container.slider-caption{max-width: <?php echo get_theme_mod('container_slider_width',1140);?>px;}
        .busicare-cta1-container.container{max-width: <?php echo get_theme_mod('container_cta1_width',1140);?>px;}
        .busicare-service-container.container{max-width: <?php echo get_theme_mod('container_service_width',1140);?>px;}
        .busicare-cta2-container.container{max-width: <?php echo get_theme_mod('container_cta2_width',1140);?>px;}
        .busicare-portfolio-container.container{max-width: <?php echo get_theme_mod('container_portfolio_width',1140);?>px;}
        .busicare-fun-container.container{max-width: <?php echo get_theme_mod('container_fun_fact_width',1140);?>px;}
        .busicare-newz.container{max-width: <?php echo get_theme_mod('container_home_blog_width',1140);?>px;}
        .busicare-tesi-container.container{max-width: <?php echo get_theme_mod('container_testimonial_width',1140);?>px;}
        .busicare-team-container.container{max-width: <?php echo get_theme_mod('container_team_width',1140);?>px;}
        .busicare-shop-container.container{max-width: <?php echo get_theme_mod('container_shop_width',1140);?>px;}
        .busicare-client-container.container{max-width: <?php echo get_theme_mod('container_clients_width',1140);?>px;}
        </style>    
<?php
}
add_action('wp_head','busicare_plus_container_style',11);

//tgmpa
add_action( 'tgmpa_register', 'busicare_plus_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function busicare_plus_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
         // This is an example of how to include a plugin from the WordPress Plugin Repository.
        array(
                'name'                  => 'BusiCare Demo Importer',
                'slug'                  => 'busicare-demo-importer',
                'source'                => BUSICAREP_PLUGIN_DIR. 'plugins/busicare-demo-importer.zip',
                'required'              => true,
                'version'               => '1.0',
                'force_activation'      => false,
                'force_deactivation'    => false,
                'external_url'          => '',
            ),
        array(
            'name'      => esc_html__('One Click Demo Import', 'busicare-plus'),
            'slug'      => 'one-click-demo-import',
            'required'  => true,
        ),
         array(
            'name' => esc_html__('Contact Form 7', 'busicare-plus'),
            'slug' => 'contact-form-7',
            'required'  => true,
        ),
          array(
            'name' => esc_html__('Unique Headers', 'busicare-plus'),
            'slug' => 'unique-headers',
            'required'  => true,
        ),
        array(
            'name' => esc_html__('Woocommerce', 'busicare-plus'),
            'slug' => 'woocommerce',
            'required'  => true,
        ),
        array(
            'name' => esc_html__('Wp Google Maps', 'busicare-plus'),
            'slug' => 'wp-google-maps',
            'required'  => true,
        ),
        array(
            'name' => esc_html__('Yoast SEO', 'busicare-plus'),
            'slug' => 'wordpress-seo',
            'required'  => true,
        ),
    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
    );

    tgmpa( $plugins, $config );
}