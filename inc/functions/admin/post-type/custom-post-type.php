<?php
/************* Home portfolio custom post type ************************/		
function busicare_plus_project_type()
		{	register_post_type( 'busicare_portfolio',
				array(
				'labels' => array(
				'name' => __('Projects', 'busicare-plus'),
				'add_new' => __('Add New', 'busicare-plus'),
                'add_new_item' => __('Add New Project','busicare-plus'),
				'edit_item' => __('Add New','busicare-plus'),
				'new_item' => __('New Link','busicare-plus'),
				'all_items' => __('All Projects','busicare-plus'),
				'view_item' => __('View Link','busicare-plus'),
				'search_items' => __('Search Links','busicare-plus'),
				'not_found' =>  __('No Links found','busicare-plus'),
				'not_found_in_trash' => __('No Links found in Trash','busicare-plus'), 
				
			),
				'supports' => array('title','thumbnail'),
				'show_in_nav_menus' => false,
				'public' => true,
				'rewrite' => array('slug' => 'busicare_portfolio'),
				'menu_position' => 11,
				'public' => true,
				'menu_icon' => BUSICAREP_PLUGIN_URL . '/inc/images/portfolio.png',
			)
	);
}
add_action( 'init', 'busicare_plus_project_type' );
?>