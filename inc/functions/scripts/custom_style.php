<?php
// Typography
$enable_top_widget_typography = get_theme_mod('enable_top_widget_typography', false);
$enable_header_typography = get_theme_mod('enable_header_typography', false);
$enable_banner_typography = get_theme_mod('enable_banner_typography', false);
$enable_section_title_typography = get_theme_mod('enable_section_title_typography', false);
$enable_slider_title_typography = get_theme_mod('enable_slider_title_typography', false);
$enable_content_typography = get_theme_mod('enable_content_typography', false);
$enable_post_typography = get_theme_mod('enable_post_typography', false);
$enable_meta_typography = get_theme_mod('enable_meta_typography', false);
$enable_shop_page_typography = get_theme_mod('enable_shop_page_typography', false);
$enable_sidebar_typography = get_theme_mod('enable_sidebar_typography', false);
$enable_footer_bar_typography = get_theme_mod('enable_footer_bar_typography', false);
$enable_footer_widget_typography = get_theme_mod('enable_footer_widget_typography', false);
$enable_after_btn_typography= get_theme_mod('enable_after_btn_typography',false);


/* Top Widget Area */
if ($enable_top_widget_typography == true) {
    ?>
    <style>
        .header-sidebar .widgettitle { 
            font-size:<?php echo get_theme_mod('topbar_widget_title_fontsize', '30') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('topbar_widget_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('topbar_widget_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('topbar_widget_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('topbar_widget_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('topbar_widget_title_line_height','45').'px'; ?> !important;
        }
        .head-contact-info li, .head-contact-info li a, .header-sidebar .custom-social-icons li > a, .header-sidebar p, .header-sidebar a, .header-sidebar em { 
            font-size:<?php echo get_theme_mod('top_widget_typography_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('top_widget_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('top_widget_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('top_widget_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('top_widget_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('topbar_widget_content_line_height','30').'px'; ?> !important;
        }

        .header-sidebar .custom-social-icons li > a {
            width: <?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?> !important;
            height: <?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?> !important;
        }
    </style>
    <?php
}


/* Site title and tagline */
if ($enable_header_typography == true) {
    ?>
    <style>
        .site-title {
            font-size:<?php echo get_theme_mod('site_title_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('site_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('site_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('site_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('site_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('site_title_line_height','39').'px'; ?> !important;
        }

        .site-description {
            font-size:<?php echo get_theme_mod('site_tagline_fontsize', '20') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('site_tagline_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('site_tagline_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('site_tagline_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('site_tagline_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('site_tagline_line_height','30').'px'; ?> !important;
        }
        .navbar .nav > li > a {
            font-size:<?php echo get_theme_mod('menu_title_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('menu_title_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('menu_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('menu_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('menu_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('menu_line_height','30').'px'; ?> !important;
        }

        .dropdown-menu .dropdown-item{
            font-size:<?php echo get_theme_mod('submenu_title_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('submenu_title_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('submenu_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('submenu_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('submenu_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('submenu_line_height','30').'px'; ?> !important;
        }
    </style>
    <?php
}

/* After Title */
if($enable_after_btn_typography == true)
{
?>
<style>
.navbar .nav > li > a.busicare_header_btn {
    font-size:<?php echo get_theme_mod('after_btn_fontsize','15').'px'; ?> !important;
    line-height:<?php echo get_theme_mod('after_btn_lheight','1').'px'; ?> !important;
    font-weight:<?php echo get_theme_mod('after_btn_fontweight','700'); ?> !important;
    font-family:<?php echo get_theme_mod('after_btn_fontfamily','Open Sans'); ?> !important;
    font-style:<?php echo get_theme_mod('after_btn_fontstyle','normal'); ?> !important;
    text-transform:<?php echo get_theme_mod('after_btn_text_transform','default'); ?> !important;
}
</style>
<?php } 

/* Banner Title */
if ($enable_banner_typography == true) {
    ?>
    <style>
        .page-title h1 {
            font-size:<?php echo get_theme_mod('banner_title_fontsize', '32') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('banner_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('banner_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('banner_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('banner_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('banner_titl_line_height','41').'px'; ?> !important;
        }

        /* Breadcrumb Title */
        .page-breadcrumb a, .page-breadcrumb span, .page-breadcrumb , .rank-math-breadcrumb a , .rank-math-breadcrumb span, .navxt-breadcrumb a , .navxt-breadcrumb span { 
            font-size:<?php echo get_theme_mod('breadcrumb_title_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('breadcrumb_title_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('breadcrumb_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('breadcrumb_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('breadcrumb_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('breadcrumb_line_height','30').'px'; ?> !important;
        }
    </style>
    <?php
}


/* Section Title */
if ($enable_section_title_typography == true) {
    ?>
    <style>
        .section-header  h2:not(.cta-2-title), .contact .section-header h2 {
            font-size:<?php echo get_theme_mod('section_title_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('section_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('section_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('section_title_fontstyle', 'Normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('section_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('section_title_line_height','54')).'px'; ?> !important;
    }
     

        .section-header .section-subtitle{
            font-size:<?php echo get_theme_mod('section_subtitle_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('section_subtitle_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('section_subtitle_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('section_subtitle_fontstyle', 'Normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('section_subtitle_text_transform', 'default'); ?> !important;
           line-height:<?php echo esc_html(get_theme_mod('section_description_line_height','30')).'px'; ?> !important;
    }
    </style>
    <?php
}


/* Slider Title */
if ($enable_slider_title_typography == true) {
    ?>
    <style>
        .slider-caption h2  {
            font-size:<?php echo get_theme_mod('slider_title_fontsize', '50') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('slider_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('slider_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('slider_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('slider_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('slider_line_height','85')).'px'; ?> !important;
        }
    </style>
    <?php
}


/* Content */
if ($enable_content_typography == true) {
    ?>
    <style>
        /* Heading H1 */
        .about h1, body:not(.woocommerce-page) .entry-content h1, .service h1, .contact h1, .error-page h1, .cta_main h1 {
            font-size:<?php echo get_theme_mod('h1_typography_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h1_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h1_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h1_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h1_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h1_line_height','54')).'px'; ?> !important;;
        }

        /* Heading H2 */
        body:not(.woocommerce-page) .entry-content h2, .about h2, .service h2, .contact h2, .cta-2 h2, .section-module.about h2{
            font-size:<?php echo get_theme_mod('h2_typography_fontsize', '30') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h2_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h2_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h2_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h2_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h2_line_height','45')).'px'; ?> !important;
    
        }

        .error-page h2{
            font-size:<?php echo get_theme_mod('h2_typography_fontsize', '30') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h2_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h2_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h2_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h2_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h2_line_height','45')).'px'; ?> !important;    
        }

        /* Heading H3 */
        body:not(.woocommerce-page) .entry-content h3, .related-posts h3, .about h3, .service h3, .contact h3, .contact-form-map .title h3, .section-module.about h3, .comment-form .comment-respond h3, .home-blog .entry-header h3.entry-title a {
            font-size:<?php echo get_theme_mod('h3_typography_fontsize', '24') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h3_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h3_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h3_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h3_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h3_line_height','36')).'px'; ?> !important;
    
        }
        .comment-title h3{
            font-size:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 4 . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h3_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h3_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h3_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h3_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h3_line_height','36')).'px'; ?> !important;
        }

        /* Heading H4 */
        .entry-content h4, .entry-header h4, .team-grid h4, .entry-header h4 a, .contact-widget h4, .about h4, .testimonial .testmonial-block .name, .service h4, .contact h4, .portfolio h4, .section-module.about h4, #related-posts-carousel .entry-header h4 a, .blog-author h4.name, .error-page h4 {

            font-size:<?php echo get_theme_mod('h4_typography_fontsize', '20') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h4_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h4_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h4_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h4_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h4_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H5 */
        .product-price h5, .blog-author h5, .comment-detail h5, .entry-content h5, .about h5, .service h5, .services h5, .services h5 a, .contact h5, .section-module.about h5,.services2 h5.entry-title,.services2 h5.entry-title a,.services3 h5.entry-title,.services3 h5.entry-title a,.services4 h5.entry-title,.services4 h5.entry-title a{
            font-size:<?php echo get_theme_mod('h5_typography_fontsize', '18') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h5_line_height','24')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h5_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h5_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h5_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h5_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H6 */
        .entry-content h6, .about h6, .service h6, .contact h6, .section-module.about h6 {
            font-size:<?php echo get_theme_mod('h6_typography_fontsize', '14') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h6_line_height','21')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h6_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h6_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h6_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h6_typography_text_transform', 'default'); ?> !important;
        }

        /* Paragraph */
        .entry-content p, .about-content p, .funfact p, .woocommerce-product-details__short-description p, .wpcf7 .wpcf7-form p label, .testimonial .testmonial-block .designation, .about p, .entry-content li, .contact address, .contact p, .service p, .contact p, .sponsors p, .cta-2 p,.team-grid .card-body p{
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> !important;
        }
        .slider-caption p{
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> !important;
        }

        .portfolio .tab a, .portfolio li a{
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400') + 200; ?> !important;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> !important;
        }


        /* Button Text */
        .btn-combo a, .mx-auto a, .pt-3 a, .wpcf7-form .wpcf7-submit,  .woocommerce .button, .btn-default, .btn-light, .sidebar .woocommerce button[type="submit"], .site-footer .woocommerce button[type="submit"], .sidebar .widget .search-submit, #commentform input[type="submit"], .woocommerce .added_to_cart,.busicare_header_btn,.search-submit,.wp-block-button__link,.more-link,.wp-block-search__button{
            font-size:<?php echo get_theme_mod('button_text_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('button_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('button_text_typography_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('button_text_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('button_text_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('button_text_typography_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}

/* Blog / Archive / Single Post */
if ($enable_post_typography == true) {
    ?>
    <style>
        .entry-header h2 a, .entry-header h3.entry-title a:not(.home-blog-title), .entry-header h3.entry-title:not(.home-blog .entry-header h3.entry-title){
            font-size:<?php echo get_theme_mod('post-title_fontsize', '36') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('post-title_line_height','54')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('post-title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('post-title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('post-title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('post-title_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}

/* Post Meta */
if ($enable_meta_typography == true) {
    ?>
    <style>
        .entry-meta a, .entry-date a{
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('meta_fontweight', '500'); ?> !important;
            font-family:<?php echo get_theme_mod('meta_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('meta_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('meta_text_transform', 'default'); ?> !important;
        }

        .entry-meta span, .entry-date span{
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') - 1 . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?> !important;
            font-family:<?php echo get_theme_mod('meta_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('meta_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('meta_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}


/* Shop Page */
if ($enable_shop_page_typography == true) {
    ?>
    <style>
        /* Heading H1 */
        .woocommerce div.product h1.product_title{
            font-size:<?php echo get_theme_mod('shop_h1_typography_fontsize', '36') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('shop_h1_line_height','54')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('shop_h1_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('shop_h1_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('shop_h1_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('shop_h1_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H2 */
        .woocommerce .products h2, .woocommerce .cart_totals h2, .woocommerce-Tabs-panel h2, .woocommerce .cross-sells h2, .woocommerce div.product h2.product_title{
            font-size:<?php echo get_theme_mod('shop_h2_typography_fontsize', '18') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('shop_h2_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('shop_h2_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('shop_h2_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('shop_h2_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('shop_h2_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H3 */
        .woocommerce .checkout h3 {
            font-size:<?php echo get_theme_mod('shop_h3_typography_fontsize', '24') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('shop_h3_line_height','36')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('shop_h3_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('shop_h3_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('shop_h3_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('shop_h3_typography_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}


/* Sidebar widgets */
if ($enable_sidebar_typography == true) {
    ?>
    <style>
        .sidebar .widget-title{
            font-size:<?php echo get_theme_mod('sidebar_fontsize', '24') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('sidebar_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('sidebar_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('sidebar_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('sidebar_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('sidebar_line_height','36')).'px'; ?> !important;
        }
        /* Sidebar Widget Content */
        .sidebar .widget_recent_entries a, .sidebar a, .sidebar p {
            font-size:<?php echo get_theme_mod('sidebar_widget_content_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('sidebar_widget_content_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('sidebar_widget_content_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('sidebar_widget_content_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('sidebar_widget_content_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('sidebar_widget_content_line_height','30')).'px'; ?> !important;
        }
    </style>
    <?php
}


/* Footer Bar */
if ($enable_footer_bar_typography == true) {
    ?>
    <style>
        .site-footer .site-info .footer-sidebar{
            font-size:<?php echo get_theme_mod('footer_bar_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('footer_bar_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('footer_bar_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('footer_bar_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('footer_bar_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('fbar_line_height','28')).'px'; ?> !important;
        }
    </style>
    <?php
}


/* Footer Widget */
if ($enable_footer_widget_typography == true) {
    ?>
    <style>
        /* Footer Ribbon Text */
        .footer-social-links .custom-social-icons span, .footer-social-links .custom-social-icons a {
            font-size:<?php echo get_theme_mod('ribbon_text_fontsize', '20') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('ribbon_text_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('ribbon_text_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('ribbon_text_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('ribbon_text_transform', 'default'); ?> !important;
        }

        /* Footer Widget Title */
        .site-footer .widget-title  {
            font-size:<?php echo get_theme_mod('footer_widget_title_fontsize', '24') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('footer_widget_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('footer_widget_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('footer_widget_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('footer_widget_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('footer_widget_title_line_height','36')).'px'; ?> !important;
        }
        /* Footer Widget Content */
        .footer-sidebar .widget_recent_entries a, .footer-sidebar.footer-typo a, .footer-sidebar.footer-typo p, .footer-sidebar  .textwidget, .footer-sidebar  .head-contact-info li, .footer-sidebar .head-contact-info li a, .footer-sidebar em  {
            font-size:<?php echo get_theme_mod('footer_widget_content_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('footer_widget_content_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('footer_widget_content_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('footer_widget_content_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('footer_widget_content_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('footer_widget_content_line_height','30')).'px'; ?> !important;
        }

        .footer-sidebar .custom-social-icons li > a {
            width: <?php echo get_theme_mod('footer_widget_content_fontsize', '15') + 12 . 'px'; ?> !important;
            height: <?php echo get_theme_mod('footer_widget_content_fontsize', '15') + 12 . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('footer_widget_content_fontsize', '15') + 12 . 'px'; ?> !important;
        }
    </style>
<?php } ?>



<?php
// -----------------Colors & Background----------------------
?>



<style>
<?php if (get_theme_mod('header_clr_enable', false) == true) : ?>
    /* Header Background */
    .navbar, a.search-icon,body.dark .navbar,body .six .header-logo.index5,body .header-sticky nav.navbar4{
        background-color: <?php echo get_theme_mod('header_background_color', '#ffffff'); ?>;
    }

    
        /* Site Title & Tagline */
        .site-title a,.dark .site-title a{
            color: <?php echo get_theme_mod('site_title_link_color', '#061018'); ?>;
        }
        .site-title a:hover,.dark .site-title a:hover,body.dark .seven .site-title a:hover{
            color: <?php echo get_theme_mod('site_title_link_hover_color', '#22a2c4'); ?>;
        }
        .site-description{
            color: <?php echo get_theme_mod('site_tagline_text_color', '#1c314c'); ?> !important;
        }
        .site-description:hover{
            color: <?php echo get_theme_mod('site_tagline_hover_color', '#22a2c4'); ?> !important;
        }
    <?php endif; ?>

    /* Sticky Header Color shceme */
    <?php if (get_theme_mod('sticky_header_clr_enable', false) == true) : ?>
        .header-sticky.stickymenu1, .header-sticky.stickymenu, .header-sticky.shrink1,body .six.stickymenu .header-logo.index5,body .header-sticky.stickymenu nav.navbar4
        {
            background-color: <?php echo get_theme_mod('sticky_header_bg_color', '#ffffff'); ?> !important;
        }
        .header-sticky.stickymenu1 .site-title a, .header-sticky.stickymenu .site-title a, .header-sticky.shrink1 .site-title a
        {
            color: <?php echo get_theme_mod('sticky_header_title_color', '#1c314c'); ?> !important;
        }
        .header-sticky.stickymenu1 .site-description, .header-sticky.stickymenu .site-description, .header-sticky.shrink1 .site-description
        {
            color: <?php echo get_theme_mod('sticky_header_subtitle_color', '#1c314c'); ?> !important;
        }
        .header-sticky.stickymenu1 .nav .nav-item .nav-link, .header-sticky.stickymenu .nav .nav-item .nav-link, .header-sticky.shrink1 .nav .nav-item .nav-link {
            color: <?php echo get_theme_mod('sticky_header_menus_link_color', '#1c314c'); ?> !important;
        }
        .header-sticky.stickymenu1 .nav .nav-item:hover .nav-link, .header-sticky.stickymenu1 .navbar.custom .nav .nav-item.active .nav-link:hover, .header-sticky.stickymenu .nav .nav-item:hover .nav-link, .header-sticky.stickymenu .navbar.custom .nav .nav-item.active .nav-link:hover , .header-sticky.shrink1 .nav .nav-item:hover .nav-link, .header-sticky.shrink1 .navbar.custom .nav .nav-item.active .nav-link:hover{
            color: <?php echo get_theme_mod('sticky_header_menus_link_hover_color', '#22a2c4'); ?> !important;
        }
        .header-sticky.stickymenu1 .nav .nav-item.active .nav-link, .header-sticky.stickymenu .nav .nav-item.active .nav-link, .header-sticky.shrink1 .nav .nav-item.active .nav-link {
            color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#22a2c4'); ?> !important;
        }
        /* Sticky Header Submenus */
        .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item, .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-menu, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item, .header-sticky.stickymenu .nav.navbar-nav .dropdown-menu, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item, .header-sticky.shrink1 .nav.navbar-nav .dropdown-menu {
            background-color: <?php echo get_theme_mod('sticky_header_submenus_background_color', '#061018'); ?>;
        }
        .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item {
            color: <?php echo get_theme_mod('sticky_header_submenus_link_color', '#ffffff'); ?>;
        }
        .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:hover, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:hover,  .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:hover {
            color: <?php echo get_theme_mod('sticky_header_submenus_link_hover_color', '#22a2c4'); ?>;
        }
        .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:focus, .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:hover, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:focus, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:hover, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:focus, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:hover
        {
            background-color: transparent;
        }
    <?php endif; ?>

    /* Primary Menu */
    <?php if (get_theme_mod('apply_menu_clr_enable', false) == true) : ?>
        .navbar.custom .nav .nav-item .nav-link,body.dark .navbar .nav .nav-item .nav-link {
            color: <?php echo get_theme_mod('menus_link_color', '#061018'); ?>;
        }
        .navbar.custom .nav .nav-item:hover .nav-link, .navbar.custom .nav .nav-item.active .nav-link:hover,body.dark .navbar .nav .nav-item .nav-link:hover,body .navbar.navbar5 .nav .nav-item .nav-link:hover,body.dark .navbar.navbar5 .nav .nav-item:hover .nav-link {
            color: <?php echo get_theme_mod('menus_link_hover_color', '#22a2c4'); ?>;
        }
        .navbar.custom .nav .nav-item.active .nav-link,body .navbar .nav .nav-item.active .nav-link,body.dark .navbar.navbar5 .nav .nav-item.active .nav-link{
            color: <?php echo get_theme_mod('menus_link_active_color', '#22a2c4'); ?>;
        }
        body .navbar.navbar4 .navbar-nav > li.active > a:after,body .navbar.navbar4 ul li > a:hover:after{
            background-color: <?php echo get_theme_mod('menus_link_active_color', '#22A2C4'); ?>;
        } 
        /* Submenus */
        .nav.navbar-nav .dropdown-item, .nav.navbar-nav .dropdown-menu {
            background-color: <?php echo get_theme_mod('submenus_background_color', '#ffffff'); ?>;
        }
        .nav.navbar-nav .dropdown-item {
            color: <?php echo get_theme_mod('submenus_link_color', '#212529'); ?>;
        }
        .nav.navbar-nav .dropdown-item:hover {
            color: <?php echo get_theme_mod('submenus_link_hover_color', '#22a2c4'); ?>;
        }
        body .navbar.navbar4 .navbar-nav > li.active > .dropdown-item:after,body .navbar.navbar4 ul li > .dropdown-item:hover:after{
            background-color: <?php echo get_theme_mod('submenus_link_hover_color', '#22a2c4'); ?>;
        } 
        .nav.navbar-nav .dropdown-item:focus, .nav.navbar-nav .dropdown-item:hover
        {
            background-color: transparent;
        }

    <?php endif; ?>

    /* Banner */
    .page-title h1{
        color: <?php echo get_theme_mod('banner_text_color', '#fff'); ?>!important;
    }

    /* Breadcrumb */
    <?php
    $enable_brd_link_clr_setting = get_theme_mod('enable_brd_link_clr_setting', false);
    if ($enable_brd_link_clr_setting == true):
        ?>
       .page-breadcrumb.text-center span a,nav.rank-math-breadcrumb a, .navxt-breadcrumb.text-center span a ,.navxt-breadcrumb  {
            color: <?php echo get_theme_mod('breadcrumb_title_link_color', '#ffffff'); ?> !important;
        }
        .page-breadcrumb.text-center span a:hover, nav.rank-math-breadcrumb a:hover,.navxt-breadcrumb.text-center span a:hover {
            color: <?php echo get_theme_mod('breadcrumb_title_link_hover_color', '#22a2c4'); ?> !important;
        }
    <?php endif; ?>

    /* After Menu Button */
     <?php
    $enable_after_menu_btn_clr_setting=get_theme_mod('enable_after_menu_btn_clr_setting',false);
    if($enable_after_menu_btn_clr_setting==true): ?>
    #wrapper .busicare_header_btn,body.dark #wrapper .busicare_header_btn {
    background-color: <?php echo get_theme_mod('after_menu_btn_bg_clr','#2d6ef8');?>
    }   
    #wrapper .busicare_header_btn,body.dark #wrapper .busicare_header_btn {
    color: <?php echo get_theme_mod('after_menu_btn_txt_clr','#ffffff');?>
    }
    #wrapper .busicare_header_btn:hover,body.dark #wrapper .busicare_header_btn:hover {
   background-color: <?php echo get_theme_mod('after_menu_btn_hover_clr','#000000');?>
    }
    <?php endif;?>  

<?php if(get_theme_mod('hp_color_skin','light')=='light'):?>
    /* Content */
    body h1:not(.cta_main .cta_content h1,.footer-sidebar h1){
        color: <?php echo get_theme_mod('h1_color', '#1c314c'); ?> ;
    }
    body .section-header h2, body h2:not(.footer-sidebar h2){
        color: <?php echo get_theme_mod('h2_color', '#1c314c'); ?>;
    }
    body h3:not(.footer-sidebar h3),.home-blog h3 a{
        color: <?php echo get_theme_mod('h3_color', '#1c314c'); ?>;
    }
    body .entry-header h4 > a:not(.single-post .entry-header .entry-title > a), body h4:not(.footer-sidebar h4) {
        color: <?php echo get_theme_mod('h4_color', '#1c314c'); ?>;
    }
    body .blog-author h5, body .comment-detail h5, body h5:not(.footer-sidebar h5), .section-space.services .entry-title a,body .section-space.services2 .entry-title a,body .section-space.services3 .entry-title a,body .section-space.services4 .entry-title a,body .post h5 {
        color: <?php echo get_theme_mod('h5_color', '#1c314c'); ?>;
    }

    body .product-price h5 > a{
        color: <?php echo get_theme_mod('h5_color', '#22a2c4'); ?>;
    }

    body h6:not(.footer-sidebar h6) {
        color: <?php echo get_theme_mod('h6_color', '#1c314c'); ?>;
    }
    p:not(.woocommerce-mini-cart__total,.cta-2 p,.footer-sidebar p,.site-description) {
        color: <?php echo get_theme_mod('p_color', '#777777'); ?>;
    }
<?php endif;?>
<?php if (get_theme_mod('apply_content_dark_enable', false) == true): ?>
   /* For Dark*/
    body.dark h1:not(.cta_main .cta_content h1,.footer-sidebar h1) {
        color: <?php echo get_theme_mod('h1_color', '#1c314c'); ?> ;
    }
    body.dark .section-header h2.section-title-two,body.dark .section-header h2.section-title,body.dark h2:not(.footer-sidebar h2,.slider-caption .title,.cta-2-title,.funfact .title), body.dark .post h2{
        color: <?php echo get_theme_mod('h2_color', '#1c314c'); ?>;
    }
    body.dark h3:not(.footer-sidebar h3,.single-post .entry-header h3.entry-title),body.dark .home-blog h3 a,body.dark .comment-form h3,body.dark .post h3:not(.single-post .post h3){
        color: <?php echo get_theme_mod('h3_color', '#1c314c'); ?>;
    }
    body.dark .entry-header h4 > a, body.dark h4:not(.footer-sidebar h4) {
        color: <?php echo get_theme_mod('h4_color', '#1c314c'); ?>;
    }
    body.dark .blog-author h5,body.dark .blog-author h5.post-by, body.dark .comment-detail h5, body.dark h5:not(.footer-sidebar h5),body.dark .section-space.services .entry-title a,body.dark .section-header .section-subtitle,body.dark .section-space.services2 .entry-title a,body.dark .section-space.services3 .entry-title a,body.dark .section-space.services4 .entry-title a,body.dark .post h5  {
        color: <?php echo get_theme_mod('h5_color', '#1c314c'); ?>;
    }

    body.dark .product-price h5 > a{
        color: <?php echo get_theme_mod('h5_color', '#22a2c4'); ?>;
    }

    body.dark h6:not(.footer-sidebar h6) {
        color: <?php echo get_theme_mod('h6_color', '#1c314c'); ?>;
    }
    body.dark p:not(.woocommerce-mini-cart__total,.bcslider-section p,.cta-2 p,.footer-sidebar p,.site-description) {
        color: <?php echo get_theme_mod('p_color', '#777777'); ?>;
    }

    <?php
    endif;
    if (get_theme_mod('apply_slider_clr_enable', false) == true): ?> 
    /* Slider Section */
    .bcslider-section .slider-caption .heading{
        color: <?php echo get_theme_mod('home_slider_subtitle_color', '#ffffff'); ?>;
    }
    .bcslider-section .slider-caption h2{
        color: <?php echo get_theme_mod('home_slider_title_color', '#ffffff'); ?>;
    }
    .bcslider-section .slider-caption .description
    {
        color: <?php echo get_theme_mod('home_slider_description_color', '#ffffff'); ?>;
    }
    
        .bcslider-section .btn-small.btn-default {
            border: 1px solid <?php echo get_theme_mod('slider_btn1_color', '#22a2c4'); ?>;
            background: <?php echo get_theme_mod('slider_btn1_color', '#22a2c4'); ?>;
        }
        .bcslider-section #slider-carousel .btn-small.btn-default:hover {
            border: 1px solid <?php echo get_theme_mod('slider_btn1_hover_color', '#ffffff'); ?>;
            background: <?php echo get_theme_mod('slider_btn1_hover_color', '#ffffff'); ?>;
        }
        .bcslider-section .btn-light {
            background: <?php echo get_theme_mod('slider_btn2_color', 'transparent'); ?>;
            /*border: 1px solid <?php echo get_theme_mod('slider_btn2_color', '#ffffff'); ?>;*/
        }
        .bcslider-section #slider-carousel .btn-light:hover {
            border: 1px solid <?php echo get_theme_mod('slider_btn2_hover_color', '#22a2c4'); ?>;
            background: <?php echo get_theme_mod('slider_btn2_hover_color', '#22a2c4'); ?>;
        }
    <?php endif; ?>
    /* Testimonial Section */


    <?php if (get_theme_mod('apply_testimonial_clr_enable', false) == true) { ?> 
        .testimonial .section-title
        {
            color: <?php echo get_theme_mod('home_testi_title_color', '#ffffff'); ?> !important;
        }
        .testimonial .title span {
            color: <?php echo get_theme_mod('home_testi_subtitle_color', '#22a2c4'); ?>;
        }
        .testmonial-block .avatar img
        {
            border: 5px solid <?php echo get_theme_mod('home_testi_img_border_color', '#ffffff'); ?> !important;
            box-shadow: <?php echo get_theme_mod('home_testi_img_border_color', '#ffffff'); ?> 0px 0px 0px 1px !important;
        }
        .testimonial .entry-content .description
        {
            color: <?php echo get_theme_mod('testimonial_description_color', '#ffffff'); ?> !important;
        }
        .testimonial .testmonial-block .name
        {
            color: <?php echo get_theme_mod('testi_clients_name_color', '#ffffff'); ?>;
        }
        .testimonial .testmonial-block .designation
        {
            color: <?php echo get_theme_mod('testi_clients_designation_color', '#ffffff'); ?>;
        }
    <?php } else { ?>
        .testimonial .section-title
        {
            color: '#ffffff';
        }

        .testimonial .entry-content .description
        {
            color: '#ffffff';
        }
    <?php } ?>
    /* CTA 1 SECTION */

    <?php if (get_theme_mod('apply_cta1_clr_enable', false) == true): ?>
        .cta_main .cta_content h1 {
            color: <?php echo get_theme_mod('home_cta1_title_color', '#ffffff'); ?>;
        }
        .cta_main .cta_content .btn-light {
            background: <?php echo get_theme_mod('home_cta1_btn_color', '#ffffff'); ?>  !important;
        }

        .cta_main .cta_content .btn-light:hover {
            background: <?php echo get_theme_mod('home_cta1_btn_hover_color', '#1c314c'); ?>  !important;
        }

        .cta_content, .cta_main{
            background-color: <?php echo get_theme_mod('cta1_bg_section_color', '#22a2c4'); ?>  !important;
        }

    <?php endif; ?>

    /* CTA 2 SECTION */
    .cta-2 .title{
        color: #ffffff;
    }
    .cta-2 p{
        color: #ffffff;
    }

    <?php if (get_theme_mod('apply_cta2_clr_enable', false) == true): ?>
        .cta-2 .title,body.dark .cta-2 .title{
            color: <?php echo get_theme_mod('home_cta2_title_color', '#ffffff'); ?>;
        }
        .cta-2 p,body.dark .cta-2 p{
            color: <?php echo get_theme_mod('home_cta2_subtitle_color', '#ffffff'); ?>;
        }
        .cta-2 .btn-default {
            background: <?php echo get_theme_mod('home_cta2_btn1_color', '#22a2c4  '); ?>  !important;
            border: 1px solid <?php echo get_theme_mod('home_cta2_btn1_color', '#22a2c4  '); ?> !important;
        }
        .cta-2 .btn-default:hover{
            background: <?php echo get_theme_mod('home_cta2_btn1_hover_color', '#ffffff'); ?>  !important;
            border: 1px solid <?php echo get_theme_mod('home_cta2_btn1_hover_color', '#ffffff'); ?>!important;
        }

        .cta-2 .btn-light{
            background: <?php echo get_theme_mod('home_cta2_btn2_color', 'transparent'); ?>  !important;
            border: 1px solid <?php
            if (get_theme_mod('home_cta2_btn2_color', 'transparent') == 'transparent') {
                echo '#ffffff';
            } else {
                get_theme_mod('home_cta2_btn2_color', 'transparent');
            }
            ?>;
        }

        .cta-2 .btn-light:hover {
            background: <?php echo get_theme_mod('home_cta2_btn2_hover_color', '#22a2c4  '); ?>  !important;
            border: 1px solid <?php echo get_theme_mod('home_cta2_btn2_hover_color', '#22a2c4  '); ?>;
        }
    <?php endif; ?>

    /* Funfact SECTION */
    .funfact .title {
        color: '#ffffff';
    }

    .funfact .funfact-inner .funfact-title, .funfact .funfact-inner .description.text-white{
        color: '#ffffff';
    }
    <?php if (get_theme_mod('apply_funfact_clr_enable', false) == true): ?>
        .funfact-block .funfact-text h2 {
            color: <?php echo get_theme_mod('funfact_title_color', '#FFFFFF'); ?>!important;
        }

        .funfact .funfact-inner .funfact-title{
            color: <?php echo get_theme_mod('funfact_count_color', '#ffffff'); ?> !important;
        }

        .funfact .description.text-white{
            color: <?php echo get_theme_mod('funfact_count_desc_color', '#ffffff'); ?> !important;
        }

    <?php endif; ?>


    /* Blog Page */
    <?php if ((get_theme_mod('apply_blg_clr_enable', false) == true) && (!is_single())): ?> 
        .standard-view .entry-title a, .entry-title.template-blog-grid-view a, .entry-title.template-blog-grid-view-sidebar a, .entry-title.template-blog-list-view a, .list-view .entry-title a, .entry-title.blog-masonry-two-col a, .entry-title.blog-masonry-three-col a, .entry-title.blog-masonry-four-col a, .section-module.blog .entry-title a{
            color: <?php echo get_theme_mod('blog_post_page_title_color', '#1c314c'); ?>;
        }
        .blog .entry-header .entry-title a:hover:not(.home-blog-title) {
            color: <?php echo get_theme_mod('blog_post_page_title_hover_color', '#22a2c4'); ?>!important;
        }
        .blog .entry-header .entry-meta a, .blog .entry-meta > span a, .blog .entry-meta a{
            color: <?php echo get_theme_mod('blog_post_page_meta_link_color', '#777777'); ?>;
        }
        .blog .entry-header .entry-meta a:hover,.blog .entry-meta > span a:hover, .blog .entry-meta a:hover{
            color: <?php echo get_theme_mod('blog_post_page_meta_link_hover_color', '#22a2c4'); ?>;
        }

        .section-module.blog .entry-meta .cat-links a, .section-module.blog .standard-view .entry-meta .author a, .section-module.blog .list-view .entry-meta .author a, .section-module.blog.grid-view .entry-meta .author a, .section-module.blog .entry-meta .comment-links a::before, .entry-meta .posted-on a, .entry-meta .comment-links a, .section-module.blog .entry-meta .comment-links a::before
        {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_color', '#777777'); ?>;
        }
        .section-module.blog .entry-meta .cat-links a:hover, .section-module.blog .standard-view .entry-meta .author a:hover, .section-module.blog .list-view .entry-meta .author a:hover, .section-module.blog .entry-meta .comment-links a:hover::before, .section-module.blog .entry-meta a:hover, .section-module.blog.grid-view .entry-meta .author a:hover
        {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_hover_color', '#22a2c4'); ?>;
        }
    <?php endif; ?>

    /* Single Post/Page */
    <?php if (get_theme_mod('apply_blg_single_clr_enable', false) == true && (is_single())): ?>
        .single-post .standard-view .entry-title,body.dark.single-post .standard-view h3.entry-title{ 
            color: <?php echo get_theme_mod('single_post_page_title_color', '#1c314c'); ?>;
        }
        .single-post .standard-view .entry-title a:hover,body.dark.single-post .standard-view h3.entry-title a:hover{
            color: <?php echo get_theme_mod('single_post_page_title_hover_color', '#22a2c4'); ?>;
        }
        .single-post .entry-meta a, .blog-single .entry-meta .cat-links a, .section-module.blog .standard-view .entry-meta .author a, .section-module.blog .list-view .entry-meta .author a, .blog-single .entry-meta .comment-links a::before{
            color: <?php echo get_theme_mod('single_post_page_meta_link_color', '#777777'); ?>;
        }
        .single-post .entry-meta a:hover, .single-post .entry-meta .comment-links a:hover, .single-post .entry-meta .posted-on a:hover, .blog-single .entry-meta .cat-links a:hover, .section-module.blog .standard-view .entry-meta .author a:hover, .section-module.blog .list-view .entry-meta .author a:hover, .blog-single .entry-meta .comment-links a:hover::before{
            color: <?php echo get_theme_mod('single_post_page_meta_link_hover_color', '#22a2c4'); ?>;
        }
    <?php endif; ?>

    /* Sidebar */
    <?php if (get_theme_mod('apply_sibar_link_hover_clr_enable', false) == true): ?>
        body .sidebar .widget .widget-title,body .sidebar .wp-block-search .wp-block-search__label,body .sidebar .widget.widget_block h1,body .sidebar .widget.widget_block h2,body .sidebar .widget.widget_block h3,body .sidebar .widget.widget_block h4,body .sidebar .widget.widget_block h5,body .sidebar .widget.widget_block h6 {
            color: <?php echo get_theme_mod('sidebar_widget_title_color', '#ffffff'); ?>;
        }
        body .sidebar p {
            color: <?php echo get_theme_mod('sidebar_widget_text_color', '#777777'); ?>!important;
        }
        body .sidebar a,body .sidebar .widget li:before,body.dark .sidebar a{
            color: <?php echo get_theme_mod('sidebar_widget_link_color', '#777777'); ?> !important;
        }
        body .sidebar.s-l-space .sidebar a:hover, 
        body .sidebar .widget a:hover, 
        body .sidebar .widget a:focus{
            color: <?php echo get_theme_mod('sidebar_widget_link_hover_color', '#22a2c4'); ?> !important;
        }
    <?php endif; ?>

    /* Footer Widgets */
    .footer-sidebar .widget_text p{
        color: #ffffff;
    }
    <?php if (get_theme_mod('apply_ftrsibar_link_hover_clr_enable', false) == true) { ?>
        body .site-footer {
            background-color: <?php echo get_theme_mod('footer_widget_background_color', '#21202e'); ?>;
        }
        body .site-footer .widget-title,body .footer-sidebar .wp-block-search .wp-block-search__label,body .footer-sidebar .widget.widget_block h1,body .footer-sidebar .widget.widget_block h2,body .footer-sidebar .widget.widget_block h3,body .footer-sidebar .widget.widget_block h4,body .footer-sidebar .widget.widget_block h5,body .footer-sidebar .widget.widget_block h6 {
            color: <?php echo get_theme_mod('footer_widget_title_color', '#22a2c4'); ?> !important;
        }
        body .footer-sidebar.footer-typo p,  body .footer-sidebar.footer-typo .widget:not(.widget_calendar), .footer-sidebar.footer-typo .widget_text p {
            color: <?php echo get_theme_mod('footer_widget_text_color', '#ffffff'); ?>!important;
        }
        body .footer-sidebar .widget:not(.widget_calendar) a, body .footer-sidebar .widget_recent_entries .post-date, body .footer-sidebar .widget:not(.widget_calendar) li  {
            color: <?php echo get_theme_mod('footer_widget_link_color', '#ffffff'); ?>;
        }
        body .footer-sidebar .widget:not(.widget_calendar) a:hover, body .footer-sidebar .widget:not(.widget_calendar) li:hover{
            color: <?php echo get_theme_mod('footer_widget_link_hover_color', '#22a2c4'); ?>;
        }
    <?php } else { ?>
        .site-footer p {
            color: #fff;
        }
    <?php } ?>

    /* Footer Bar */
        body .site-info {
        border-top: <?php echo get_theme_mod('footer_bar_border',0);?>px <?php echo get_theme_mod('footer_border_style','solid');?> <?php echo get_theme_mod('busicare_footer_border_clr','#fff');?>
        }
        body .site-info .footer-sidebar .widget-title,body .site-footer .site-info .widget-title{
        color: <?php echo get_theme_mod('advance_footer_bar_title_color','#fff'); ?>!important;
    }
        <?php if (get_theme_mod('apply_foot_hover_anchor_enable', false) == true):
        ?>
        
        body .site-info {
            background-color: <?php echo get_theme_mod('footer_bar_background_color', '#22a2c4'); ?>;
        }
        body .site-info p, body .site-info span, .footer-sidebar .widget_text p,body .site-info .footer-sidebar p{
            color: <?php echo get_theme_mod('footer_bar_text_color', '#ffffff'); ?>;
        }
        body footer.site-footer .site-info a, body footer.site-footer .site-info a{
            color: <?php echo get_theme_mod('footer_bar_link_color', '#1c314c'); ?> ;
        }

        body .site-info a:hover,body .site-info .widget_recent_entries li a:hover,body .site-info .footer-sidebar li:hover a,body .site-info .footer-sidebar .widget:not(.widget_calendar) a:hover,body footer.site-footer .site-info a:hover, body footer.site-footer .site-info a:hover {
            color: <?php echo get_theme_mod('footer_bar_link_hover_color', '#ffffff'); ?>;
        }
    <?php endif; ?>

    .header-sticky.stickymenu1, .header-sticky.stickymenu, .header-sticky.shrink1,.navbar6.navbar.header-sticky.stickymenu1, body .navbar6.navbar.header-sticky.stickymenu, .navbar6.navbar.header-sticky.shrink1
    {
        opacity: <?php echo get_theme_mod('sticky_header_opacity', '1.0'); ?>;
        <?php if (get_theme_mod('sticky_header_height', 0) > 0): ?>
            padding-top: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
            padding-bottom: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
        <?php endif; ?>
    }
body .header-sticky.six.stickymenu, body .header-sticky.five.stickymenu{
    padding-top: 0;
    padding-bottom: 0;
}
body .header-sticky.six.stickymenu .header-logo.index5,body .header-sticky.stickymenu .header-logo.index4 {
padding-top: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
}
body .header-sticky.six.stickymenu nav.navbar5,body .header-sticky.stickymenu nav.navbar4 {
    padding-bottom: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
}    
    .custom-logo{width: <?php echo esc_html(get_theme_mod('busicare_logo_length',''));?>px; height: auto;}
body .navbar-brand.sticky-logo img{width: <?php echo esc_html(get_theme_mod('busicare_logo_length',''));?>px; height: auto !important;}
body .navbar-brand.sticky-logo-mbl img{width: <?php echo esc_html(get_theme_mod('busicare_logo_length',''));?>px; height: auto !important;}
.busicare_header_btn{ -webkit-border-radius: <?php echo esc_html(get_theme_mod('after_menu_btn_border',0));?>px;border-radius: <?php echo esc_html(get_theme_mod('after_menu_btn_border',0));?>px;}
@media (min-width: 991px)
{
    body.boxed .navbar6 .header-lt::before {
    width:18vw;
}
}
.dark .seven .site-title a:hover{
    color: #fff;
}
</style>

<?php
if(get_theme_mod('blog_sidebar_layout','right')=='stretched') { ?>
    <style>
        body #content .section-space-page.blog.stretched .container {
            max-width: 100%;
            padding: 0;
            margin: 0;
        }
        
    </style>
<?php 
}

if(get_theme_mod('single_post_sidebar_layout','right')=='stretched') { ?>
    <style>
        body.single-post #content .section-module.blog.stretched .container {
            max-width: 100%;
            padding: 0;
            margin: 0;
        }
    </style>
<?php }

if(get_theme_mod('page_sidebar_layout','right')=='stretched') { ?>
    <style>
        body #content .section-space-page.page.stretched .container {
            max-width: 100%;
            padding: 0;
            margin: 0;
        }
        body #content .section-module.blog.woocommerce.section-space-page.stretched .container {
            max-width: 100%;
            padding: 0;
            margin: 0;
        }
    </style>
<?php } ?>