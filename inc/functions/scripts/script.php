<?php

function busicare_plus_enqueue_script() {

    $suffix = ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) ? '' : '.min';
    wp_enqueue_style('animate', BUSICAREP_PLUGIN_URL . '/inc/css/animate.css');
    //wp_enqueue_style('owl', BUSICAREP_PLUGIN_URL . '/inc/css/owl.carousel.css');
    wp_enqueue_style('lightbox', BUSICAREP_PLUGIN_URL . 'inc/css/lightbox.css');
    $theme = wp_get_theme();
    if (get_theme_mod('custom_color_enable') == true) {
        add_action('wp_footer', 'busicare_plus_custom_light');
    } else {
        if('BusiCare Dark' == $theme->name)
        {
            $class = get_theme_mod('theme_color', 'green.css');
            wp_enqueue_style('busicare-dark-default-style', BUSICAREP_PLUGIN_URL . '/inc/css/' . $class);
        }
        else
        {
           $class = get_theme_mod('theme_color', 'default.css');
            wp_enqueue_style('busicarep-default', BUSICAREP_PLUGIN_URL . '/inc/css/' . $class);
        }
        
        
    }
    require_once('custom_style.php');
    $theme = wp_get_theme();
    if('BusiCare Dark' == $theme->name) {$bc_theme_skin='dark';}
    else{$bc_theme_skin='light';}
    if(get_theme_mod('hp_color_skin',$bc_theme_skin)=='dark')
    {
    wp_enqueue_style('customize-css', BUSICAREP_PLUGIN_URL . '/inc/css/dark.css');
    }

    //js file
    wp_enqueue_script('wow', BUSICAREP_PLUGIN_URL . '/inc/js/wow.js', array('jquery'), '', true);
   // wp_enqueue_script('owl', BUSICAREP_PLUGIN_URL. '/inc/js/owl.carousel' . $suffix . '.js', array('jquery'), '', true);
    wp_enqueue_script('grid-masonary', BUSICAREP_PLUGIN_URL.'/inc/js/grid-mansory.js', array('jquery'), '', true);
    wp_enqueue_script('busicare-mp-masonry-js', BUSICAREP_PLUGIN_URL . '/inc/js/masonry/mp.mansory.js');
    wp_enqueue_script('busicare-custom-js', BUSICAREP_PLUGIN_URL . '/inc/js/custom.js', array('jquery'), '', true);
    wp_enqueue_script('lightbox', BUSICAREP_PLUGIN_URL . '/inc/js/lightbox/lightbox-2.6.min.js', array('jquery'), '', true);
    wp_enqueue_script('imgLoad', BUSICAREP_PLUGIN_URL.'/inc/js/img-loaded.js', array('jquery'), '', true);
    wp_enqueue_script('busicare-plus-video-slider-js', BUSICAREP_PLUGIN_URL. '/inc/js/jquery.mb.YTPlayer.js');
    
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'busicare_plus_enqueue_script');

function busicare_plus_admin_enqueue_scripts() {
    wp_enqueue_style('customize-css', BUSICAREP_PLUGIN_URL . '/inc/css/customize.css');
}

add_action('admin_enqueue_scripts', 'busicare_plus_admin_enqueue_scripts');

/* slider heading */

function busicare_plus_sliderHeading() {
    $themeCor = get_theme_mod('theme_color', 'default.css');
    if (explode('.', $themeCor)[0] == 'default') {
        $clrsldr = '#22a2c4';
    } elseif (explode('.', $themeCor)[0] == 'green') {
        $clrsldr = '#82b440';
    } elseif (explode('.', $themeCor)[0] == 'red') {
        $clrsldr = '#ce1b28';
    } elseif (explode('.', $themeCor)[0] == 'purple') {
        $clrsldr = '#6974ea';
    } elseif (explode('.', $themeCor)[0] == 'orange') {
        $clrsldr = '#ee591f';
    } elseif (explode('.', $themeCor)[0] == 'yellow') {
        $clrsldr = '#ffba00';
    }
    if (!is_rtl()) {
        $clrsldr1 = ".slider-caption .heading { border-left: 4px solid $clrsldr; }";
    } else {
        $clrsldr1 = ".slider-caption .heading { border-right: 4px solid $clrsldr; }";
    }
    echo "<style>$clrsldr1</style>";
}

add_action('wp_head', 'busicare_plus_sliderHeading');
/* slider heading */

// slider custom script
function busicare_plus_add_theme_scripts() {
    $animation = get_theme_mod('animation', '');

    if ($animation == '') {
        $animate_In = '';
        $animate_Out = '';
    } else {
        $animate_In = 'fadeIn';
        $animate_Out = 'fadeOut';
    }
    $slider_autoplay = get_theme_mod('slider_autoplay', true);
    $slider_loop = get_theme_mod('slider_loop', true);
    $slider_rewind = get_theme_mod('slider_rewind', true);
    $animation_speed = get_theme_mod('animation_speed', 3000);
    $slider_smooth_speed = get_theme_mod('slider_smooth_speed', 1000);
    $slider_nav_style = get_theme_mod('slider_nav_style', 'navigation');
    $isRTL = (is_rtl()) ? (bool) true : (bool) false;
    $settings = array('animateIn' => $animate_In, 'animateOut' => $animate_Out, 'animationSpeed' => $animation_speed, 'smoothSpeed' => $slider_smooth_speed,'slideAutoplay'=>$slider_autoplay,'slideLoop'=>$slider_loop,'slideRewind'=>$slider_rewind,'slider_nav_style' => $slider_nav_style, 'rtl' => $isRTL);

    wp_register_script('busicare-slider', BUSICAREP_PLUGIN_URL . '/inc/js/front-page/slider.js', array('jquery'));
    wp_localize_script('busicare-slider', 'slider_settings', $settings);
    wp_enqueue_script('busicare-slider');
}

add_action('wp_enqueue_scripts', 'busicare_plus_add_theme_scripts');


add_action('wp_footer', 'busicare_plus_custom_script');

function busicare_plus_custom_script() {
    $col = 6;

    if (is_page_template('template/template-blog-masonry-two-column.php')) {

        $col = 6;
    } elseif (is_page_template('template/template-blog-masonry-three-column.php')) {

        $col = 4;
    } elseif (is_page_template('template/template-blog-masonry-four-column.php')) {

        $col = 3;
    }
    ?>
    <script>
        jQuery(document).ready(function (jQuery) {
            jQuery("#blog-masonry").mpmansory(
                    {
                        childrenClass: 'item', // default is a div
                        columnClasses: 'padding', //add classes to items
                        breakpoints: {
                            lg: <?php echo $col; ?>, //Change masonry column here like 2, 3, 4 column
                            md: 6,
                            sm: 6,
                            xs: 12
                        },
                        distributeBy: {order: false, height: false, attr: 'data-order', attrOrder: 'asc'}, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
                        onload: function (items) {
                            //make somthing with items
                        }
                    }
            );
        });
    </script>
    <?php
}

//Load script at admin side
function busicare_admin_scripts() {
 wp_enqueue_script( 'busicare-admin-script', BUSICAREP_PLUGIN_URL . 'inc/js/admin.js', array('jquery'));
}
add_action( 'customize_controls_enqueue_scripts', 'busicare_admin_scripts');