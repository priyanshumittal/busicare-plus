<?php
if (!class_exists('WP_Customize_Control')) {
    return null;
}

class busicare_plus_Repeater extends WP_Customize_Control {

    public $id;
    private $boxtitle = array();
    private $add_field_label = array();
    private $customizer_repeater_title_control = false;
    private $customizer_repeater_subtitle_control = false;
    private $customizer_repeater_button_text_control = false;
    private $customizer_repeater_link_control = false;
    private $customizer_repeater_slide_format = false;
    private $customizer_repeater_video_url_control = false;
    private $customizer_repeater_image_control = false;
    private $customizer_repeater_icon_control = false;
    private $customizer_repeater_color_control = false;
    private $customizer_repeater_text_control = false;
    private $customizer_repeater_user_name_control = false;
    private $customizer_repeater_member_name_control = false;
    private $customizer_repeater_designation_control = false;
    private $customizer_repeater_shortcode_control = false;
    private $customizer_repeater_repeater_control = false;
    private $customizer_repeater_checkbox_control = false;
    private $customizer_repeater_price_control = false;
    private $customizer_repeater_features_control = false;
    private $customizer_repeater_price_heighlight = false;
    private $customizer_repeater_abtsliderbutton_text_control = false;
    private $customizer_repeater_abtbutton_text_control = false;
    private $customizer_repeater_abt_slider_link_control = false;
    private $customizer_repeater_abtlink_control = false;
    private $customizer_repeater_abt_slider_checkbox_control = false;
    private $customizer_repeater_abtcheckbox_control = false;
    private $customizer_repeater_sidebarcheckbox_control = false;
    private $customizer_repeater_slider_caption_aligment_control = false;
    private $customizer_icon_container = '';
    private $allowed_html = array();

    /* Class constructor */

    public function __construct($manager, $id, $args = array()) {
        parent::__construct($manager, $id, $args);
        /* Get options from customizer.php */
        $this->add_field_label = esc_html__('Add new field', 'busicare-plus');
        if (!empty($args['add_field_label'])) {
            $this->add_field_label = $args['add_field_label'];
        }

        $this->boxtitle = esc_html__('Customizer Repeater', 'busicare-plus');
        if (!empty($args['item_name'])) {
            $this->boxtitle = $args['item_name'];
        } elseif (!empty($this->label)) {
            $this->boxtitle = $this->label;
        }

        if (!empty($args['customizer_repeater_image_control'])) {
            $this->customizer_repeater_image_control = $args['customizer_repeater_image_control'];
        }


        if (!empty($args['customizer_repeater_icon_control'])) {
            $this->customizer_repeater_icon_control = $args['customizer_repeater_icon_control'];
        }

        if (!empty($args['customizer_repeater_color_control'])) {
            $this->customizer_repeater_color_control = $args['customizer_repeater_color_control'];
        }

        if (!empty($args['customizer_repeater_title_control'])) {
            $this->customizer_repeater_title_control = $args['customizer_repeater_title_control'];
        }

        if (!empty($args['customizer_repeater_subtitle_control'])) {
            $this->customizer_repeater_subtitle_control = $args['customizer_repeater_subtitle_control'];
        }

        if (!empty($args['customizer_repeater_text_control'])) {
            $this->customizer_repeater_text_control = $args['customizer_repeater_text_control'];
        }

        if (!empty($args['customizer_repeater_designation_control'])) {
            $this->customizer_repeater_designation_control = $args['customizer_repeater_designation_control'];
        }

        if (!empty($args['customizer_repeater_user_name_control'])) {
            $this->customizer_repeater_user_name_control = $args['customizer_repeater_user_name_control'];
        }

        if (!empty($args['customizer_repeater_member_name_control'])) {
            $this->customizer_repeater_member_name_control = $args['customizer_repeater_member_name_control'];
        }

        if (!empty($args['customizer_repeater_button_text_control'])) {
            $this->customizer_repeater_button_text_control = $args['customizer_repeater_button_text_control'];
        }

        if (!empty($args['customizer_repeater_link_control'])) {
            $this->customizer_repeater_link_control = $args['customizer_repeater_link_control'];
        }

        if (!empty($args['customizer_repeater_checkbox_control'])) {
            $this->customizer_repeater_checkbox_control = $args['customizer_repeater_checkbox_control'];
        }

        if (!empty($args['customizer_repeater_abtsliderbutton_text_control'])) {
            $this->customizer_repeater_abtsliderbutton_text_control = $args['customizer_repeater_abtsliderbutton_text_control'];
        }

        if ( ! empty( $args['customizer_repeater_sidebarcheckbox_control'] ) ) {
            $this->customizer_repeater_sidebarcheckbox_control = $args['customizer_repeater_sidebarcheckbox_control'];
        }

        if (!empty($args['customizer_repeater_abtbutton_text_control'])) {
            $this->customizer_repeater_abtbutton_text_control = $args['customizer_repeater_abtbutton_text_control'];
        }

        if (!empty($args['customizer_repeater_abt_slider_link_control'])) {
            $this->customizer_repeater_abt_slider_link_control = $args['customizer_repeater_abt_slider_link_control'];
        }

        if (!empty($args['customizer_repeater_abtlink_control'])) {
            $this->customizer_repeater_abtlink_control = $args['customizer_repeater_abtlink_control'];
        }

        if (!empty($args['customizer_repeater_abt_slider_checkbox_control'])) {
            $this->customizer_repeater_abt_slider_checkbox_control = $args['customizer_repeater_abt_slider_checkbox_control'];
        }

        if (!empty($args['customizer_repeater_abtcheckbox_control'])) {
            $this->customizer_repeater_abtcheckbox_control = $args['customizer_repeater_abtcheckbox_control'];
        }
        if (!empty($args['customizer_repeater_slider_caption_aligment_control'])) {
            $this->customizer_repeater_slider_caption_aligment_control = $args['customizer_repeater_slider_caption_aligment_control'];
        }
        if (!empty($args['customizer_repeater_slide_format'])) {
            $this->customizer_repeater_slide_format = $args['customizer_repeater_slide_format'];
        }

        if (!empty($args['customizer_repeater_price_heighlight'])) {
            $this->customizer_repeater_price_heighlight = $args['customizer_repeater_price_heighlight'];
        }



        if (!empty($args['customizer_repeater_video_url_control'])) {
            $this->customizer_repeater_video_url_control = $args['customizer_repeater_video_url_control'];
        }

        if (!empty($args['customizer_repeater_price_control'])) {
            $this->customizer_repeater_price_control = $args['customizer_repeater_price_control'];
        }

        if (!empty($args['customizer_repeater_features_control'])) {
            $this->customizer_repeater_features_control = $args['customizer_repeater_features_control'];
        }


        if (!empty($args['customizer_repeater_shortcode_control'])) {
            $this->customizer_repeater_shortcode_control = $args['customizer_repeater_shortcode_control'];
        }

        if (!empty($args['customizer_repeater_repeater_control'])) {
            $this->customizer_repeater_repeater_control = $args['customizer_repeater_repeater_control'];
        }


        if (!empty($id)) {
            $this->id = $id;
        }

        if (file_exists(BUSICAREP_PLUGIN_DIR . '/inc/functions/customizer-repeater/inc/icons.php')) {
            $this->customizer_icon_container = BUSICAREP_PLUGIN_DIR . '/inc/functions/customizer-repeater/inc/icons.php';
        }

        $allowed_array1 = wp_kses_allowed_html('post');
        $allowed_array2 = array(
            'input' => array(
                'type' => array(),
                'class' => array(),
                'placeholder' => array()
            )
        );

        $this->allowed_html = array_merge($allowed_array1, $allowed_array2);
    }

    /* Enqueue resources for the control */

    public function enqueue() {
        wp_enqueue_style('busicare-font-awesome',  get_template_directory_uri(). '/css/font-awesome/css/all.min.css', array(), 999);

        wp_enqueue_style('busicare_customizer-repeater-admin-stylesheet', BUSICAREP_PLUGIN_URL . '/inc/functions/customizer-repeater/css/admin-style.css', array(), 999);

        wp_enqueue_style('wp-color-picker');

        wp_enqueue_script('busicare_customizer-repeater-script', BUSICAREP_PLUGIN_URL . '/inc/functions/customizer-repeater/js/customizer_repeater.js', array('jquery', 'jquery-ui-draggable', 'wp-color-picker'), 999, true);

        wp_enqueue_script('busicare_customizer-repeater-fontawesome-iconpicker', BUSICAREP_PLUGIN_URL . '/inc/functions/customizer-repeater/js/fontawesome-iconpicker.js', array('jquery'), 999, true);

        wp_enqueue_style('busicare_customizer-repeater-fontawesome-iconpicker-script', BUSICAREP_PLUGIN_URL . '/inc/functions/customizer-repeater/css/fontawesome-iconpicker.min.css', array(), 999);
    }

    public function render_content() {

        /* Get default options */
        $this_default = json_decode($this->setting->default);

        /* Get values (json format) */
        $values = $this->value();

        /* Decode values */
        $json = json_decode($values);

        if (!is_array($json)) {
            $json = array($values);
        }
        ?>

        <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
        <div class="customizer-repeater-general-control-repeater customizer-repeater-general-control-droppable">
            <?php
            if (( count($json) == 1 && '' === $json[0] ) || empty($json)) {
                if (!empty($this_default)) {
                    $this->iterate_array($this_default);
                    ?>
                    <input type="hidden"
                           id="customizer-repeater-<?php echo esc_attr($this->id); ?>-colector" <?php esc_attr($this->link()); ?>
                           class="customizer-repeater-colector"
                           value="<?php echo esc_textarea(json_encode($this_default)); ?>"/>
                <?php
            } else {
                $this->iterate_array();
                ?>
                    <input type="hidden"
                           id="customizer-repeater-<?php echo esc_attr($this->id); ?>-colector" <?php esc_attr($this->link()); ?>
                           class="customizer-repeater-colector"/>
                <?php
            }
        } else {
            $this->iterate_array($json);
            ?>
                <input type="hidden" id="customizer-repeater-<?php echo esc_attr($this->id); ?>-colector" <?php esc_attr($this->link()); ?>
                       class="customizer-repeater-colector" value="<?php echo esc_textarea($this->value()); ?>"/>
            <?php }
        ?>
        </div>
        <button type="button" class="button add_field customizer-repeater-new-field">
        <?php echo esc_html($this->add_field_label); ?>
        </button>
        <?php
    }

    private function iterate_array($array = array()) {

        /* Counter that helps checking if the box is first and should have the delete button disabled */
        $it = 0;
        if (!empty($array)) {
            foreach ($array as $icon) {
                ?>
                <div class="customizer-repeater-general-control-repeater-container customizer-repeater-draggable">
                    <div class="customizer-repeater-customize-control-title">
                        <?php echo esc_html($this->boxtitle) ?>
                    </div>
                    <div class="customizer-repeater-box-content-hidden">
                        <?php
                        $choice = $image_url = $icon_value = $title = $subtitle = $text = $slide_format = $sidebar_check= $link = $clientname = $membername = $designation = $home_slider_caption = $abtbutton_text = $abtsliderbutton_text = $abtlink = $abtsliderlink = $abtopen_new_tab = $abtslider_open_new_tab = $button = $open_new_tab = $shortcode = $repeater = $color = $video_url = $price = $features = $price_heighlight = '';
                        if (!empty($icon->id)) {
                            $id = $icon->id;
                        }
                        if (!empty($icon->choice)) {
                            $choice = $icon->choice;
                        }
                        if (!empty($icon->image_url)) {
                            $image_url = $icon->image_url;
                        }
                        if (!empty($icon->icon_value)) {
                            $icon_value = $icon->icon_value;
                        }
                        if (!empty($icon->color)) {
                            $color = $icon->color;
                        }
                        if (!empty($icon->title)) {
                            $title = $icon->title;
                        }

                        if (!empty($icon->clientname)) {
                            $clientname = $icon->clientname;
                        }

                        if (!empty($icon->membername)) {
                            $membername = $icon->membername;
                        }

                        if (!empty($icon->designation)) {
                            $designation = $icon->designation;
                        }

                        if (!empty($icon->subtitle)) {
                            $subtitle = $icon->subtitle;
                        }
                        if (!empty($icon->text)) {
                            $text = $icon->text;
                        }
                        if (!empty($icon->video_url)) {
                            $video_url = $icon->video_url;
                        }

                        if (!empty($icon->slide_format)) {
                            $slide_format = $icon->slide_format;
                        }

                        if (!empty($icon->button_text)) {
                            $button = $icon->button_text;
                        }
                        if (!empty($icon->abtsliderbutton_text)) {
                            $abtsliderbutton_text = $icon->abtsliderbutton_text;
                        }
                        if (!empty($icon->abtbutton_text)) {
                            $abtbutton_text = $icon->abtbutton_text;
                        }
                        if (!empty($icon->home_slider_caption)) {
                            $home_slider_caption = $icon->home_slider_caption;
                        }
                        if (!empty($icon->link)) {
                            $link = $icon->link;
                        }
                        if (!empty($icon->abtsliderlink)) {
                            $abtsliderlink = $icon->abtsliderlink;
                        }
                        if (!empty($icon->abtlink)) {
                            $abtlink = $icon->abtlink;
                        }
                        if (!empty($icon->shortcode)) {
                            $shortcode = $icon->shortcode;
                        }

                        if (!empty($icon->social_repeater)) {
                            $repeater = $icon->social_repeater;
                        }

                        if (!empty($icon->open_new_tab)) {
                            $open_new_tab = $icon->open_new_tab;
                        }

                        if (!empty($icon->abtslider_open_new_tab)) {
                            $abtslider_open_new_tab = $icon->abtslider_open_new_tab;
                        }

                        if (!empty($icon->abtopen_new_tab)) {
                            $abtopen_new_tab = $icon->abtopen_new_tab;
                        }

                        if(!empty($icon->sidebar_check)){
                            $sidebar_check = $icon->sidebar_check;
                        }
                        
                        if (!empty($icon->price)) {
                            $price = $icon->price;
                        }

                        if (!empty($icon->features)) {
                            $features = $icon->features;
                        }

                        if (!empty($icon->price_heighlight)) {
                            $price_heighlight = $icon->price_heighlight;
                        }

                        if ($this->customizer_repeater_subtitle_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Subtitle', 'busicare-plus'), $this->id, 'customizer_repeater_subtitle_control'),
                                'class' => 'customizer-repeater-subtitle-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_subtitle_control'),
                                    ), $subtitle);
                        }

                        if ($this->customizer_repeater_title_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Title', 'busicare-plus'), $this->id, 'customizer_repeater_title_control'),
                                'class' => 'customizer-repeater-title-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_title_control'),
                                    ), $title);
                        }



                        if ($this->customizer_repeater_text_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Description', 'busicare-plus'), $this->id, 'customizer_repeater_text_control'),
                                'class' => 'customizer-repeater-text-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_text_control'),
                                    ), $text);
                        }

                        if ($this->customizer_repeater_features_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Features', 'busicare-plus'), $this->id, 'customizer_repeater_features_control'),
                                'class' => 'customizer-repeater-features-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_features_control'),
                                    ), $features);
                        }




                        if ($this->customizer_repeater_button_text_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Button Text',
                                                'busicare-plus'), $this->id, 'customizer_repeater_button_text_control'),
                                'class' => 'customizer-repeater-button-text-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id,
                                        'customizer_repeater_button_text_control'),
                                    ), $button);
                        }




                        if ($this->customizer_repeater_abtsliderbutton_text_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Button1 Text',
                                                'busicare-plus'), $this->id, 'customizer_repeater_abtsliderbutton_text_control'),
                                'class' => 'customizer-repeater-abtsliderbutton-text-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id,
                                        'customizer_repeater_abtsliderbutton_text_control'),
                                    ), $abtsliderbutton_text);
                        }
                        
                        if ($this->customizer_repeater_abt_slider_link_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Link1', 'busicare-plus'), $this->id, 'customizer_repeater_abt_slider_link_control'),
                                'class' => 'customizer-repeater-abtsliderlink-control',
                                'sanitize_callback' => 'esc_url_raw',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_abt_slider_link_control'),
                                    ), $abtsliderlink);
                        }
                        
                        if ($this->customizer_repeater_abt_slider_checkbox_control == true) {
                            $this->abt_slider_check($abtslider_open_new_tab);
                        }

                        if ($this->customizer_repeater_abtbutton_text_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Button2 Text',
                                                'busicare-plus'), $this->id, 'customizer_repeater_abtbutton_text_control'),
                                'class' => 'customizer-repeater-abtbutton-text-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id,
                                        'customizer_repeater_abtbutton_text_control'),
                                    ), $abtbutton_text);
                        }
                        
                        if ($this->customizer_repeater_abtlink_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Link2', 'busicare-plus'), $this->id, 'customizer_repeater_abtlink_control'),
                                'class' => 'customizer-repeater-abtlink-control',
                                'sanitize_callback' => 'esc_url_raw',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_abtlink_control'),
                                    ), $abtlink);
                        }
                        
//                        if ($this->customizer_repeater_abt_slider_checkbox_control == true) { 
//                            $this->abt_slider_check($abtslider_open_new_tab);
//                        }
                        
                        if ($this->customizer_repeater_abtcheckbox_control == true) {
                            $this->abt_check($abtopen_new_tab);
                        }

                        if($this->customizer_repeater_sidebarcheckbox_control == true){
                            $this->sidebar_check($sidebar_check);
                            
                        }

                        if ($this->customizer_repeater_price_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Price', 'busicare-plus'), $this->id, 'customizer_repeater_price_control'),
                                'class' => 'customizer-repeater-price-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_price_control'),
                                    ), $price);
                        }



                        if ($this->customizer_repeater_slide_format == true) {
                            $this->slide_format($slide_format);
                        }
                        if ($this->customizer_repeater_slider_caption_aligment_control == true) {
                            $this->slide_caption_alignment($home_slider_caption);
                        }

                        if ($this->customizer_repeater_price_heighlight == true) {
                            $this->price_heighlight($price_heighlight);
                        }



                        if ($this->customizer_repeater_video_url_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Video Url',
                                                'busicare-plus'), $this->id, 'customizer_repeater_video_url_control'),
                                'class' => 'customizer-repeater-video-url-control',
                                'type' => apply_filters('customizer_repeater_video_url_control', 'textarea', $this->id, 'customizer_repeater_video_url_control'),
                                    ), $video_url);
                        }

                        if ($this->customizer_repeater_user_name_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Client Name', 'busicare-plus'), $this->id, 'customizer_repeater_user_name_control'),
                                'class' => 'customizer-repeater-user-name-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_user_name_control'),
                                    ), $clientname);
                        }

                        if ($this->customizer_repeater_member_name_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Name', 'busicare-plus'), $this->id, 'customizer_repeater_member_name_control'),
                                'class' => 'customizer-repeater-member-name-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_member_name_control'),
                                    ), $membername);
                        }

                        if ($this->customizer_repeater_designation_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Designation', 'busicare-plus'), $this->id, 'customizer_repeater_designation_control'),
                                'class' => 'customizer-repeater-designation-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_designation_control'),
                                    ), $designation);
                        }

                        if ($this->customizer_repeater_link_control) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Link', 'busicare-plus'), $this->id, 'customizer_repeater_link_control'),
                                'class' => 'customizer-repeater-link-control',
                                'sanitize_callback' => 'esc_url_raw',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_link_control'),
                                    ), $link);
                        }

                        if ($this->customizer_repeater_checkbox_control == true) {
                            $this->testimonila_check($open_new_tab);
                        }

                        if ($this->customizer_repeater_image_control == true && $this->customizer_repeater_icon_control == true) {
                            $this->icon_type_choice($choice);
                        }
                        if ($this->customizer_repeater_image_control == true) {
                            $this->image_control($image_url, $choice);
                        }
                        if ($this->customizer_repeater_icon_control == true) {
                            $this->icon_picker_control($icon_value, $choice);
                        }




                        if ($this->customizer_repeater_color_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Color', 'busicare-plus'), $this->id, 'customizer_repeater_color_control'),
                                'class' => 'customizer-repeater-color-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'color', $this->id, 'customizer_repeater_color_control'),
                                'sanitize_callback' => 'sanitize_hex_color'
                                    ), $color);
                        }


                        if ($this->customizer_repeater_shortcode_control == true) {
                            $this->input_control(array(
                                'label' => apply_filters('repeater_input_labels_filter', esc_html__('Shortcode', 'busicare-plus'), $this->id, 'customizer_repeater_shortcode_control'),
                                'class' => 'customizer-repeater-shortcode-control',
                                'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_shortcode_control'),
                                    ), $shortcode);
                        }



                        if ($this->customizer_repeater_repeater_control == true) {
                            $this->repeater_control($repeater);
                        }
                        ?>

                        <input type="hidden" class="social-repeater-box-id" value="<?php
                        if (!empty($id)) {
                            echo esc_attr($id);
                        }
                        ?>">
                        <button type="button" class="social-repeater-general-control-remove-field" <?php
                if ($it == 0) {
                    echo 'style="display:none;"';
                }
                ?>>
                <?php esc_html_e('Delete field', 'busicare-plus'); ?>
                        </button>

                    </div>
                </div>

                        <?php
                        $it++;
                    }
                } else {
                    ?>
            <div class="customizer-repeater-general-control-repeater-container">
                <div class="customizer-repeater-customize-control-title">
                    <?php echo esc_html($this->boxtitle) ?>
                </div>
                <div class="customizer-repeater-box-content-hidden">
                    <?php
                    if ($this->customizer_repeater_image_control == true && $this->customizer_repeater_icon_control == true) {
                        $this->icon_type_choice();
                    }
                    if ($this->customizer_repeater_image_control == true) {
                        $this->image_control();
                    }
                    if ($this->customizer_repeater_icon_control == true) {
                        $this->icon_picker_control();
                    }



                    if ($this->customizer_repeater_color_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Color', 'busicare-plus'), $this->id, 'customizer_repeater_color_control'),
                            'class' => 'customizer-repeater-color-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'color', $this->id, 'customizer_repeater_color_control'),
                            'sanitize_callback' => 'sanitize_hex_color'
                        ));
                    }
                    if ($this->customizer_repeater_title_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Title', 'busicare-plus'), $this->id, 'customizer_repeater_title_control'),
                            'class' => 'customizer-repeater-title-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_title_control'),
                        ));
                    }

                    if ($this->customizer_repeater_subtitle_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Subtitle', 'busicare-plus'), $this->id, 'customizer_repeater_subtitle_control'),
                            'class' => 'customizer-repeater-subtitle-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_subtitle_control'),
                        ));
                    }
                    if ($this->customizer_repeater_text_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Description', 'busicare-plus'), $this->id, 'customizer_repeater_text_control'),
                            'class' => 'customizer-repeater-text-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_text_control'),
                        ));
                    }

                    if ($this->customizer_repeater_features_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Features', 'busicare-plus'), $this->id, 'customizer_repeater_features_control'),
                            'class' => 'customizer-repeater-features-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_features_control'),
                        ));
                    }



                    if ($this->customizer_repeater_button_text_control) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Button Text',
                                            'busicare-plus'), $this->id, 'customizer_repeater_button_text_control'),
                            'class' => 'customizer-repeater-button-text-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id,
                                    'customizer_repeater_button_text_control'),
                        ));
                    }

                    if ($this->customizer_repeater_link_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Link', 'busicare-plus'), $this->id, 'customizer_repeater_link_control'),
                            'class' => 'customizer-repeater-link-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_link_control'),
                        ));
                    }

                    if ($this->customizer_repeater_checkbox_control == true) {
                        $this->testimonila_check();
                    }

                    if ($this->customizer_repeater_abtsliderbutton_text_control) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Button1 Text',
                                            'busicare-plus'), $this->id, 'customizer_repeater_abtsliderbutton_text_control'),
                            'class' => 'customizer-repeater-abtsliderbutton-text-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id,
                                    'customizer_repeater_abtsliderbutton_text_control'),
                        ));
                    }

                    if ($this->customizer_repeater_abtbutton_text_control) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Button2 Text',
                                            'busicare-plus'), $this->id, 'customizer_repeater_abtbutton_text_control'),
                            'class' => 'customizer-repeater-abtbutton-text-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id,
                                    'customizer_repeater_abtbutton_text_control'),
                        ));
                    }

                    if ($this->customizer_repeater_abt_slider_link_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Link2', 'busicare-plus'), $this->id, 'customizer_repeater_abt_slider_link_control'),
                            'class' => 'customizer-repeater-abtsliderlink-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_abt_slider_link_control'),
                        ));
                    }

                    if ($this->customizer_repeater_abtlink_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Link2', 'busicare-plus'), $this->id, 'customizer_repeater_abtlink_control'),
                            'class' => 'customizer-repeater-abtlink-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_abtlink_control'),
                        ));
                    }

                    if ($this->customizer_repeater_abt_slider_checkbox_control == true) {
                        $this->abt_slider_check();
                    }

                    if ($this->customizer_repeater_abtcheckbox_control == true) {
                        $this->abt_check();
                    }

                    if($this->customizer_repeater_sidebarcheckbox_control == true)
                    {
                            $this->sidebar_check();
                    }

                    if ($this->customizer_repeater_price_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Price', 'busicare-plus'), $this->id, 'customizer_repeater_price_control'),
                            'class' => 'customizer-repeater-price-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_price_control'),
                        ));
                    }




                    if ($this->customizer_repeater_slider_caption_aligment_control == true) {
                        $this->slide_caption_alignment($home_slider_caption);
                    }

                    if ($this->customizer_repeater_price_heighlight == true) {
                        $this->price_heighlight($price_heighlight);
                    }

                    if ($this->customizer_repeater_video_url_control) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Video Url',
                                            'busicare-plus'), $this->id, 'customizer_repeater_video_url_control'),
                            'class' => 'customizer-repeater-video-url-control',
                            'type' => apply_filters('customizer_repeater_video_url_control', 'textarea', $this->id, 'customizer_repeater_video_url_control'),
                        ));
                    }



                    if ($this->customizer_repeater_shortcode_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Shortcode', 'busicare-plus'), $this->id, 'customizer_repeater_shortcode_control'),
                            'class' => 'customizer-repeater-shortcode-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', '', $this->id, 'customizer_repeater_shortcode_control'),
                        ));
                    }


                    if ($this->customizer_repeater_user_name_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Client Name', 'busicare-plus'), $this->id, 'customizer_repeater_user_name_control'),
                            'class' => 'customizer-repeater-user-name-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_user_name_control'),
                        ));
                    }

                    if ($this->customizer_repeater_member_name_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Name', 'busicare-plus'), $this->id, 'customizer_repeater_member_name_control'),
                            'class' => 'customizer-repeater-member-name-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_member_name_control'),
                        ));
                    }

                    if ($this->customizer_repeater_designation_control == true) {
                        $this->input_control(array(
                            'label' => apply_filters('repeater_input_labels_filter', esc_html__('Designation', 'busicare-plus'), $this->id, 'customizer_repeater_designation_control'),
                            'class' => 'customizer-repeater-designation-control',
                            'type' => apply_filters('busicare_plus_Repeater_input_types_filter', 'textarea', $this->id, 'customizer_repeater_designation_control'),
                        ));
                    }

                    if ($this->customizer_repeater_repeater_control == true) {
                        $this->repeater_control();
                    }
                    ?>
                    <input type="hidden" class="social-repeater-box-id">
                    <button type="button" class="social-repeater-general-control-remove-field button" style="display:none;">
            <?php esc_html_e('Delete field', 'busicare-plus'); ?>
                    </button>
                </div>
            </div>
            <?php
        }
    }

    private function input_control($options, $value = '') {
//print_r($options);
        ?>
        <span class="customize-control-title <?php echo esc_html($options['label']); ?>" 
              <?php if ($options['class'] == 'customizer-repeater-video-url-control') {
                  echo 'style="display:none;"';
              } ?>

              ><?php echo esc_html($options['label']); ?></span>
        <?php
        if (!empty($options['type'])) {
            switch ($options['type']) {
                case 'textarea':
                    ?>
                    <textarea class="<?php echo esc_attr($options['class']); ?>" placeholder="<?php echo esc_attr($options['label']); ?>"><?php echo (!empty($options['sanitize_callback']) ? call_user_func_array($options['sanitize_callback'], array($value)) : esc_attr($value) ); ?></textarea>
                    <?php
                    break;
                case 'color':
                    ?>
                    <input type="text" value="<?php echo (!empty($options['sanitize_callback']) ? call_user_func_array($options['sanitize_callback'], array($value)) : esc_attr($value) ); ?>" class="<?php echo esc_attr($options['class']); ?>" />
                    <?php
                    break;
            }
        } else {
            ?>
            <input type="text" value="<?php echo (!empty($options['sanitize_callback']) ? call_user_func_array($options['sanitize_callback'], array($value)) : esc_attr($value) ); ?>" class="<?php echo esc_attr($options['class']); ?>" placeholder="<?php echo esc_attr($options['label']); ?>"/>
            <?php
        }
    }

    private function testimonila_check($value = 'no') {
        ?>
        <div class="customize-control-title">
        <?php esc_html_e('Open link in new tab:', 'busicare-plus'); ?>
            <span class="switch">
                <input type="checkbox" name="custom_checkbox" value="yes" <?php if ($value == 'yes') {
            echo 'checked';
        } ?> class="customizer-repeater-checkbox">
            </span>
        </div>
        <?php
    }

    private function abt_slider_check($value = 'no') {
        ?>
        <div class="customize-control-title">
                <?php esc_html_e('Open link in new tab:', 'busicare-plus'); ?>
            <span class="switch">
                <input type="checkbox" name="abt_slider_custom_checkbox" value="yes" <?php if ($value == 'yes') {
                    echo 'checked';
                } ?> class="customizer-repeater-abtslider_checkbox">
            </span>
        </div>
                <?php
            }

    private function abt_check($value = 'no') {
        ?>
        <div class="customize-control-title">
                <?php esc_html_e('Open link in new tab:', 'busicare-plus'); ?>
            <span class="switch">
                <input type="checkbox" name="abtcustom_checkbox" value="yes" <?php if ($value == 'yes') {
                    echo 'checked';
                } ?> class="customizer-repeater-abtcheckbox">
            </span>
        </div>
                <?php
            }
    private function sidebar_check($value='no'){
        ?>
        
    <div class="customize-control-title">
    <?php esc_html_e('Enable Slider Widgets:','busicare-plus'); ?>
    <span class="switch">
      <input type="checkbox" name="abtcustom_checkbox" value="yes" <?php if($value=='yes'){echo 'checked';}?> class="customizer-repeater-sidebarcheckbox" style="margin-bottom: 0 !important;"><br>
      <label><?php esc_html_e('Note: Widgets will not apply with center content alignment','busicare-plus'); ?></label>
    </span>
    </div>
    <?php
    }

            private function icon_picker_control($value = '', $show = '') {
                ?>
        <div class="social-repeater-general-control-icon" <?php if ($show === 'customizer_repeater_image' || $show === 'customizer_repeater_none') {
                    echo 'style="display:none;"';
                } ?>>
            <span class="customize-control-title">
            <?php esc_html_e('Icon', 'busicare-plus'); ?>
            </span>
            <span class="description customize-control-description">
        <?php
        echo sprintf(
                esc_html__('Note: Some icons may not be displayed here. You can see the full list of icons at %1$s.', 'busicare-plus'),
                sprintf('<a href="http://fontawesome.io/icons/" rel="nofollow">%s</a>', esc_html__('http://fontawesome.io/icons/', 'busicare-plus'))
        );
        ?>
            </span>
            <div class="input-group icp-container">
                <input data-placement="bottomRight" class="icp icp-auto" value="<?php if (!empty($value)) {
            echo esc_attr($value);
        } ?>" type="text">
                <span class="input-group-addon">
                    <i class="fa <?php echo esc_attr($value); ?>"></i>
                </span>
            </div>
            <?php include($this->customizer_icon_container); ?>
        </div>
        <?php
    }

    private function image_control($value = '', $show = '') {
        ?>
        <div class="customizer-repeater-image-control" <?php if ($show === 'customizer_repeater_icon' || $show === 'customizer_repeater_none') {
            echo 'style="display:none;"';
        } ?>>
            <span class="customize-control-title">
        <?php esc_html_e('Image', 'busicare-plus') ?>
            </span>
            <input type="text" class="widefat custom-media-url" value="<?php echo esc_attr($value); ?>">
            <input type="button" class="button button-secondary customizer-repeater-custom-media-button" value="<?php esc_html_e('Upload Image', 'busicare-plus'); ?>" />
        </div>
        <?php
    }

    private function slide_caption_alignment($value = 'customizer_repeater_slide_caption_left') {
        ?>

        <span class="customize-control-title">
        <?php esc_html_e('Content Alignment', 'busicare-plus'); ?>
        </span>
        <select class="customizer-repeater-slide-caption-align">
            <option value="customizer_repeater_slide_caption_left" <?php if ($value == 'customizer_repeater_slide_caption_left') {
            echo 'selected';
        } ?>>
                <?php esc_html_e('Left', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_slide_caption_center" <?php if ($value == 'customizer_repeater_slide_caption_center') {
            echo 'selected';
        } ?>>
        <?php esc_html_e('Center', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_slide_caption_right" <?php if ($value == 'customizer_repeater_slide_caption_right') {
            echo 'selected';
        } ?>>
                <?php esc_html_e('Right', 'busicare-plus') ?>
            </option>

        </select>

        <?php
    }

    private function slide_format($value = 'customizer_repeater_slide_format_standard') {
        ?>

        <span class="customize-control-title">
        <?php esc_html_e('Slide Format', 'busicare-plus'); ?>
        </span>
        <select class="customizer-repeater-slide-format">
            <option value="customizer_repeater_slide_format_standard" <?php selected($value, 'customizer_repeater_slide_format_standard'); ?>>
        <?php esc_html_e('Standard', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_slide_format_aside" <?php selected($value, 'customizer_repeater_slide_format_aside'); ?>>
        <?php esc_html_e('Aside', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_slide_format_quote" <?php selected($value, 'customizer_repeater_slide_format_quote'); ?>>
        <?php esc_html_e('Quote', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_slide_format_status" <?php selected($value, 'customizer_repeater_slide_format_status'); ?>>
        <?php esc_html_e('Status', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_slide_forma_video" <?php selected($value, 'customizer_repeater_slide_forma_video'); ?>>
            <?php esc_html_e('Video', 'busicare-plus') ?>
            </option>

        </select>

        <?php
    }

    private function price_heighlight($value = 'customizer_repeater_price_heighlight') {
        ?>

        <span class="customize-control-title">
        <?php esc_html_e('Price highlight', 'busicare-plus'); ?>
        </span>
        <select class="customizer-repeater-price-heighlight">
            <option value="customizer_repeater_price_heighlight" <?php selected($value, 'customizer_repeater_price_heighlight'); ?>>
        <?php esc_html_e('Highlight', 'busicare-plus') ?>
            </option>

            <option value="customizer_repeater_price_heighlight_nonlight" <?php selected($value, 'customizer_repeater_price_heighlight_nonlight'); ?>>
        <?php esc_html_e('Non highlight', 'busicare-plus') ?>
            </option>
        </select>

        <?php
    }

    private function icon_type_choice($value = 'customizer_repeater_icon') {
        ?>
        <span class="customize-control-title">
                    <?php esc_html_e('Image type', 'busicare-plus'); ?>
        </span>
        <select class="customizer-repeater-image-choice">
            <option value="customizer_repeater_icon" <?php selected($value, 'customizer_repeater_icon'); ?>><?php esc_html_e('Icon', 'busicare-plus'); ?></option>
            <option value="customizer_repeater_image" <?php selected($value, 'customizer_repeater_image'); ?>><?php esc_html_e('Image', 'busicare-plus'); ?></option>
            <option value="customizer_repeater_none" <?php selected($value, 'customizer_repeater_none'); ?>><?php esc_html_e('None', 'busicare-plus'); ?></option>
        </select>
        <?php
    }

    private function repeater_control($value = '') {
        $social_repeater = array();
        $show_del = 0;
        ?>
        <span class="customize-control-title"><?php esc_html_e('Social icons', 'busicare-plus'); ?></span>
        <?php
        if (!empty($value)) {
            $social_repeater = json_decode(html_entity_decode($value), true);
        }
        if (( count($social_repeater) == 1 && '' === $social_repeater[0] ) || empty($social_repeater)) {
            ?>
            <div class="customizer-repeater-social-repeater">
                <div class="customizer-repeater-social-repeater-container">
                    <div class="customizer-repeater-rc input-group icp-container">
                        <input data-placement="bottomRight" class="icp icp-auto" value="<?php if (!empty($value)) {
                echo esc_attr($value);
            } ?>" type="text">
                        <span class="input-group-addon"></span>
                    </div>
                            <?php include_once($this->customizer_icon_container); ?>
                    <input type="text" class="customizer-repeater-social-repeater-link"
                           placeholder="<?php esc_html_e('Link', 'busicare-plus'); ?>">
                    <input type="hidden" class="customizer-repeater-social-repeater-id" value="">
                    <button class="social-repeater-remove-social-item" style="display:none">
                <?php esc_html_e('Remove Icon', 'busicare-plus'); ?>
                    </button>
                </div>
                <input type="hidden" id="social-repeater-socials-repeater-colector" class="social-repeater-socials-repeater-colector" value=""/>
            </div>
            <button class="social-repeater-add-social-item button-secondary"><?php esc_html_e('Add Icon', 'busicare-plus'); ?></button>
            <?php } else {
            ?>
            <div class="customizer-repeater-social-repeater">
            <?php
            foreach ($social_repeater as $social_icon) {
                $show_del++;
                ?>
                    <div class="customizer-repeater-social-repeater-container">
                        <div class="customizer-repeater-rc input-group icp-container">
                            <input data-placement="bottomRight" class="icp icp-auto" value="<?php if (!empty($social_icon['icon'])) {
                    echo esc_attr($social_icon['icon']);
                } ?>" type="text">
                            <span class="input-group-addon"><i class="fa <?php echo esc_attr($social_icon['icon']); ?>"></i></span>
                        </div>
                <?php include($this->customizer_icon_container); ?>
                        <input type="text" class="customizer-repeater-social-repeater-link"
                               placeholder="<?php esc_html_e('Link', 'busicare-plus'); ?>"
                               value="<?php
                if (!empty($social_icon['link'])) {
                    echo esc_url($social_icon['link']);
                }
                ?>">
                        <input type="hidden" class="customizer-repeater-social-repeater-id"
                               value="<?php
                if (!empty($social_icon['id'])) {
                    echo esc_attr($social_icon['id']);
                }
                ?>">
                        <button class="social-repeater-remove-social-item"
                                style="<?php
                if ($show_del == 1) {
                    echo "display:none";
                }
                ?>"><?php esc_html_e('Remove Icon', 'busicare-plus'); ?></button>
                    </div>
                <?php }
            ?>
                <input type="hidden" id="social-repeater-socials-repeater-colector"
                       class="social-repeater-socials-repeater-colector"
                       value="<?php echo esc_textarea(html_entity_decode($value)); ?>" />
            </div>
            <button class="social-repeater-add-social-item button-secondary"><?php esc_html_e('Add Icon', 'busicare-plus'); ?></button>
            <?php
        }
    }

}
