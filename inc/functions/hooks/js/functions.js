/**
 *  Exported busicare_filter_hooks
 *
 */

/**
 * Filter hooks
 */
function busicare_plus_filter_hooks() {

	var search_val = '';

	if ( typeof jQuery( '#busicare_search_hooks' ) !== 'undefined' ) {

		if ( typeof jQuery( '#busicare_search_hooks' ).val() !== 'undefined' ) {

			search_val = jQuery( '#busicare_search_hooks' ).val().toUpperCase();

			if ( typeof search_val !== 'undefined' ) {

				jQuery( '#BusiCare_plus_Hooks_Settings th' ).each(
					function () {

						if ( jQuery( this ).text().toUpperCase().indexOf( search_val ) > -1 ) {
							jQuery( this ).parent().removeClass( 'hooks-none' );
						} else {
							jQuery( this ).parent().addClass( 'hooks-none' );
						}
					}
				);

			}

		}

	}
}