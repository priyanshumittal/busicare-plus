<?php
/**
 * List of available hooks
 *
 */
 
/* Top Header section */
/**
 * Hook just before the Top Header
 *
 */ 
function busicare_plus_before_header_section_trigger() {
	do_action( 'busicare_plus_before_header_section_hook' );
}
/**
 * Hook just after the Top Header
 *
 */
function busicare_plus_after_header_section_trigger() {
	do_action( 'busicare_plus_after_header_section_hook' );
}


/* Slider section */
/**
 * Hook just before the Slider
 *
 */ 
function busicare_plus_before_slider_section_trigger() {
	do_action( 'busicare_plus_before_slider_section_hook' );
}
/**
 * Hook just after the Slider
 *
 */
function busicare_plus_after_slider_section_trigger() {
	do_action( 'busicare_plus_after_slider_section_hook' );
}

/* CTA1 section 1 */
/**
 * Hook just before the Callout
 *
 */ 
function busicare_plus_before_cta1_section_trigger() {
	do_action( 'busicare_plus_before_cta1_section_hook' );
}
/**
 * Hook just after the Callout
 *
 */
function busicare_plus_after_cta1_section_trigger() {
	do_action( 'busicare_plus_after_cta1_section_hook' );
}

/* Service section */
/**
 * Hook just before the Service
 *
 */ 
function busicare_plus_before_services_section_trigger() {
	do_action( 'busicare_plus_before_service_section_hook' );
}
/**
 * Hook just after the Service
 *
 */
function busicare_plus_after_services_section_trigger() {
	do_action( 'busicare_plus_after_service_section_hook' );
}

 
/* Portfolio section */
/**
 * Hook just before the Portfolio section
 *
 */ 
function busicare_plus_before_portfolio_section_trigger() {
	do_action( 'busicare_plus_before_portfolio_section_hook' );
}
/**
 * Hook just after the Portfolio section
 *
 */
function busicare_plus_after_portfolio_section_trigger() {
	do_action( 'busicare_plus_after_portfolio_section_hook' );
}

 
/* Fun section */
/**
 * Hook just before the Fun section
 *
 */ 
function busicare_plus_before_fun_section_trigger() {
	do_action( 'busicare_plus_before_fun_section_hook' );
}
/**
 * Hook just after the Fun section
 *
 */
function busicare_plus_after_fun_section_trigger() {
	do_action( 'busicare_plus_after_fun_section_hook' );
}

/* Testimonial section */
/**
 * Hook just before the Testimonial
 *
 */ 
function busicare_plus_before_testimonial_section_trigger() {
	do_action( 'busicare_plus_before_testimonial_section_hook' );
}
/**
 * Hook just after the Testimonial
 *
 */
function busicare_plus_after_testimonial_section_trigger() {
	do_action( 'busicare_plus_after_testimonial_section_hook' );
}



/* Blog section */
/**
 * Hook just before the Blog
 *
 */ 
function busicare_plus_before_news_section_trigger() {
	do_action( 'busicare_plus_before_news_section_hook' );
}
/**
 * Hook just after the Blog
 *
 */
function busicare_plus_after_news_section_trigger() {
	do_action( 'busicare_plus_after_news_section_hook' );
}


/* Gallery section */
/**
 * Hook just before the Gallery
 *
 */ 
function busicare_plus_before_gallery_section_trigger() {
	do_action( 'busicare_plus_before_gallery_section_hook' );
}
/**
 * Hook just after the Gallery
 *
 */
function busicare_plus_after_gallery_section_trigger() {
	do_action( 'busicare_plus_after_gallery_section_hook' );
}


/* Team section */
/**
 * Hook just before the Team
 *
 */ 
function busicare_plus_before_team_section_trigger() {
	do_action( 'busicare_plus_before_team_section_hook' );
}
/**
 * Hook just after the Team
 *
 */
function busicare_plus_after_team_section_trigger() {
	do_action( 'busicare_plus_after_team_section_hook' );
}


/* Shop section */
/**
 * Hook just before the Shop
 *
 */ 
function busicare_plus_before_shop_section_trigger() {
	do_action( 'busicare_plus_before_shop_section_hook' );
}
/**
 * Hook just after the Shop
 *
 */
function busicare_plus_after_wooproduct_section_trigger() {
	do_action( 'busicare_plus_after_wooproduct_section_hook' );
}


/* Client section */
/**
 * Hook just before the Client
 *
 */ 
function busicare_plus_before_client_section_trigger() {
	do_action( 'busicare_plus_before_client_section_hook' );
}
/**
 * Hook just after the Client
 *
 */
function busicare_plus_after_client_section_trigger() {
	do_action( 'busicare_plus_after_client_section_hook' );
}

/* CTA1 section 2 */
/**
 * Hook just before the Callout
 *
 */ 
function busicare_plus_before_cta2_section_trigger() {
	do_action( 'busicare_plus_before_cta2_section_hook' );
}
/**
 * Hook just after the Callout
 *
 */
function busicare_plus_after_cta2_section_trigger() {
	do_action( 'busicare_plus_after_cta2_section_hook' );
}


/* Footer section */
/**
 * Hook just before the Footer
 *
 */ 
function busicare_plus_before_footer_section_trigger() {
	do_action( 'busicare_plus_before_footer_section_hook' );
}
/**
 * Hook just after the Footer
 *
 */
function busicare_plus_after_footer_section_trigger() {
	do_action( 'busicare_plus_after_footer_section_hook' );
}