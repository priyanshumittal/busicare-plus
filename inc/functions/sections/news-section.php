<?php
//call the action for the news section
add_action('busicare_plus_news_action','busicare_plus_news_section');
//function for the news section
function busicare_plus_news_section()
{
$latest_news_section_enable = get_theme_mod('latest_news_section_enable', true);
if ($latest_news_section_enable != false) {
    $news_layout=get_theme_mod('home_news_design_layout', 1);
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/news-content'.$news_layout.'.php');
}
}