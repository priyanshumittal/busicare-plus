<?php
//call the action for the portfolio section
add_action('busicare_plus_portfolio_action','busicare_plus_portfolio_section');
//function for the portfolio section
function busicare_plus_portfolio_section()
{
$busicare_home_project_enabled = get_theme_mod('portfolio_section_enable', true);
if ($busicare_home_project_enabled != false) {
    $busicare_project_section_title = get_theme_mod('home_portfolio_section_title', __('Look at our projects', 'busicare-plus'));
    $home_portfolio_section_subtitle = get_theme_mod('home_portfolio_section_subtitle', __('View Our Recent Works', 'busicare-plus'));
    $portfolio_col=get_theme_mod('home_portfolio_column_laouts',4);
    $no_portfolio=get_theme_mod('home_portfolio_numbers_options',3);
    $team_animation_speed = get_theme_mod('team_animation_speed', 3000);
    $team_smooth_speed = get_theme_mod('team_smooth_speed', 1000);
    $portfolio_nav_style = get_theme_mod('portfolio_nav_style', 'navigation');
    $isRTL = (is_rtl()) ? (bool) true : (bool) false;
    $portfoliosettings = array('portfolio_nav_style' => $portfolio_nav_style, 'rtl' => $isRTL);
    wp_register_script('busicare-portfolio', BUSICAREP_PLUGIN_URL . '/inc/js/front-page/portfolio.js', array('jquery'));
    wp_localize_script('busicare-portfolio', 'portfolio_settings', $portfoliosettings);
    wp_enqueue_script('busicare-portfolio');

    $post_type = 'busicare_portfolio';
    $tax = 'portfolio_categories';
    $term_args = array('hide_empty' => true, 'orderby' => 'id');
    $posts_per_page = get_theme_mod('home_portfolio_numbers', 3);
    $tax_terms = get_terms($tax, $term_args);
    $defualt_tex_id = get_option('busicare_default_term_id');
    $j = 1;
    $k = 1;
    ?>
    <section class="section-space portfolio home-portfolio" id="portfolio">
        <div class="busicare-portfolio-container container">
        <?php if (!empty($busicare_project_section_title) || !empty($home_portfolio_section_subtitle)) {
            ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="section-header">
                            <?php if (!empty($busicare_project_section_title)): ?><h2 class="section-title home_project_title "><?php echo esc_html($busicare_project_section_title); ?></h2><div class="title_seprater"></div><?php endif; ?>
                            <?php if (!empty($home_portfolio_section_subtitle)): ?><h5 class="section-subtitle home_project_subtitle"><?php echo esc_html($home_portfolio_section_subtitle); ?></h5><?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            if (!empty($tax_terms)) {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <ul id="tabs" class="nav filter-tabs justify-content-center" role="tablist">
                            <?php foreach ($tax_terms as $tax_term) { ?>
                                <li rel="tab" class="nav-item" >
                                    <a id="tab-<?php echo esc_attr(rawurldecode($tax_term->slug)); ?>" href="#<?php echo $tax_term->slug;  ?>" data-toggle="tab" role="tab"  class="nav-link <?php
                                    if ($j == 1) {
                                        echo 'active';
                                        $j = 2;
                                    }
                                    ?>"><?php echo $tax_term->name; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } else { ?>

                <div class="row">
                    <div class="col-lg-12">
                        <ul id="tabs" class="nav filter-tabs justify-content-center" role="tablist">
                            <li class="nav-item"><a id="tab-A" href="#all" class="nav-link active" data-toggle="tab" role="tab"><?php _e('Show All', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-B" href="#bussiness" class="nav-link" data-toggle="tab" role="tab"><?php _e('Bussiness', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-C" href="#branding" class="nav-link" data-toggle="tab" role="tab"><?php _e('Branding', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-C" href="#wordpress" class="nav-link" data-toggle="tab" role="tab"><?php _e('Wordpress', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-C" href="#graphics" class="nav-link" data-toggle="tab" role="tab"><?php _e('Graphics', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-C" href="#photography" class="nav-link" data-toggle="tab" role="tab"><?php _e('Photography', 'busiprof'); ?></a></li>
                        </ul>
                    </div>
                </div>
            <?php } if (!empty($tax_terms)) { ?>

                <div id="content" class="tab-content" role="tablist">
                    <?php
                    $is_active = true;
                    if ($tax_terms) {
                        foreach ($tax_terms as $tax_term) {
                            $args = array(
                                'post_type' => $post_type,
                                'post_status' => 'publish',
                                'portfolio_categories' => $tax_term->slug,
                                'posts_per_page' => $no_portfolio,
                                'orderby' => 'menu_order',
                            );
                            $portfolio_query = null;
                            $portfolio_query = new WP_Query($args);
                            if ($portfolio_query->have_posts()):
                                ?>
                                <div id="<?php echo esc_attr(rawurldecode($tax_term->slug)); ?>" class="tab-pane fade show <?php
                                if ($k == 1) {
                                    echo 'active';
                                    $k = 2;
                                }
                                ?>" role="tabpanel" aria-labelledby="tab-<?php echo esc_attr(rawurldecode($tax_term->slug)); ?>">
                                    <div class="row">
                                        <?php
                                        while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
                                            $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                                            $portfolio_sub_title = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_sub_title', true));
                                            $portfolio_title_description = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_title_description', true));
                                            if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                                $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                                            } else {
                                                $portfolio_link = '';
                                            }
                                            ?>
                                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                                <article class="post">
                                                    <figure class="portfolio-thumbnail">
                                                        <?php
                                                        the_post_thumbnail('full', array('class' => 'img-fluid'));
                                                        if (has_post_thumbnail()) {
                                                            $post_thumbnail_id = get_post_thumbnail_id();
                                                            $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                                        }
                                                        ?>
                                                        <div class="click-view">
                                                            <div class="view-content">
                                                                <a href="<?php echo esc_url($post_thumbnail_url); ?>" data-lightbox="image" title="<?php the_title(); ?>" class="mr-2"><i class="fa-solid fa-image"></i></a>
                                                                <?php if (!empty($portfolio_link)) { ?>
                                                                    <a href="<?php echo esc_url($portfolio_link); ?>" <?php
                                                                    if (!empty($portfolio_target)) {
                                                                        echo 'target="_blank"';
                                                                    }
                                                                    ?> ><i class="fa-solid fa-link"></i></a>
                                                                   <?php } ?>
                                                            </div>
                                                        </div>                             
                                                    </figure>	
                                                    <figcaption>
                                                        <h4 class="portfolio_title"><?php the_title(); ?></h4>
                                                    </figcaption>
                                                </article>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                                <?php
                                wp_reset_query();
                            else:
                                ?>
                                <div id="<?php echo esc_attr(rawurldecode($tax_term->slug)); ?>" class="tab-pane fade in <?php
                                if ($tab == '') {
                                    if ($is_active == true) {
                                        echo 'active';
                                    }$is_active = false;
                                } else if ($tab == rawurldecode($tax_term->slug)) {
                                    echo 'active';
                                }
                                ?>"></div>
                                 <?php
                                 endif;
                             }
                         }
                         ?>	
                </div>

            <?php } else { ?>
                <div id="content" class="tab-content" role="tablist">

                    <div id="all" class="tab-pane fade show active" role="tabpanel" aria-labelledby="tab-A">
                        <div class="row">
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" class="img-fluid" alt="Business Consultant">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" class="img-fluid" alt="Why our busicare">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" class="img-fluid" alt="Shopify App Development">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                        </div>
                    </div>


                    <div id="bussiness" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-B">
                        <div class="row">
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" class="img-fluid" alt="Business Consultant">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" class="img-fluid" alt="Why our busicare">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" class="img-fluid" alt="Shopify App Development">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                        </div>
                    </div>


                    <div id="branding" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                        <div class="row">
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" class="img-fluid" alt="Business Consultant">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" class="img-fluid" alt="Why our busicare">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                        </div>
                    </div>


                    <div id="wordpress" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                        <div class="row">
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" class="img-fluid" alt="Business Consultant">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" class="img-fluid" alt="Why our busicare">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" class="img-fluid" alt="Shopify App Development">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                        </div>
                    </div>


                    <div id="graphics" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                        <div class="row">
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" class="img-fluid" alt="Business Consultant">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" class="img-fluid" alt="Why our busicare">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                        </div>
                    </div>


                    <div id="photography" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                        <div class="row">
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" class="img-fluid" alt="Business Consultant">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item01.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" class="img-fluid" alt="Why our busicare">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item02.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                            <div class="col-lg-<?php echo esc_attr($portfolio_col);?> col-md-6 col-sm-12">	
                                <article class="post">
                                    <figure class="portfolio-thumbnail">
                                        <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" class="img-fluid" alt="Shopify App Development">
                                        <div class="click-view"><div class="view-content"><a href="<?php echo esc_url(BUSICAREP_PLUGIN_URL); ?>/inc/images/portfolio/item03.jpg" data-lightbox="image" title="Why our busicare" class="mr-2"><i class="fa-solid fa-image"></i></a><a href="#" title="More details" alt="more-details" class="ml-2"><i class="fa-solid fa-link"></i></a></div></div>
                                    </figure>	
                                    <figcaption>
                                        <h4 class="portfolio_title">Why our busicare</h4>
                                    </figcaption>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
<?php }
}