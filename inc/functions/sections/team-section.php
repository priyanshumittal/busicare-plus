<?php
//call the action for the team section
add_action('busicare_plus_team_action','busicare_plus_team_section');
//function for the services section
function busicare_plus_team_section()
{
$team_section_enable = get_theme_mod('team_section_enable', true);
if ($team_section_enable != false) {
	$team_layout=get_theme_mod('home_team_design_layout', 1);
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/team-content'.$team_layout.'.php');
}
}