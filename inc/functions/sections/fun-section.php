<?php
//call the action for the fun-acts section
add_action('busicare_plus_fun_action','busicare_plus_fun_section');
//function for the fun-acts section
function busicare_plus_fun_section()
{
$funfact_section_enabled = get_theme_mod('funfact_section_enabled', true);
if($funfact_section_enabled != false)
{ 
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/fun-content.php');
}
}