<?php
//call the action for the cta2 section
add_action('busicare_plus_cta2_action','busicare_plus_cta2_section');
//function for the cta2 section
function busicare_plus_cta2_section()
{
$cta2_section_enable  = get_theme_mod('cta2_section_enable', true);
if($cta2_section_enable != false) 
{ 
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/cta2-content.php');
}
}