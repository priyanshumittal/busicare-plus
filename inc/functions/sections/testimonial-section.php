<?php
//call the action for the testimonial section
add_action('busicare_plus_testimonial_action','busicare_plus_testimonial_section');
//function for the testimonial section
function busicare_plus_testimonial_section()
{
$testimonial_section_enable = get_theme_mod('testimonial_section_enable', true);
if($testimonial_section_enable != false)
{
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/testimonial-content.php');
} 
}