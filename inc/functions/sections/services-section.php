<?php
//call the action for the services section
add_action('busicare_plus_services_action','busicare_plus_services_section');
//function for the services section
function busicare_plus_services_section()
{
$busicare_home_service_enabled = get_theme_mod('home_service_section_enabled', true);
if($busicare_home_service_enabled != false)
{
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/service-content.php');   
}
}