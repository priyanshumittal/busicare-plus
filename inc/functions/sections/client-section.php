<?php
//call the action for the client section
add_action('busicare_plus_client_action','busicare_plus_client_section');
//function for the client section
function busicare_plus_client_section()
{
$client_section_enable = get_theme_mod('client_section_enable', true);
if($client_section_enable != false)
{ 
include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}
}