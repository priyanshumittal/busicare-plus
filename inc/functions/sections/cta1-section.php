<?php
//call the action for the cta1 section
add_action('busicare_plus_cta1_action','busicare_plus_cta1_section');
//function for the cta1 section
function busicare_plus_cta1_section()
{
$cta1_section_enable  = get_theme_mod('cta1_section_enable', true);
if($cta1_section_enable != false){ 
	include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/cta1-content.php');	
} 
}