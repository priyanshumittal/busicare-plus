<?php
if (!function_exists('busicare_plus_starter_service_json')) {

    function busicare_plus_starter_service_json() {
        return json_encode(array(
        array(
            'icon_value' => 'fa-headphones',
            'title' => esc_html__('Unlimited Support', 'busicare-plus'),
            'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'busicare-plus'),
            'choice' => 'customizer_repeater_icon',
            'link' => '#',
            'open_new_tab' => 'yes',
            'id' => 'customizer_repeater_56d7ea7f40b56',
        ),
        array(
            'icon_value' => 'fa-mobile',
            'title' => esc_html__('Pixel Perfect Design', 'busicare-plus'),
            'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'busicare-plus'),
            'choice' => 'customizer_repeater_icon',
            'link' => '#',
            'open_new_tab' => 'yes',
            'id' => 'customizer_repeater_56d7ea7f40b66',
        ),
        array(
            'icon_value' => 'fa fa-cogs',
            'title' => esc_html__('Powerful Options', 'busicare-plus'),
            'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'busicare-plus'),
            'choice' => 'customizer_repeater_icon',
            'link' => '#',
            'open_new_tab' => 'yes',
            'id' => 'customizer_repeater_56d7ea7f40b86',
        ),
            ));
    }

}

if (!function_exists('busicare_plus_starter_contact_content_json')) {

    function busicare_plus_starter_contact_content_json() {
        return json_encode(array(
       array(
            'icon_value' => 'fa-solid fa-envelope',
            'title'=>'Email Your Problem',
            'text' => __('info@busicare.com <br/>support@busicare.com', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b60',
        ),
        array(
            'icon_value' => 'fa fa-phone',
            'title'=>'Call Us Any Time',
            'text' => __('+(15) 718-999-3939, <br/>+(15) 781-254-8437', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b61',
        ),
        array(
            
            'icon_value' => 'fa-solid fa-location-dot',
            'title'=>'Visit Our Office',
            'text' => __('514 S. Magnolia St. Orlando, <br/>FL 32806', 'busicare-plus'),
            'id' => 'customizer_repeater_56d7ea7f40b62',
        ),
     ));
    }
}

if (!function_exists('busicare_plus_starter_contact_social_json')) {

    function busicare_plus_starter_contact_social_json() {
        return json_encode(array(
        array(
                        'icon_value' => 'fa-brands fa-facebook-f',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b76',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-x-twitter',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b77',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-linkedin',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b78',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-instagram',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
    ));
    }
}

if (!function_exists('busicare_plus_starter_team_json')) {

    function busicare_plus_starter_team_json() {
        return json_encode(array(
        array(
            'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team1.jpg',
            'membername' => 'Danial Wilson',
            'designation' => esc_html__('Senior Manager', 'busicare-plus'),
            'open_new_tab' => 'no',
            'id' => 'customizer_repeater_56d7ea7f40c56',
            'social_repeater' => json_encode(
                    array(
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb908674e06',
                            'link' => 'facebook.com',
                            'icon' => 'fa-brands fa-facebook-f',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9148530fc',
                            'link' => 'twitter.com',
                            'icon' => 'fa-brands fa-x-twitter',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9150e1e89',
                            'link' => 'linkedin.com',
                            'icon' => 'fa-brands fa-linkedin',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9150e1e256',
                            'link' => 'behance.com',
                            'icon' => 'fa-brands fa-behance',
                        ),
                    )
            ),
        ),
        array(
            'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team2.jpg',
            'membername' => 'Amanda Smith',
            'designation' => esc_html__('Founder & CEO', 'busicare-plus'),
            'open_new_tab' => 'no',
            'id' => 'customizer_repeater_56d7ea7f40c66',
            'social_repeater' => json_encode(
                    array(
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9155a1072',
                            'link' => 'facebook.com',
                            'icon' => 'fa-brands fa-facebook-f',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9160ab683',
                            'link' => 'twitter.com',
                            'icon' => 'fa-brands fa-x-twitter',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb916ddffc9',
                            'link' => 'linkedin.com',
                            'icon' => 'fa-brands fa-linkedin',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb916ddffc784',
                            'link' => 'behance.com',
                            'icon' => 'fa-brands fa-behance',
                        ),
                    )
            ),
        ),
        array(
            'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team3.jpg',
            'membername' => 'Victoria Wills',
            'designation' => esc_html__('Web Master', 'busicare-plus'),
            'text' => 'Pok pok direct trade godard street art, poutine fam typewriter food truck narwhal kombucha wolf cardigan butcher whatever pickled you.',
            'open_new_tab' => 'no',
            'id' => 'customizer_repeater_56d7ea7f40c76',
            'social_repeater' => json_encode(
                    array(
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                            'link' => 'facebook.com',
                            'icon' => 'fa-brands fa-facebook-f',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                            'link' => 'twitter.com',
                            'icon' => 'fa-brands fa-x-twitter',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                            'link' => 'linkedin.com',
                            'icon' => 'fa-brands fa-linkedin',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                            'link' => 'behance.com',
                            'icon' => 'fa-brands fa-behance',
                        ),
                    )
            ),
        ),
        array(
            'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team4.jpg',
            'membername' => 'Travis Marcus',
            'designation' => esc_html__('UI Developer', 'busicare-plus'),
            'open_new_tab' => 'no',
            'id' => 'customizer_repeater_56d7ea7f40c86',
            'social_repeater' => json_encode(
                    array(
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                            'link' => 'facebook.com',
                            'icon' => 'fa-brands fa-facebook-f',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                            'link' => 'twitter.com',
                            'icon' => 'fa-brands fa-x-twitter',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                            'link' => 'linkedin.com',
                            'icon' => 'fa-brands fa-linkedin',
                        ),
                        array(
                            'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                            'link' => 'behance.com',
                            'icon' => 'fa-brands fa-behance',
                        ),
                    )
            ),
        ),
    ));
    }
}
if (!function_exists('busicare_plus_testimonial_design')) {

    function busicare_plus_testimonial_design($designId,$type) {
        switch ($designId) {
            case 'testimonial-carousel':
                $sectionClass = '';
                $textAlign = 'text-center';
                break;
            case 'testimonial-carousel2':
                $sectionClass = 'testi-2';
                $textAlign = '';
                break;
            case 'testimonial-carousel4':
                $sectionClass = 'testi-4';
                $textAlign = '';
                break;

            default:
                break;
        }

        $testimonial_options = get_theme_mod('busicare_testimonial_content');
        if (empty($testimonial_options)) 
        {
            if (get_theme_mod('home_testimonial_title') != '' || get_theme_mod('home_testimonial_desc') != '' || get_theme_mod('home_testimonial_name') != '' || get_theme_mod('home_testimonial_thumb') != '') {
            $home_testimonial_title = get_theme_mod('home_testimonial_title');
            $home_testimonial_discription = get_theme_mod('home_testimonial_desc');
            $home_testimonial_client_name = get_theme_mod('home_testimonial_name');
            $home_testimonial_designation = get_theme_mod('home_testimonial_designation');
            $home_testimonial_link = get_theme_mod('home_testimonial_link');
            $home_testimonial_image = get_theme_mod('home_testimonial_thumb');
            $testimonial_options = json_encode( array(
                                        array(
                                        'title' => !empty($home_testimonial_title) ? $home_testimonial_title : 'Exellent Theme & Very Fast Support',
                                        'text' => !empty($home_testimonial_discription) ? $home_testimonial_discription :'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                                        'clientname' => !empty($home_testimonial_client_name) ? $home_testimonial_client_name : __('Amanda Smith', 'busicare-plus'),
                                        'designation' => !empty($home_testimonial_designation) ? $home_testimonial_designation : __('Developer', 'busicare-plus'),
                                        'link' => !empty($home_testimonial_link) ? $home_testimonial_link : '#',
                                        'image_url' =>  !empty($home_testimonial_image) ? $home_testimonial_image : BUSICAREP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                                        'open_new_tab' => 'no',
                                        'id' => 'customizer_repeater_56d7ea7f40b96',
                                        ),
                                    ));
            }
            else
            {
                $home_testimonial_section_title = get_theme_mod('home_testimonial_section_title');
                $home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription');
                $designation = get_theme_mod('designation');
                $home_testimonial_thumb = get_theme_mod('home_testimonial_thumb');
                $testimonial_options = json_encode(array(
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Amanda Smith', 'busicare-plus'),
                        'designation' => __('Developer', 'busicare-plus'),
                        'home_testimonial_star' => '4.5',
                        'link' => '#',
                        'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_77d7ea7f40b96',
                        'home_slider_caption' => 'customizer_repeater_star_4.5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Travis Cullan', 'busicare-plus'),
                        'designation' => __('Team Leader', 'busicare-plus'),
                        'home_testimonial_star' => '5',
                        'link' => '#',
                        'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_88d7ea7f40b97',
                        'home_slider_caption' => 'customizer_repeater_star_5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Victoria Wills', 'busicare-plus'),
                        'designation' => __('Volunteer', 'busicare-plus'),
                        'home_testimonial_star' => '3.5',
                        'link' => '#',
                        'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                        'id' => 'customizer_repeater_11d7ea7f40b98',
                        'open_new_tab' => 'no',
                        'home_slider_caption' => 'customizer_repeater_star_3.5',
                    ),
                ));
            }
        }
        $testimonial_animation_speed = get_theme_mod('testimonial_animation_speed', 3000);
        $testimonial_smooth_speed = get_theme_mod('testimonial_smooth_speed', 1000);
        $isRTL = (is_rtl()) ? (bool) true : (bool) false;

        $slide_items = get_theme_mod('home_testimonial_slide_item', 1);
        $testimonial_nav_style = get_theme_mod('testimonial_nav_style', 'bullets');

        $testimonialsettings = array('design_id' => '#' . $designId, 'slide_items' => $slide_items, 'animationSpeed' => $testimonial_animation_speed, 'smoothSpeed' => $testimonial_smooth_speed, 'testimonial_nav_style' => $testimonial_nav_style, 'rtl' => $isRTL);

        wp_register_script('busicare-testimonial', BUSICAREP_PLUGIN_URL . '/inc/js/front-page/testi.js', array('jquery'));
        wp_localize_script('busicare-testimonial', 'testimonial_settings', $testimonialsettings);
        wp_enqueue_script('busicare-testimonial');
        $home_testimonial_section_title = get_theme_mod('home_testimonial_section_title', __('Our Clients Says', 'busicare-plus'));
        $home_testimonial_section_desc = get_theme_mod('home_testimonial_section_desc', __('industry is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think.', 'busicare-plus'));

        $testimonial_callout_background = get_theme_mod('testimonial_callout_background', BUSICAREP_PLUGIN_URL .'inc/images/bg/testimonial-bg.jpg');
        if ($testimonial_callout_background != '') {
            ?>
            <section class="section-space testimonial <?php echo esc_attr($sectionClass); ?>"  style="background:url('<?php echo esc_url($testimonial_callout_background); ?>') 112% 50% no-repeat; -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover; background-position: center center;
    background-repeat: no-repeat;">
                <?php
            } else {
                ?>
                <section class="section-space testimonial <?php echo esc_attr($sectionClass); ?>" >
                    <?php
                }?>
                <div class="overlay"></div>
                 <div class="busicare-tesi-container container">
                    <?php if ($home_testimonial_section_title != '' ) { ?>
                   <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="section-header">
                                <h2 class="section-title text-white"><?php echo esc_attr($home_testimonial_section_title); ?></h2>
                                <div class="title_seprater"></div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--Testimonial-->
                        <div class="row">
                            <?php if($type=='carousel'):?>
                            <div class="owl-carousel owl-theme col-md-12" id="<?php echo esc_attr($designId); ?>">
                                <?php endif;
                                $testimonial_options = json_decode($testimonial_options);
                                if ($testimonial_options != '') {
                                    $allowed_html = array(
                                        'br' => array(),
                                        'em' => array(),
                                        'strong' => array(),
                                        'b' => array(),
                                        'i' => array(),
                                    );
                                    foreach ($testimonial_options as $testimonial_iteam) {
                                        $title = !empty($testimonial_iteam->title) ? apply_filters('busicare_translate_single_string', $testimonial_iteam->title, 'Testimonial section') : '';
                                        $test_desc = !empty($testimonial_iteam->text) ? apply_filters('busicare_translate_single_string', $testimonial_iteam->text, 'Testimonial section') : '';
                                        $test_link = $testimonial_iteam->link;
                                        $open_new_tab = $testimonial_iteam->open_new_tab;
                                        $clientname = !empty($testimonial_iteam->clientname) ? apply_filters('busicare_translate_single_string', $testimonial_iteam->clientname, 'Testimonial section') : '';
                                        $designation = !empty($testimonial_iteam->designation) ? apply_filters('busicare_translate_single_string', $testimonial_iteam->designation, 'Testimonial section') : '';
                                        $stars = !empty($testimonial_iteam->home_testimonial_star) ? apply_filters('busicare_translate_single_string', $testimonial_iteam->home_testimonial_star, 'Testimonial section') : '';
                                        ?>
                                        <div <?php if($type=='grid') { ?>class="col-lg-6 col-md-6 col-sm-12"<?php } else { ?> class="item" <?php } ?>>
                                            <blockquote class="testmonial-block <?php echo esc_html($textAlign); ?>">
                                                <?php $default_arg = array('class' => "img-circle"); ?>
                                                <?php if ($testimonial_iteam->image_url != ''): ?>
                                                    <figure class="avatar">
                                                        <img src="<?php echo esc_url($testimonial_iteam->image_url); ?>" class="img-fluid rounded-circle" alt="img" >
                                                    </figure>
                                                <?php endif;
                                                if (($clientname != '' || $designation != '' ) && $designId == 'testimonial-carousel4') { ?>                              
                                                    <figcaption <?php echo $test_link . "-" . $open_new_tab; ?>>
                                                        <?php if (!empty($clientname)): if (!empty($test_link)){ ?>
                                                            <a href="<?php echo esc_url($test_link);?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>><?php }?>
                                                                <cite class="name"><?php echo esc_html($clientname); ?></cite><?php if (!empty($test_link)){ ?></a><?php } endif; ?>
                                                        <?php if (!empty($designation)): ?><span class="designation"><?php echo esc_html($designation); ?></span><?php endif; ?>
                                                    </figcaption>
                                                <?php } ?>
                                                <?php if (!empty($test_desc)): ?>
                                                    <div class="entry-content">
                                                        <?php if ($title != ''){ ?><h3 class="title"><span>“ <?php echo wp_kses_post(html_entity_decode($title), $allowed_html); ?> ”</span></h3><?php } ?>
                                                        <?php if ($test_desc != '') { ?><p class="description text-white" ><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></p> <?php } ?>
                                                    </div>  
                                                    <?php
                                                endif;

                                                if (($clientname != '' || $designation != '' ) && $designId != 'testimonial-carousel4') { ?>                              
                                                    <figcaption <?php echo $test_link . "-" . $open_new_tab; ?>>
                                                        <?php if (!empty($clientname)): 
                                                            if (!empty($test_link)){ ?>
                                                            <a href="<?php echo esc_url($test_link);?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>><?php }?>
                                                                <cite class="name"><?php echo esc_html($clientname); ?></cite><?php if (!empty($test_link)){ ?></a><?php } endif; ?>
                                                        <?php if (!empty($designation)): ?><span class="designation"><?php echo esc_html($designation); ?></span><?php endif; ?>
                                                    </figcaption>
                                                <?php } ?>

                                            </blockquote>
                                        </div>      
                                        <?php
                                    }
                                } else {
                                    $image = array('user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7', 'user8', 'user9', 'user10');
                                    $name = array(__('Martin Wills', 'busicare-plus'),
                                        __('Amanda Smith', 'busicare-plus'),
                                        __('Sue Thomas ', 'busicare-plus'),
                                    );
                                    $desc = array(__('Developer', 'busicare-plus'),
                                        __('Team Leader', 'busicare-plus'),
                                        __('Volunteer', 'busicare-plus'),
                                    );
                                    for ($i = 0; $i <= 2; $i++) {
                                        ?>  
                                        <div  class="item" >
                                            <blockquote class="testmonial-block <?php echo $textAlign; ?>">
                                                <figure class="avatar">
                                                    <img src="<?php echo esc_url(BUSICAREP_PLUGIN_URL.'/inc/images/user/'.$image[$i]); ?>.jpg" class="img-fluid rounded-circle" alt="Adriel Harlyn" >
                                                </figure>
                                                <?php if ($designId == 'testimonial-carousel4') { ?>
                                                    <figcaption>
                                                        <cite class="name <?php echo esc_attr($textColor); ?>"><?php echo esc_html($name[$i]); ?></cite>
                                                        <span class="designation <?php echo esc_attr($textColor); ?>"><?php echo esc_html($desc[$i]); ?></span>
                                                    </figcaption>
                                                <?php } ?>
                                                <div class="entry-content">
                                                    <h3 class="title"><span>“ <?php echo esc_html__('Exellent Theme & Very Fast Support');?> ”</span></h3>
                                                    <p class="description <?php echo esc_attr($textColor); ?>"><?php echo esc_html__('Lorem Ipsum is simply dummy text of the printing and typesetting industry  is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think. Lorem Ipsum is simply dummy text of the printing and typesetting industry  is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think.', 'busicare-plus'); ?></p>
                                                </div>  
                                                <?php if ($designId != 'testimonial-carousel4') { ?>
                                                    <figcaption>
                                                        <cite class="name <?php echo esc_attr($textColor); ?>"><?php echo esc_html($name[$i]); ?></cite>
                                                        <span class="designation <?php echo esc_attr($textColor); ?>"><?php echo esc_html($desc[$i]); ?></span>
                                                    </figcaption>
                                                <?php } ?>
                                                
                                            </blockquote>                                            
                                        </div>
                                        <?php
                                    }
                                }
                                if($type=='carousel'):?>    
                            </div>
                        <?php endif;?>
                        </div>
                    </div>
            </section>  
            <?php
        }

    }