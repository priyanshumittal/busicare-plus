<?php

/**
 * General Settings Customizer
 *
 * @package BusiCare
 */
function busicare_plus_general_settings_customizer($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

    class busicare_Header_Logo_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>
            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>
            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>
            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">
                <?php foreach ($this->choices as $value => $args) : ?>
                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />
                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) : ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                        <?php endif; ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], get_template_directory_uri(), get_stylesheet_directory_uri())); ?>"
                        <?php
                        if (!empty($args['label'])) :
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        endif;
                        ?>
                             />
                    </label>
            <?php endforeach; ?>
            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();


                if(jQuery("#_customize-input-after_menu_multiple_option").val()=='menu_btn')
                {
                    jQuery("#customize-control-after_menu_btn_txt").show();
                    jQuery("#customize-control-after_menu_btn_link").show();
                    jQuery("#customize-control-after_menu_btn_new_tabl").show();
                    jQuery("#customize-control-after_menu_btn_border").show();
                    jQuery("#customize-control-after_menu_html").hide();  
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                }
                else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='html')
                {
                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                    jQuery("#customize-control-after_menu_html").show(); 
                }
                else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='top_menu_widget')
                {

                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_html").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").show();
                }
                else
                {
                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_html").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                }
                
                //Js For Homepage Slider Variation
                if(jQuery("#_customize-input-slide_variation").val()=='slide')
                {
                    jQuery("#customize-control-slide_video_upload").hide();
                    jQuery("#customize-control-slide_video_url").hide();
                }
                else
                {
                    jQuery("#customize-control-slide_video_upload").show();
                    jQuery("#customize-control-slide_video_url").show();
                }
                });
            </script>
            <?php
        }

        /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>
            <style type="text/css" id="hybrid-customize-radio-image-css">
                .customize-control-radio-image .ui-buttonset {
                    text-align: center;
                }
                .footer_widget_area_placing-3, .footer_widget_area_placing-4, .footer_widget_area_placing-6
                {
                    max-width: 32% !important;
                }
                .footer_widget_area_placing-3 .wp-ui-highlight, .footer_widget_area_placing-4 .wp-ui-highlight, .footer_widget_area_placing-6 .wp-ui-highlight {
                    background-color: #E6E6E6 !important;
                }
                .customize-control-radio-image label {
                    display: inline-block;
                    max-width: 49%;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }
                .customize-control-radio-image label:first-of-type {
                    float: left;
                }
                .customize-control-radio-image label:hover {
                    background: none;
                    border-color: inherit;
                    color: inherit;
                }
                .customize-control-radio-image label:active {
                    background: none;
                    border-color: inherit;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    -webkit-transform: none;
                    -ms-transform: none;
                    transform: none;
                }
                .customize-control-radio-image img { border: 1px solid transparent; }
                .customize-control-radio-image .ui-state-active img {
                    border-color: #5B9DD9;
                    -webkit-box-shadow: 0 0 3px rgba(0,115,170,.8);
                    box-shadow: 0 0 3px rgba(0,115,170,.8);
                }
            </style>
            <?php
        }

    }

    $wp_customize->add_panel('busicare_general_settings',
            array(
                'priority' => 124,
                'capability' => 'edit_theme_options',
                'title' => esc_html__('General Settings', 'busicare-plus')
            )
    );

        // Preloader
    $wp_customize->add_section(
        'preloader_section',
        array(
            'title' =>esc_html__('Preloader','busicare-plus'),
            'panel'  => 'busicare_general_settings',
            'priority'   => 1,
            
            )
    );

     $wp_customize->add_setting('preloader_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'busicare_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new busicare_Toggle_Control( $wp_customize, 'preloader_enable',
        array(
            'label'    => esc_html__( 'Enable / Disable Preloader', 'busicare-plus' ),
            'section'  => 'preloader_section',
            'type'     => 'toggle',
            'priority' => 1,
        )
    ));

    if ( class_exists( 'busicare_Header_Logo_Customize_Control_Radio_Image' ) ) {
            $wp_customize->add_setting(
                'preloader_style', array(
                    'default'           => 1,
                )
            );

            $wp_customize->add_control(
                new busicare_Header_Logo_Customize_Control_Radio_Image(
                    $wp_customize, 'preloader_style', array(
                        'label'    => esc_html__('Preloader Style', 'busicare-plus' ),
                        'description' => esc_html__('You can change the color skin of only first and second Preloader styles', 'busicare-plus' ),
                        'priority' => 2,
                        'section' => 'preloader_section',
                        'choices' => array(
                            1 => array(
                                'url' => trailingslashit( BUSICAREP_PLUGIN_URL ) . 'inc/images/loader/preloader1.png',
                            ),
                            2 => array(
                                'url' => trailingslashit( BUSICAREP_PLUGIN_URL ) . 'inc/images/loader/preloader2.png',
                            ),
                            3 => array(
                                'url' => trailingslashit( BUSICAREP_PLUGIN_URL ) . 'inc/images/loader/preloader3.png',

                            ),
                            4 => array(
                                'url' => trailingslashit( BUSICAREP_PLUGIN_URL ) . 'inc/images/loader/preloader4.png',
                                
                            ),
                            5 => array(
                                'url' => trailingslashit( BUSICAREP_PLUGIN_URL ) . 'inc/images/loader/preloader5.png',
                                
                            ),
                            6 => array(
                                'url' => trailingslashit( BUSICAREP_PLUGIN_URL ) . 'inc/images/loader/preloader6.png',
                                
                            ),
                        ),
                    )
                )
            );
        }





    // After Menu
    $wp_customize->add_section(
        'after_menu_setting_section',
        array(
            'title' =>esc_html__('After Menu','busicare-plus'),
            'panel'  => 'busicare_general_settings',
            'priority'   => 3,
            )
    );

    //Dropdown button or html option
    $wp_customize->add_setting(
    'after_menu_multiple_option',
    array(
        'default'           =>  'none',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'busicare_plus_sanitize_select',
    ));
    $wp_customize->add_control('after_menu_multiple_option', array(
        'label' => esc_html__('After Menu','busicare-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_multiple_option',
        'type'    =>  'select',
        'choices' =>  array(
            'none'      =>  esc_html__('None', 'busicare-plus'),
            'menu_btn'  => esc_html__('Button', 'busicare-plus'),
            'html'      => esc_html__('HTML', 'busicare-plus'),
            'top_menu_widget' => esc_html__('Widget', 'busicare-plus'),
            )
    ));

    //After Menu Button Text
    $wp_customize->add_setting(
    'after_menu_btn_txt',
    array(
        'default'           =>  '',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'busicare_plus_home_page_sanitize_text',
    ));
    $wp_customize->add_control('after_menu_btn_txt', array(
        'label' => esc_html__('Button Text','busicare-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_txt',
        'type' => 'text',
    ));

    //After Menu Button Text
    $wp_customize->add_setting(
    'after_menu_btn_link',
    array(
        'default'           =>  '#',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'esc_url_raw',
    ));
    $wp_customize->add_control('after_menu_btn_link', array(
        'label' => esc_html__('Button Link','busicare-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_link',
        'type' => 'text',
    ));

    //Open in new tab
    $wp_customize->add_setting(
    'after_menu_btn_new_tabl',
    array(
        'default'           =>  false,
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'busicare_sanitize_checkbox',
    ) );
    
    $wp_customize->add_control('after_menu_btn_new_tabl', array(
        'label' => esc_html__('Open link in a new tab','busicare-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_new_tabl',
        'type'    =>  'checkbox'
    )); 

    //Border Radius
    $wp_customize->add_setting( 'after_menu_btn_border',
            array(
                'default' => 0,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'after_menu_btn_border',
            array(
                'label' => esc_html__( 'Button Border Radius', 'busicare-plus' ),
                'section' => 'after_menu_setting_section',
                'input_attrs' => array(
                    'min' => 0,
                    'max' => 30,
                    'step' => 1,),)
        ));

    //After Menu HTML section
    $wp_customize->add_setting('after_menu_html', 
        array(
        'default'=> '',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback'=> 'busicare_plus_home_page_sanitize_text',
        )
    );

    $wp_customize->add_control('after_menu_html', 
        array(
            'label'=> __('HTML','busicare-plus'),
            'section'=> 'after_menu_setting_section',
            'type'=> 'textarea',
        )
    );


    class WP_After_Menu_Widget_Customize_Control extends WP_Customize_Control {
        public $type = 'new_menu';
        /**
        * Render the control's content.
        */
        public function render_content() {
        ?>
         <h3><?php _e('To add widgets, Go to Widgets >> After Menu Widget Area','busicare-plus');?></h3>
        <?php
        }
    }
    $wp_customize->add_setting(
        'after_menu_widget_area_section',
        array(
            'capability'     => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        )   
    );
    $wp_customize->add_control( new WP_After_Menu_Widget_Customize_Control( $wp_customize, 'after_menu_widget_area_section', array( 
            'section' => 'after_menu_setting_section',
            'setting' => 'after_menu_widget_area_section',
        ))
    );

    //Enable / Disable Cart Icon
    $wp_customize->add_setting('cart_btn_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'busicare_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new busicare_Toggle_Control( $wp_customize, 'cart_btn_enable',
        array(
            'label'    => esc_html__( 'Enable / Disable Cart', 'busicare-plus' ),
            'section'  => 'after_menu_setting_section',
            'type'     => 'toggle',
        )
    ));

    // Header Preset Section
    $wp_customize->add_section(
            'header_preset_setting_section',
            array(
                'title' => esc_html__('Header Presets', 'busicare-plus'),
                'panel' => 'busicare_general_settings',
                'priority' => 99,
            )
    );
    if (class_exists('busicare_Header_Logo_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'header_logo_placing', array(
            'default' => 'left',
                )
        );
        $wp_customize->add_control(
                new busicare_Header_Logo_Customize_Control_Radio_Image(
                        $wp_customize, 'header_logo_placing', array(
                    'label' => esc_html__('Header layout with logo placing', 'busicare-plus'),
                    'priority' => 6,
                    'section' => 'header_preset_setting_section',
                    'choices' => array(
                        'left' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/container-right.png',
                        ),
                        'right' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/container-left.png',
                        ),
                        'center' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/center.png',
                        ),
                        'full' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/full-left.png',
                        ),
                        'five' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/5.png',
                            
                        ),
                        'six' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/6.png',
                            
                        ),
                        'seven' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/header-preset/7.png',
                            
                        ),
                        
                    
                    ),
                        )
                )
        );
    }
    
    // Sticky Header 
	$wp_customize->add_section(
        'sticky_header_section',
        array(
            'title' =>esc_html__('Sticky Header','busicare-plus'),
			'panel'  => 'busicare_general_settings',
			'priority'   => 99,
			
			)
    );

     $wp_customize->add_setting('sticky_header_enable',
		array(
			'default' => false,
			'sanitize_callback' => 'busicare_sanitize_checkbox'
			)
	);

	$wp_customize->add_control(new busicare_Toggle_Control( $wp_customize, 'sticky_header_enable',
		array(
			'label'    => esc_html__( 'Enable / Disable Sticky Header', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'toggle',
			'priority' 				=> 1,
		)
	));

	//Differet logo for sticky header
	$wp_customize->add_setting('sticky_header_logo',
		array(
			'default' => false,
			'sanitize_callback' => 'busicare_sanitize_checkbox'
			)
	);

	$wp_customize->add_control(new busicare_Toggle_Control( $wp_customize, 'sticky_header_logo',
		array(
			'label'    => esc_html__( 'Differet logo for sticky header', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'toggle',
			'priority' 	=> 2,
		)
	));

	// Stick Header logo for Desktop
	$wp_customize->add_setting( 'sticky_header_logo_desktop', array(
			  'sanitize_callback' => 'esc_url_raw',
			) );
			
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sticky_header_logo_desktop', array(
			  'label'    => esc_html__( 'Logo for desktop view', 'busicare-plus' ),
			  'section'  => 'sticky_header_section',
			  'settings' => 'sticky_header_logo_desktop',
			  'active_callback' => 'busicare_plus_sticky_header_logo_callback',
			  'priority' 	=> 3,
			) ) );

	// Stick Header logo for Mobile
	$wp_customize->add_setting( 'sticky_header_logo_mbl', array(
			  'sanitize_callback' => 'esc_url_raw',
			) );
			
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sticky_header_logo_mbl', array(
			  'label'    => esc_html__( 'Logo for mobile view', 'busicare-plus' ),
			  'section'  => 'sticky_header_section',
			  'settings' => 'sticky_header_logo_mbl',
			  'active_callback' => 'busicare_plus_sticky_header_logo_callback',
			  'priority' 	=> 4,
			) ) );

	//Sticky Header Animation Effect
	$wp_customize->add_setting( 'sticky_header_animation', array( 'default' => '') );
	$wp_customize->add_control(	'sticky_header_animation', 
		array(
			'label'    => esc_html__( 'Animation Effect', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'select',
			'choices'=>array(
				''=>esc_html__('None', 'busicare-plus'),
				'slide'=>esc_html__('Slide', 'busicare-plus'),
				'fade'=>esc_html__('Fade', 'busicare-plus'),
				'shrink'=>esc_html__('Shrink', 'busicare-plus'),
				)
	));

	//Sticky Header Enable
	$wp_customize->add_setting( 'sticky_header_device_enable', array( 'default' => 'desktop') );
	$wp_customize->add_control(	'sticky_header_device_enable', 
		array(
			'label'    => esc_html__( 'Enable', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'select',
			'choices'=>array(
				'desktop'=>esc_html__('Desktop', 'busicare-plus'),
				'mobile'=>esc_html__('Mobile', 'busicare-plus'),
				'both'=>esc_html__('Desktop + Mobile', 'busicare-plus')
				)
	));
        
        //Sticky Header Opacity
	$wp_customize->add_setting( 'sticky_header_device_enable', array( 'default' => 'desktop') );
	$wp_customize->add_control(	'sticky_header_device_enable', 
		array(
			'label'    => esc_html__( 'Enable', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'select',
			'choices'=>array(
				'desktop'=>esc_html__('Desktop', 'busicare-plus'),
				'mobile'=>esc_html__('Mobile', 'busicare-plus'),
				'both'=>esc_html__('Desktop + Mobile', 'busicare-plus')
				)
	));

	$wp_customize->add_setting('sticky_header_opacity',
		array(
			'default' => 1.0,
			'capability'     => 'edit_theme_options',
			));

	$wp_customize->add_control(new busicare_plus_Opacity_Control( $wp_customize, 'sticky_header_opacity',
		array(
			'label'    => esc_html__( 'Sticky Opacity', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'slider',
			'min' 	=> 0.1,
			'max'	=> 1.0,
		)
	));
        
        //Sticky Header Height
	$wp_customize->add_setting('sticky_header_height',
		array(
			'default' => 0,
			'capability'     => 'edit_theme_options',
			)
	);

	$wp_customize->add_control(new busicare_plus_Slider_Control( $wp_customize, 'sticky_header_height',
		array(
			'label'    => esc_html__( 'Sticky Height', 'busicare-plus' ),
                        'description'    => esc_html__( 'Note: Sticky Height will not work with shrink effect', 'busicare-plus' ),
			'section'  => 'sticky_header_section',
			'type'     => 'slider',
			'min' 	=> 0,
			'max'	=> 50,
		)
	));
    
    // Search Effect settings
    $wp_customize->add_section(
            'search_effect_setting_section',
            array(
                'title' => esc_html__('Search settings', 'busicare-plus'),
                'panel' => 'busicare_general_settings',
                'priority' => 100,
            )
    );
    //Enable/Disable Search Effect
    $wp_customize->add_setting('search_btn_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'busicare_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'search_btn_enable',
                    array(
                'label' => esc_html__('Enable / Disable Search Icon', 'busicare-plus'),
                'section' => 'search_effect_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));
    $wp_customize->add_setting('search_effect_style_setting',
            array(
                'default' => 'toggle',
                'sanitize_callback' => 'busicare_plus_sanitize_select'
            )
    );
    $wp_customize->add_control('search_effect_style_setting',
            array(
                'label' => esc_html__('Choose Position', 'busicare-plus'),
                'section' => 'search_effect_setting_section',
                'type' => 'radio',
                'active_callback' => 'search_icon_hide_show_callback',
                'choices' => array(
                    'toggle' => esc_html__('Toggle', 'busicare-plus'),
                    'popup_light' => esc_html__('Pop up light', 'busicare-plus'),
                    'popup_dark' => esc_html__('Pop up dark', 'busicare-plus'),
                )
            )
    );

    // add section to manage breadcrumb settings
    $wp_customize->add_section(
            'breadcrumb_setting_section',
            array(
                'title' => esc_html__('Breadcrumb settings', 'busicare-plus'),
                'panel' => 'busicare_general_settings',
                'priority' => 100,
            )
    );

    // enable/disable banner
    $wp_customize->add_setting(
            'banner_enable',
            array('capability' => 'edit_theme_options',
                'default' => true,
    ));

    $wp_customize->add_control(
            'banner_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable banner', 'busicare-plus'),
                'section' => 'breadcrumb_setting_section',
            )
    );

    // enable/disable breadcrumb
    $wp_customize->add_setting(
            'breadcrumb_setting_enable',
            array('capability' => 'edit_theme_options',
                'default' => true,
    ));

    $wp_customize->add_control(
            'breadcrumb_setting_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable breadcrumbs', 'busicare-plus'),
                'section' => 'breadcrumb_setting_section',
            )
    );
		//Dropdown button or html option
	$wp_customize->add_setting(
    'busicare_breadcrumb_type',
    array(
        'default'           =>  'yoast',
		'capability'        =>  'edit_theme_options',
		'sanitize_callback' =>  'busicare_sanitize_select',
    ));
	$wp_customize->add_control('busicare_breadcrumb_type', array(
		'label' => esc_html__('Breadcrumb type','busicare-plus'),
		'description' => esc_html__( 'If you use other than "default" one you will need to install and activate respective plugins Breadcrumb NavXT, Yoast SEO and Rank Math SEO', 'busicare-plus' ),
        'section' => 'breadcrumb_setting_section',
		'setting' => 'busicare_breadcrumb_type',
		'type'    =>  'select',
		'choices' =>  array(
			'default' => __( 'Default(Blank)', 'busicare-plus' ),
            'yoast'  => __( 'Yoast SEO', 'busicare-plus' ),
            'rankmath'  => __( 'Rank Math', 'busicare-plus' ),
			'navxt'  => __( 'NavXT', 'busicare-plus' ),
			)
	));
    
    // Image overlay
	$wp_customize->add_setting( 'breadcrumb_image_overlay', array(
		'default' => true,
		'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
		) 
	);
		
	$wp_customize->add_control('breadcrumb_image_overlay', array(
		'label'    => esc_html__('Enable Breadcrumb image overlay', 'busicare-plus' ),
		'section' => 'breadcrumb_setting_section',
		'type' => 'checkbox',
		) 
	);
    
    //breadcrumb overlay color
    $wp_customize->add_setting( 'breadcrumb_overlay_section_color', array(
		'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
		'default' => 'rgba(0,0,0,0.6)',
        ) 
	);	
            
    $wp_customize->add_control(new busicare_plus_Customize_Alpha_Color_Control( $wp_customize,'breadcrumb_overlay_section_color', array(
        'label'      => esc_html__('Breadcrumb image overlay color','busicare-plus' ),
        'palette' => true,
        'section' => 'breadcrumb_setting_section')
    ) );

    // Container Setting
    $wp_customize->add_section(
        'container_width_section',
        array(
            'title' =>__('Container Settings','busicare-plus'),
            'panel'  => 'busicare_general_settings',
            'priority'   => 102,
            
            )
    );
    //Bg Image for 404 page
    $wp_customize->add_setting( 'error_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'error_background_img', array(
              'label'    => __( '404 Page', 'busicare-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'error_background_img',
    ) ) );

    //Bg Image for search page
    $wp_customize->add_setting( 'search_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'search_background_img', array(
              'label'    => __( 'Search Page', 'busicare-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'search_background_img',
    ) ) );

    //Bg Image for date page
    $wp_customize->add_setting( 'date_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'date_background_img', array(
              'label'    => __( 'Date Archive', 'busicare-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'date_background_img',
    ) ) );

    //Bg Image for author page
    $wp_customize->add_setting( 'author_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'author_background_img', array(
              'label'    => __( 'Author Archive', 'busicare-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'author_background_img',
    ) ) );
    
    //Container width
    $wp_customize->add_setting( 'container_width_pattern',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_width_pattern',
            array(
                'label'    => __( 'Container Width', 'busicare-plus' ),
                'priority' => 1,
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));


    $wp_customize->add_setting( 'page_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'page_container_setting', 
        array(
            'label'    => __( 'Page layout', 'busicare-plus' ),
            'priority' => 2,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'busicare-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'busicare-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'busicare-plus'),
                )
        ));
        $wp_customize->add_setting( 'post_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'post_container_setting', 
        array(
            'label'    => __( 'Blog layout', 'busicare-plus' ),
            'priority' => 3,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'busicare-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'busicare-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'busicare-plus'),
                )
        ));
        $wp_customize->add_setting( 'single_post_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'single_post_container_setting', 
        array(
            'label'    => __( 'Single Post layout', 'busicare-plus' ),
            'priority' => 4,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'busicare-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'busicare-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'busicare-plus'),
                )
        ));


class WP_Home_Container_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
     <h3><?php esc_attr_e('Homepage sections container width','busicare-plus'); ?></h3>
    <?php
    }
}
$wp_customize->add_setting(
    'business_temp_container_width',
    array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )   
);
$wp_customize->add_control( new WP_Home_Container_Customize_Control( $wp_customize, 'business_temp_container_width', array( 
        'section' => 'container_width_section',
        'setting' => 'business_temp_container_width',
    ))
);

        //Container Width For Slider Section
    $wp_customize->add_setting( 'container_slider_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_slider_width',
            array(
                'label'    => __( 'Slider', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));

    //Container Width For Callout Section
    $wp_customize->add_setting( 'container_cta1_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_cta1_width',
            array(
                'label'    => __( 'Callout 1', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Service Section
    $wp_customize->add_setting( 'container_service_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_service_width',
            array(
                'label'    => __( 'Service', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Callout 2 Section
    $wp_customize->add_setting( 'container_cta2_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_cta2_width',
            array(
                'label'    => __( 'Callout 2', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 
    
    //Container Width For Portfolio Section
    $wp_customize->add_setting( 'container_portfolio_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_portfolio_width',
            array(
                'label'    => __( 'Portfolio', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Fun&Fact Section
    $wp_customize->add_setting( 'container_fun_fact_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_fun_fact_width',
            array(
                'label'    => __( 'FunFact', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Testimonial Section
    $wp_customize->add_setting( 'container_testimonial_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_testimonial_width',
            array(
                'label'    => __( 'Testimonial', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));


    //Container Width For Blog Section
    $wp_customize->add_setting( 'container_home_blog_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_home_blog_width',
            array(
                'label'    => __( 'Blog', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For TEAM Section
    $wp_customize->add_setting( 'container_team_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_team_width',
            array(
                'label'    => __( 'Team', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 


    //Container Width For SHOP Section
    $wp_customize->add_setting( 'container_shop_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_shop_width',
            array(
                'label'    => __( 'Shop', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 


    //Container Width For Client & Partners Section
    $wp_customize->add_setting( 'container_clients_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new Busicare_Slider_Custom_Control( $wp_customize, 'container_clients_width',
            array(
                'label'    => __( 'Clients', 'busicare-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));


      // Post Navigation effect
    $wp_customize->add_section(
            'post_navigation_section',
            array(
                'title' => esc_html__('Post Navigation Style', 'busicare-plus'),
                'panel' => 'busicare_general_settings',
                'priority' => 102,
            )
    );
    $wp_customize->add_setting('post_nav_style_setting',
            array(
                'default' => 'pagination',
                'sanitize_callback' => 'busicare_plus_sanitize_select'
            )
    );

    $wp_customize->add_control('post_nav_style_setting',
            array(
                'label' => esc_html__('Choose Position', 'busicare-plus'),
                'section' => 'post_navigation_section',
                'type' => 'radio',
                'choices' => array(
                    'pagination' => esc_html__('Pagination', 'busicare-plus'),
                    'load_more' => esc_html__('Load More', 'busicare-plus'),
                    'infinite' => esc_html__('Infinite Scroll', 'busicare-plus'),
                )
            )
    );

    // add section to manage scroll_to_top icon settings
    $wp_customize->add_section(
            'scrolltotop_setting_section',
            array(
                'title' => esc_html__('Scroll to Top settings', 'busicare-plus'),
                'panel' => 'busicare_general_settings',
                'priority' => 102,
            )
    );

    // enable/disable scroll_to_top icon
    $wp_customize->add_setting(
            'scrolltotop_setting_enable',
            array('capability' => 'edit_theme_options',
                'default' => true,
    ));

    $wp_customize->add_control(
            'scrolltotop_setting_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable Scroll to top icon', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
            )
    );


    $wp_customize->add_setting('scroll_position_setting',
            array(
                'default' => 'right',
                'sanitize_callback' => 'busicare_plus_sanitize_select'
            )
    );

    $wp_customize->add_control('scroll_position_setting',
            array(
                'label' => esc_html__('Choose Position', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'type' => 'radio',
                'choices' => array(
                    'left' => esc_html__('Left', 'busicare-plus'),
                    'right' => esc_html__('Right', 'busicare-plus'),
                )
            )
    );


    $wp_customize->add_setting('busicare_scroll_icon_class',
            array(
                'default' => esc_html__('fa fa-arrow-up', 'busicare-plus'),
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            )
    );
    $wp_customize->add_control('busicare_scroll_icon_class',
            array(
                'label' => esc_html__('Icon Class Name', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'type' => 'text',
            )
    );

    $wp_customize->add_setting('busicare_scroll_border_radius',
            array(
                'default' => 50,
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'busicare_plus_sanitize_number_range',
            )
    );
    $wp_customize->add_control('busicare_scroll_border_radius',
            array(
                'label' => esc_html__('Border Radius', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'type' => 'number',
                'input_attrs' => array('min' => 0, 'max' => 50, 'step' => 1, 'style' => 'width: 100%;'),
            )
    );

    // enable/disable Callout section from service page
    $wp_customize->add_setting(
            'apply_scrll_top_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_scrll_top_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable To Apply Color Setting', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
            )
    );



    $wp_customize->add_setting(
            'busicare_scroll_bg_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#22a2c4'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'busicare_scroll_bg_color',
                    array(
                'label' => esc_html__('Background color', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'settings' => 'busicare_scroll_bg_color',
    )));


    $wp_customize->add_setting(
            'busicare_scroll_icon_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'busicare_scroll_icon_color',
                    array(
                'label' => esc_html__('Icon color', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'settings' => 'busicare_scroll_icon_color',
    )));


    $wp_customize->add_setting(
            'busicare_scroll_bghover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'busicare_scroll_bghover_color',
                    array(
                'label' => esc_html__('Background Hover color', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'settings' => 'busicare_scroll_bghover_color',
    )));


    $wp_customize->add_setting(
            'busicare_scroll_iconhover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#22a2c4'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'busicare_scroll_iconhover_color',
                    array(
                'label' => esc_html__('Icon Hover color', 'busicare-plus'),
                'section' => 'scrolltotop_setting_section',
                'settings' => 'busicare_scroll_iconhover_color',
    )));



    // Sidebar Layout
    $wp_customize->add_section('sidebar_layout_setting_section',
        array(
            'title'     => esc_html__('Sidebar Layout','busicare-plus' ),
            'panel'     => 'busicare_general_settings',
            'priority'   => 105
        )
    );

    /*Blog/Archive sidebar layout*/
    $wp_customize->add_setting( 'blog_sidebar_layout',
        array(
            'default'           => 'right',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_sanitize_select'
        )
    );
    $wp_customize->add_control( new Busicare_Image_Radio_Button_Custom_Control( $wp_customize, 'blog_sidebar_layout',
        array(
            'label'     => esc_html__( 'Blog/Archives', 'busicare-plus'  ),
            'section'   => 'sidebar_layout_setting_section',
            'priority'  => 1,
            'choices'   => 
            array(
                'right' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/right.jpg'),
                'left' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/left.jpg'),
                'full' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/full.jpg'),
                'stretched' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/stretched.jpg')   
            )
        )
    ));

    /*Single post sidebar layout*/
    $wp_customize->add_setting( 'single_post_sidebar_layout',
        array(
            'default'           => 'right',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_sanitize_select'
        )
    );
    $wp_customize->add_control( new Busicare_Image_Radio_Button_Custom_Control( $wp_customize, 'single_post_sidebar_layout',
        array(
            'label'     => esc_html__( 'Single Post', 'busicare-plus'  ),
            'section'   => 'sidebar_layout_setting_section',
            'priority'  => 2,
            'choices'   => 
            array(
                'right' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/right.jpg'),
                'left' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/left.jpg'),
                'full' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/full.jpg'),
                'stretched' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/stretched.jpg')   
            )
        )
    ));
    /*Page sidebar layout*/
    $wp_customize->add_setting( 'page_sidebar_layout',
        array(
            'default'           => 'right',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_sanitize_select'
        )
    );
    $wp_customize->add_control( new Busicare_Image_Radio_Button_Custom_Control( $wp_customize, 'page_sidebar_layout',
        array(
            'label'     => esc_html__( 'Page', 'busicare-plus'  ),
            'section'   => 'sidebar_layout_setting_section',
            'priority'  => 3,
            'choices'   => 
            array(
                'right' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/right.jpg'),
                'left' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/left.jpg'),
                'full' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/full.jpg'),
                'stretched' => array('image' => trailingslashit( get_template_directory_uri() ) . 'assets/images/stretched.jpg')   
            )
        )
    ));
}

add_action('customize_register', 'busicare_plus_general_settings_customizer');