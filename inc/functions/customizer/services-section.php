<?php
$theme = wp_get_theme();
if('BusiCare Dark' == $theme->name) {$bc_service_design=2;}
else{$bc_service_design=1;}
$wp_customize->add_section('services_section', array(
    'title' => esc_html__('Services settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 12,
));

// Enable service more btn
$wp_customize->add_setting('home_service_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'home_service_section_enabled',
                array(
            'label' => esc_html__('Enable Services on homepage', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'services_section',
                )
));

//Style Design
$wp_customize->add_setting('home_serive_design_layout', array('default' => $bc_service_design));
$wp_customize->add_control('home_serive_design_layout',
        array(
            'label' => esc_html__('Design Style', 'busicare-plus'),
            'section' => 'services_section',
            'type' => 'select',
            'choices' => array(
                1 => esc_html__('Design 1', 'busicare-plus'),
                2 => esc_html__('Design 2', 'busicare-plus'),
                3 => esc_html__('Design 3', 'busicare-plus'),
                4 => esc_html__('Design 4', 'busicare-plus')
            ),
            'active_callback' => 'busicare_plus_service_callback'
));

//Service section title
$wp_customize->add_setting('home_service_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Why Choose Us?', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_service_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_service_callback'
));

// Service section description
$wp_customize->add_setting('home_service_section_discription', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Our Services', 'busicare-plus'),
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_service_section_discription', array(
    'label' => esc_html__('Sub title', 'busicare-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_service_callback'
));

if (class_exists('busicare_plus_Repeater')) {
    $wp_customize->add_setting('busicare_service_content', array());

    $wp_customize->add_control(new busicare_plus_Repeater($wp_customize, 'busicare_service_content', array(
                'label' => esc_html__('Services content', 'busicare-plus'),
                'section' => 'services_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Service', 'busicare-plus'),
                'item_name' => esc_html__('Service', 'busicare-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'busicare_plus_service_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_service_section_title', array(
    'selector' => '.services .section-title, .services2 .section-title, .services3 .section-title, .services4 .section-title',
    'settings' => 'home_service_section_title',
    'render_callback' => 'busicare_plus_home_service_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_service_section_discription', array(
    'selector' => '.services .section-subtitle, .services2 .section-subtitle, .services3 .section-subtitle, .services4 .section-subtitle',
    'settings' => 'home_service_section_discription',
    'render_callback' => 'busicare_plus_home_service_section_discription_render_callback'
));

$wp_customize->selective_refresh->add_partial('service_viewmore_btn_text', array(
    'selector' => '.services .view-more-services',
    'settings' => 'service_viewmore_btn_text',
    'render_callback' => 'busicare_plus_service_viewmore_btn_text_render_callback'
));

function busicare_plus_home_service_section_title_render_callback() {
    return get_theme_mod('home_service_section_title');
}

function busicare_plus_home_service_section_discription_render_callback() {
    return get_theme_mod('home_service_section_discription');
}

function busicare_plus_service_viewmore_btn_text_render_callback() {
    return get_theme_mod('service_viewmore_btn_text');
}
