<?php
$wp_customize->add_section('portfolio_section', array(
    'title' => esc_html__('Portfolio settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 14,
));

// Enable portfolio more btn
$wp_customize->add_setting('portfolio_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'portfolio_section_enable',
                array(
            'label' => esc_html__('Enable Home Portfolio section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'portfolio_section',
                )
));

//Column Layout
$wp_customize->add_setting(
        'home_portfolio_column_laouts',
        array(
            'default' => 4,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        )
);
$wp_customize->add_control('home_portfolio_column_laouts', array(
    'type' => 'radio',
    'label' => esc_html__('Portfolio Column layout section', 'wallstreet'),
    'section' => 'portfolio_section',
    'choices' => array(3 => '4 Column Layout', 4 => '3 Column Layout', 6 => '2 Column Layout'),
    'active_callback' => 'busicare_plus_portfolio_callback'
        )
);
// Number of Portfolio in Portfolio's Section
$wp_customize->add_setting(
        'home_portfolio_numbers_options',
        array(
            'default' => 3,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        )
);
$wp_customize->add_control('home_portfolio_numbers_options',
        array(
            'type' => 'number',
            'label' => esc_html__('Number of portfolio on portfolio section', 'busicare-plus'),
            'section' => 'portfolio_section',
            'input_attrs' => array(
                'min' => '1', 'step' => '1', 'max' => '50',
            ),
            'active_callback' => 'busicare_plus_portfolio_callback'
        )
);

// room section title
$wp_customize->add_setting('home_portfolio_section_title', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'default' => esc_html__('Look at our projects', 'busicare-plus'),
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_portfolio_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_portfolio_callback'
));

$wp_customize->add_setting('home_portfolio_section_subtitle', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'default' => esc_html__('View Our Recent Works', 'busicare-plus'),
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_portfolio_section_subtitle', array(
    'label' => esc_html__('Sub Title', 'busicare-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_portfolio_callback'
));

//link
class WP_client_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    /**
     * Render the control's content.
     */
    public function render_content() {
        ?>
        <a href="<?php bloginfo('url'); ?>/wp-admin/edit.php?post_type=busicare_portfolio" class="button"  target="_blank"><?php esc_html_e('Click here to add project', 'busicare-plus'); ?></a>
        <?php
    }

}

$wp_customize->add_setting(
        'pro_project',
        array(
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        )
);

$wp_customize->add_control(new WP_client_Customize_Control($wp_customize, 'pro_project', array(
            'section' => 'portfolio_section',
            'active_callback' => 'busicare_plus_portfolio_callback'
                ))
);

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('home_portfolio_section_title', array(
    'selector' => '.home-portfolio h2',
    'settings' => 'home_portfolio_section_title',
    'render_callback' => 'busicare_plus_home_portfolio_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_portfolio_section_subtitle', array(
    'selector' => '.home-portfolio h5',
    'settings' => 'home_portfolio_section_subtitle',
    'render_callback' => 'busicare_plus_home_portfolio_section_subtitle_render_callback'
));

function busicare_plus_home_portfolio_section_title_render_callback() {
    return get_theme_mod('home_portfolio_section_title');
}

function busicare_plus_home_portfolio_section_subtitle_render_callback() {
    return get_theme_mod('home_portfolio_section_subtitle');
}
