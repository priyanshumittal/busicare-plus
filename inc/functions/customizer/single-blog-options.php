<?php

/**
 * Single Blog Options Customizer
 *
 * @package BusiCare
 */
function busicare_plus_single_blog_customizer($wp_customize) {
      
/************************* Blog Button Title*********************************/
$wp_customize->add_setting( 'busicare_blog_button_title',
    array(
        'default'           => esc_html__('Read More','busicare-plus'),
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'busicare_home_page_sanitize_text',  
        )
    );
$wp_customize->add_control( 'busicare_blog_button_title',
    array(
        'label'    => esc_html__( 'Excerpt Read More Button Title', 'busicare-plus' ),
        'section'  => 'busicare_blog_section',
        'type'     => 'text',  
        'priority' => 4, 
        )
    );

/************************* Enable Author*********************************/
$wp_customize->add_setting( 'busicare_enable_blog_author',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'busicare_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new Busicare_Toggle_Control( $wp_customize,  'busicare_enable_blog_author',
    array(
        'label'    => esc_html__( 'Hide / Show Author', 'busicare-plus' ),
        'section'  => 'busicare_blog_section',
        'type'     => 'toggle', 
        'priority' => 5,
        )
    ));

/************************* Enable Date*********************************/
$wp_customize->add_setting( 'busicare_enable_blog_date',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'busicare_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new Busicare_Toggle_Control( $wp_customize,  'busicare_enable_blog_date',
    array(
        'label'    => esc_html__( 'Hide / Show Date', 'busicare-plus' ),
        'section'  => 'busicare_blog_section',
        'type'     => 'toggle', 
        'priority' => 6,
        )
    ));

/************************* Enable Category*********************************/
$wp_customize->add_setting( 'busicare_enable_blog_category',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'busicare_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new Busicare_Toggle_Control( $wp_customize,   'busicare_enable_blog_category',
    array(
        'label'    => esc_html__( 'Hide / Show Category', 'busicare-plus' ),
        'section'  => 'busicare_blog_section',
        'type'     => 'toggle', 
        'priority' => 7,
        )
    ));

/************************* Enable Continue Reading Button*********************************/
$wp_customize->add_setting( 'busicare_enable_blog_read_button',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'busicare_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new Busicare_Toggle_Control( $wp_customize, 'busicare_enable_blog_read_button',
    array(
        'label'    => esc_html__( 'Hide / Show Read More Button', 'busicare-plus' ),
        'section'  => 'busicare_blog_section',
        'type'     => 'toggle', 
        'priority' => 8,
        )
    ));

    $wp_customize->add_setting('busicare_enable_related_post',
            array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'busicare_enable_related_post',
                    array(
                'label' => esc_html__('Enable Related Post', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'busicare_single_blog_section',
                'priority' => 1,
                    )
    ));

    /*     * *********************** Related Post Title  ******************************** */

    $wp_customize->add_setting('busicare_related_post_title',
            array(
                'default' => esc_html__('Related Posts', 'busicare-plus'),
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control('busicare_related_post_title',
            array(
                'label' => esc_html__('Related Post Title', 'busicare-plus'),
                'type' => 'text',
                'section' => 'busicare_single_blog_section',
                'priority' => 2,
                'active_callback' => 'busicare_plus_rt_post_callback',
            )
    );

    /*     * *********************** Related Post Option ******************************** */

    $wp_customize->add_setting('busicare_related_post_option',
            array(
                'default' => esc_html__('categories', 'busicare-plus'),
                'sanitize_callback' => 'busicare_sanitize_select'
            )
    );

    $wp_customize->add_control('busicare_related_post_option',
            array(
                'label' => esc_html__('Related Post Option', 'busicare-plus'),
                'section' => 'busicare_single_blog_section',
                'settings' => 'busicare_related_post_option',
                'active_callback' => 'busicare_plus_rt_post_callback',
                'priority' => 3,
                'type' => 'select',
                'choices' => array(
                    'categories' => esc_html__('All', 'busicare-plus'),
                    'tags' => esc_html__('Related by tags', 'busicare-plus'),
                )
            )
    );
}

add_action('customize_register', 'busicare_plus_single_blog_customizer');