<?php

/**
 * Template Options Customizer
 *
 * @package BusiCare
 */
function busicare_plus_template_customizer($wp_customize) {
     $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

    $wp_customize->add_panel('busicare_template_settings',
            array(
                'priority' => 920,
                'capability' => 'edit_theme_options',
                'title' => esc_html__('Template Settings', 'busicare-plus')
            )
    );


//     add section to manage All Blog page settings
    $wp_customize->add_section('blog_page_section',
            array(
                'title' => esc_html__('Blog templates setting', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 100,
            )
    );

    // enable/disable Sponsors section from all blog page
    $wp_customize->add_setting('blog_sponsors_enable',array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
        'blog_sponsors_enable',array(
                'label' => esc_html__('Enable Sponsor section in all blog templates', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'blog_page_section',
            )
    ));
//     add section to manage All Blog page settings

// add section to manage About us page settings
    $wp_customize->add_section('about_page_section', array(
                'title' => esc_html__('About Us page settings', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 100,
            )
    );

    // enable/disable Testimonial section from about page
    $wp_customize->add_setting('about_testimonial_enable',array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
            'about_testimonial_enable',
            array(
                'label' => esc_html__('Enable Testimonial section', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'about_page_section',
            )
    ));

    // enable/disable Team section from about page
    $wp_customize->add_setting('about_team_enable',array(
        'default' => true,
        'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
            'about_team_enable',
            array(
                
                'label' => esc_html__('Enable Team section', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'about_page_section',
            )
    ));


    // enable/disable Client Section
    $wp_customize->add_setting('about_client_enable',array(
        'default' => true,
        'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
            'about_client_enable',array(
                'label' => esc_html__('Enable Client section', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'about_page_section',
            )
    ));



// Add section to manage Service page settings

    $wp_customize->add_section('service_page_section',array(
                'title' => esc_html__('Service page settings', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 200,
            )
    );


    // enable/disable Service section from service page
    $wp_customize->add_setting('services_enable',array(
               'default' => true,
               'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
            'services_enable',array(
                'label' => esc_html__('Enable Service section', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'service_page_section',
            )
    ));

    // enable/disable Callout section from service page
    $wp_customize->add_setting('service_cta2_enable',array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
        'service_cta2_enable',array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable CTA 2 section', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'service_page_section',
            )
    ));

    // enable/disable Client Section from service page
    $wp_customize->add_setting('service_client_enable',array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
            'service_client_enable',array(
                'label' => esc_html__('Enable Client section', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'service_page_section',
            )
    ));



//   add section to manage Error page settings

    $wp_customize->add_section('error_page_section',array(
                'title' => esc_html__('Error page settings', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 220,
            )
    );

    // enable/disable Sponsors section from Error page
    $wp_customize->add_setting('error_sponsors_enable',array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize,
        'error_sponsors_enable',array(
                'label' => esc_html__('Enable Sponsor section for 404 ( Error ) page', 'busicare-plus'),
                'type' => 'toggle',
                'section' => 'error_page_section',
            )
    ));

    
// Add section to manage Portfolio page settings
    $wp_customize->add_section(
            'porfolio_page_section',
            array(
                'title' => esc_html__('Portfolio page settings', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 300,
            )
    );

    // Portfolio page title
    $wp_customize->add_setting(
            'porfolio_page_title', array(
        'default' => esc_html__('Our Portfolio', 'busicare-plus'),
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));

    $wp_customize->add_control(
            'porfolio_page_title',
            array(
                'type' => 'text',
                'label' => esc_html__('Title', 'busicare-plus'),
                'section' => 'porfolio_page_section',
            )
    );

    // Portfolio page subtitle
    $wp_customize->add_setting(
            'porfolio_page_subtitle', array(
        'default' => esc_html__('Our Recent Works', 'busicare-plus'),
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));

    $wp_customize->add_control(
            'porfolio_page_subtitle',
            array(
                'type' => 'text',
                'label' => esc_html__('Sub Title', 'busicare-plus'),
                'section' => 'porfolio_page_section',
            )
    );

    // Number of Portfolio in Portfolio's template
    $wp_customize->add_setting(
            'portfolio_numbers_options',
            array(
                'default' => 4,
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            )
    );
    $wp_customize->add_control('portfolio_numbers_options',
            array(
                'type' => 'number',
                'label' => esc_html__('Number of portfolio on portfolio templates', 'busicare-plus'),
                'section' => 'porfolio_page_section',
                'input_attrs' => array(
                    'min' => '1', 'step' => '1', 'max' => '50',
                ),
            )
    );


    // Add section to manage Portfolio category page settings
    $wp_customize->add_section(
            'porfolio_category_page_section',
            array(
                'title' => esc_html__('Portfolio Category page settings', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 400,
            )
    );
    // Portfolio category page title
    $wp_customize->add_setting(
            'porfolio_category_page_title', array(
        'default' => esc_html__('Portfolio Category Title', 'busicare-plus'),
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        'transport' => $selective_refresh,
    ));
    $wp_customize->add_control(
            'porfolio_category_page_title',
            array(
                'type' => 'text',
                'label' => esc_html__('Title', 'busicare-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    // Portfolio category page subtitle
    $wp_customize->add_setting(
            'porfolio_category_page_desc', array(
        'default' => esc_html__('Morbi facilisis, ipsum ut ullamcorper venenatis, nisi diam placerat turpis, at auctor nisi magna cursus arcu.', 'busicare-plus'),
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        'transport' => $selective_refresh,
    ));
    $wp_customize->add_control(
            'porfolio_category_page_desc',
            array(
                'type' => 'text',
                'label' => esc_html__('Description', 'busicare-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    // Number of portfolio column layout
    $wp_customize->add_setting('portfolio_cat_column_layout', array(
        'default' => 3,
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));
    $wp_customize->add_control('portfolio_cat_column_layout', array(
        'type' => 'select',
        'label' => esc_html__('Select column layout', 'busicare-plus'),
        'section' => 'porfolio_category_page_section',
        'choices' => array(2 => 2, 3 => 3, 4 => 4),
    ));

    // Add section to manage Contact page settings
    $wp_customize->add_section(
            'contact_page_section',
            array(
                'title' => esc_html__('Contact page setting', 'busicare-plus'),
                'panel' => 'busicare_template_settings',
                'priority' => 500,
            )
    );

    // Contact form title
    $wp_customize->add_setting(
            'contact_cf7_title', array(
        'default' => esc_html__('Get In Touch', 'busicare-plus'),
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));

    $wp_customize->add_control(
            'contact_cf7_title',
            array(
                'type' => 'text',
                'label' => esc_html__('Contact Form Title', 'busicare-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact form 7 shortcode
    $wp_customize->add_setting('contact_form_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));
    $wp_customize->add_control('contact_form_shortcode', array(
        'label' => esc_html__('Contact Form shortcode', 'busicare-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // Conatct Info title
    $wp_customize->add_setting(
            'contact_info_title', array(
        'default' => esc_html__('Contact Info', 'busicare-plus'),
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));

    $wp_customize->add_control(
            'contact_info_title',
            array(
                'type' => 'text',
                'label' => esc_html__('Contact Title', 'busicare-plus'),
                'section' => 'contact_page_section',
            )
    );

    if (class_exists('busicare_plus_Repeater')) {
        $wp_customize->add_setting('busicare_contact_content', array());

        $wp_customize->add_control(new busicare_plus_Repeater($wp_customize, 'busicare_contact_content', array(
                    'label' => esc_html__('Contact Details', 'busicare-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Contact Info', 'busicare-plus'),
                    'item_name' => esc_html__('Contact', 'busicare-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_title_control' => true,
                    'customizer_repeater_text_control' => true,
        )));
    }

    if (class_exists('busicare_plus_Repeater')) {
        $wp_customize->add_setting('busicare_social_links', array());

        $wp_customize->add_control(new busicare_plus_Repeater($wp_customize, 'busicare_social_links', array(
                    'label' => esc_html__('Social Links', 'busicare-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Social Link', 'busicare-plus'),
                    'item_name' => esc_html__('Social Link', 'busicare-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_link_control' => true,
        )));
    }

    $wp_customize->add_setting(
            'contact_template_viewmore_btn_link_target',
            array('sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));

    $wp_customize->add_control(
            'contact_template_viewmore_btn_link_target',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Open Social links in new tab', 'busicare-plus'),
                'section' => 'contact_page_section',
                // 'active_callback' => 'busicare_plus_service_callback'
            )
    );

    // google map shortcode
    $wp_customize->add_setting('contact_google_map_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        'transport' => $selective_refresh,
    ));
    $wp_customize->add_control('contact_google_map_shortcode', array(
        'label' => __('Google Map Shortcode', 'busicare-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // enable/disable Funfact section from about page
    $wp_customize->add_setting(
            'contact_client_enable',
            array('capability' => 'edit_theme_options',
                'default' => true,
    ));

    $wp_customize->add_control(
            'contact_client_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable Client section', 'busicare-plus'),
                'section' => 'contact_page_section',
            )
    );

    $wp_customize->selective_refresh->add_partial('contact_cf7_title', array(
        'selector' => '.section-space-page.contact .contant-form .title h4',
        'settings' => 'contact_cf7_title',
        'render_callback' => 'busicare_plus_contact_cf7_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('contact_info_title', array(
        'selector' => '.section-space-page.contact .sidebar_contact h4',
        'settings' => 'contact_info_title',
        'render_callback' => 'busicare_plus_contact_info_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_title', array(
        'selector' => '.portfoliocat h2',
        'settings' => 'porfolio_category_page_title',
        'render_callback' => 'busicare_plus_porfolio_category_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_desc', array(
        'selector' => '.portfoliocat h5',
        'settings' => 'porfolio_category_page_desc',
        'render_callback' => 'busicare_plus_porfolio_category_page_desc_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_title', array(
        'selector' => '.portfolio-template-col h2',
        'settings' => 'porfolio_page_title',
        'render_callback' => 'busicare_plus_porfolio_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_subtitle', array(
        'selector' => '.portfolio-template-col h5',
        'settings' => 'porfolio_page_subtitle',
        'render_callback' => 'busicare_plus_porfolio_page_subtitle_render_callback'
    ));

    function busicare_plus_contact_cf7_title_render_callback() {
        return get_theme_mod('contact_cf7_title');
    }

    function busicare_plus_contact_info_title_render_callback() {
        return get_theme_mod('contact_info_title');
    }

    function busicare_plus_porfolio_category_page_title_render_callback() {
        return get_theme_mod('porfolio_category_page_title');
    }

    function busicare_plus_porfolio_category_page_desc_render_callback() {
        return get_theme_mod('porfolio_category_page_desc');
    }

    function busicare_plus_porfolio_page_title_render_callback() {
        return get_theme_mod('porfolio_page_title');
    }

    function busicare_plus_porfolio_page_subtitle_render_callback() {
        return get_theme_mod('porfolio_page_subtitle');
    }

}

add_action('customize_register', 'busicare_plus_template_customizer');
?>