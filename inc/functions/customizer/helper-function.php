<?php 
/**
 * Helper functions.
 *
 * @package BusiCare
 */
/**
 * Get Footer widgets
 */
if (!function_exists('busicare_plus_footer_widget_area')) {

    /**
     * Get Footer Default Sidebar
     *
     * @param  string $sidebar_id   Sidebar Id..
     * @return void
     */
    function busicare_plus_footer_widget_area($sidebar_id) {

        if (is_active_sidebar($sidebar_id)) {
            dynamic_sidebar($sidebar_id);
        } elseif (current_user_can('edit_theme_options')) {

            global $wp_registered_sidebars;
            $sidebar_name = '';
            if (isset($wp_registered_sidebars[$sidebar_id])) {
                $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
            }
            ?>
            <div class="widget ast-no-widget-row">
                <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                <p class='no-widget-text'>
                    <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                        <?php esc_html_e('Click here to assign a widget for this area.', 'busicare-plus'); ?>
                    </a>
                </p>
            </div>
            <?php
        }
    }

}

/**
 * Function to get Footer Menu
 */
if (!function_exists('busicare_plus_footer_bar_menu')) {

    /**
     * Function to get Footer Menu
     *
     */
    function busicare_plus_footer_bar_menu() {

        ob_start();

        if (has_nav_menu('footer_menu')) {
            wp_nav_menu(
                    array(
                        'theme_location' => 'footer_menu',
                        'menu_class' => 'nav-menu',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 1,
                    )
            );
        } else {
            if (is_user_logged_in() && current_user_can('edit_theme_options')) {
                ?>
                <a href="<?php echo esc_url(admin_url('/nav-menus.php?action=locations')); ?>"><?php esc_html_e('Assign Footer Menu', 'busicare-plus'); ?></a>
                <?php
            }
        }

        return ob_get_clean();
    }

}

if (!function_exists('busicare_plus_widget_layout')):

    function busicare_plus_widget_layout() {

        $busicare_footer_widget = get_theme_mod('footer_widgets_section', 4);
        switch ($busicare_footer_widget) {
            case 1:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-1.php');
                break;

            case 2:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-2.php');
                break;

            case 3:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-3.php');
                break;

            case 4:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-4.php');
                break;

            case 5:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-5.php');
                break;

            case 6:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-6.php');
                break;

            case 7:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-7.php');
                break;

            case 8:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-8.php');
                break;

            case 9:
                include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-9.php');
                break;
        }
    }

endif;

/* Footer Widget Layout section */
if (!function_exists('busicare_plus_footer_widget_layout_section')) {

    function busicare_plus_footer_widget_layout_section() {
            /**
             * Get Footer widgets
             */
            if (!function_exists('busicare_plus_footer_widget_area')) {

                /**
                 * Get Footer Default Sidebar
                 *
                 * @param  string $sidebar_id   Sidebar Id..
                 * @return void
                 */
                function busicare_plus_footer_widget_area($sidebar_id) {

                    if (is_active_sidebar($sidebar_id)) {
                        dynamic_sidebar($sidebar_id);
                    } elseif (current_user_can('edit_theme_options')) {

                        global $wp_registered_sidebars;
                        $sidebar_name = '';
                        if (isset($wp_registered_sidebars[$sidebar_id])) {
                            $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
                        }
                        ?>
                        <div class="widget ast-no-widget-row">
                            <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                            <p class='no-widget-text'>
                                <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                                    <?php esc_html_e('Click here to assign a widget for this area.', 'busicare-plus'); ?>
                                </a>
                            </p>
                        </div>
                        <?php
                    }
                }

            }
            /* Function for widget sectons */
            busicare_plus_widget_layout();
        /* Function for widget sectons */

        
    }

}
/* Footer Widget Layout section */

/* Footer Bar layout section */
if (!function_exists('busicare_plus_footer_bar_section')) {

    function busicare_plus_footer_bar_section() {
        if (get_theme_mod('ftr_bar_enable', true) == true):
            echo '<div class="site-info">';
            $advance_footer_bar_section = get_theme_mod('advance_footer_bar_section', 1);
            switch ($advance_footer_bar_section) {
                case 1:
                    include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-1.php');
                    break;

                case 2:
                    include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-2.php');
                    break;
            }
        endif;
        echo '</div>';
    }

}
/* Footer Bar layout section */

if ( ! function_exists( 'busicare_plus_custom_navigation' ) ) :
    function busicare_plus_custom_navigation() {
        if(get_theme_mod('post_nav_style_setting','pagination')=='pagination'){
            echo '<div class="row justify-content-center">';
                if (!is_rtl()) {
                    the_posts_pagination(array(
                        'prev_text' => __('<i class="fa fa-angle-double-left"></i>', 'busicare-plus'),
                        'next_text' => __('<i class="fa fa-angle-double-right"></i>', 'busicare-plus'),
                    ));
                } else {
                    the_posts_pagination(array(
                        'prev_text' => __('<i class="fa fa-angle-double-right"></i>', 'busicare-plus'),
                        'next_text' => __('<i class="fa fa-angle-double-left"></i>', 'busicare-plus'),
                    ));
                }
                echo '</div>';
        }else{
            echo do_shortcode('[ajax_posts]');
        }
    }
endif;
add_action( 'busicarep_post_navigation', 'busicare_plus_custom_navigation' );

/*  Related posts
/* ------------------------------------ */
if ( ! function_exists( 'busicare_plus_related_posts' ) ) {
    function busicare_plus_related_posts() {
        wp_reset_postdata();
        global $post;

        // Define shared post arguments
        $args = array(
            'no_found_rows'             => true,
            'update_post_meta_cache'    => false,
            'update_post_term_cache'    => false,
            'ignore_sticky_posts'       => 1,
            'orderby'                   => 'rand',
            'post__not_in'              => array($post->ID),
            'posts_per_page'            => 10
        );
        // Related by categories
        if ( get_theme_mod('busicare_related_post_option') == 'categories' ) {
            
            $cats = get_post_meta($post->ID, 'related-cat', true);
            
            if ( !$cats ) {
                $cats = wp_get_post_categories($post->ID, array('fields'=>'ids'));
                $args['category__in'] = $cats;
            } else {
                $args['cat'] = $cats;
            }
        }
        // Related by tags
        if ( get_theme_mod('busicare_related_post_option') == 'tags' ) {
        
            $tags = get_post_meta($post->ID, 'related-tag', true);
            
            if ( !$tags ) {
                $tags = wp_get_post_tags($post->ID, array('fields'=>'ids'));
                $args['tag__in'] = $tags;
            } else {
                $args['tag_slug__in'] = explode(',', $tags);
            }
            if ( !$tags ) { $break = true; }
        }
        
        $query = !isset($break)?new WP_Query($args):new WP_Query;
        return $query;
    }
}



add_action('busicare_sticky_header_logo','busicare_plus_sticky_header_logo_fn');
function busicare_plus_sticky_header_logo_fn()
{
$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
  if(get_theme_mod('sticky_header_device_enable','desktop')=='desktop')
{
  $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop','');
  if(!empty($sticky_header_logo_desktop)):
  ?>
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo" style="display: none;">
    <img src="<?php echo esc_url($sticky_header_logo_desktop);?>" class="custom-logo"></a>
  <?php 
else:
  ?>
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo" style="display: none;">
    <img src="<?php echo esc_url($image[0]);?>" class="custom-logo"></a>
  <?php
endif;
}

elseif( get_theme_mod('sticky_header_device_enable','desktop')=='mobile')
{
  $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl','');
   if(!empty($sticky_header_logo_mbl)):
  ?>
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
    <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl);?>" class="custom-logo"></a>
  <?php 
  else: ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
    <img width="280" height="48" src="<?php echo esc_url($image[0]);?>" class="custom-logo"></a>
<?php endif;
}
else
{
  $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop','');
  if(!empty($sticky_header_logo_desktop)):
  ?>
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo" style="display: none;">
    <img src="<?php echo esc_url($sticky_header_logo_desktop);?>" class="custom-logo"></a>
  <?php
  else: ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo" style="display: none;">
    <img src="<?php echo esc_url($image[0]);?>" class="custom-logo"></a>
<?php endif;

  $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl','');
   if(!empty($sticky_header_logo_mbl)):
  ?>
  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
    <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl);?>" class="custom-logo"></a>
  <?php
  else: ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
    <img width="280" height="48" src="<?php echo esc_url($image[0]);?>" class="custom-logo"></a>
<?php endif;
}
}

//Add Footer hook
add_action('busicare_plus_footer_section_hook','busicare_plus_footer_section_hook');
function busicare_plus_footer_section_hook()
{ busicare_plus_before_footer_section_trigger();?>
    <footer class="site-footer" <?php if (!empty(get_theme_mod('ftr_wgt_background_img'))): ?> style="background-image: url(<?php echo get_theme_mod('ftr_wgt_background_img'); ?>);" <?php endif; ?>>  
        <?php $fwidgets_overlay_section_color = get_theme_mod('busicare_fwidgets_overlay_section_color', 'rgba(0, 0, 0, 0.7)'); ?>
        <div class="overlay" <?php
        if (!empty(get_theme_mod('ftr_wgt_background_img'))) {
            if (get_theme_mod('busicare_fwidgets_image_overlay', true) == true) {
                ?> style="background-color:<?php echo $fwidgets_overlay_section_color; ?>" <?php
                 }
             }
             ?>>

            <?php if (get_theme_mod('ftr_widgets_enable', true) === true) { ?>
                <div class="container">
                    <?php
    //        Widget Layout
                    busicare_plus_footer_widget_layout_section();
                    ?>
                </div>
            <?php } ?>



            <?php
    //    Footer bar
            busicare_plus_footer_bar_section();
            ?>
        </div>
    </footer>
    <?php busicare_plus_after_footer_section_trigger(); ?>
    <?php
    $client_bg_clr=get_theme_mod('clt_bg_color','#fff');
    $ribon_enable = get_theme_mod('scrolltotop_setting_enable', true);
    if ($ribon_enable == true) {
        ?>
        <div class="scroll-up custom <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>"><a href="#totop"><i class="<?php echo get_theme_mod('busicare_scroll_icon_class', 'fa fa-arrow-up'); ?>"></i></a></div>
    <?php } ?>

    <style type="text/css">
        .scroll-up {
            <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>: 30px !important;
        }
        .scroll-up.custom a {
            border-radius: <?php echo get_theme_mod('busicare_scroll_border_radius', 50); ?>px;
        }
        <?php if (get_theme_mod('apply_scrll_top_clr_enable', false) == true): ?>
            .scroll-up.custom a {
                background: <?php echo get_theme_mod('busicare_scroll_bg_color', '#22a2c4'); ?>;
                color: <?php echo get_theme_mod('busicare_scroll_icon_color', '#fff'); ?>;
            }
            .scroll-up.custom a:hover,
            .scroll-up.custom a:active {
                background: <?php echo get_theme_mod('busicare_scroll_bghover_color', '#fff'); ?>;
                color: <?php echo get_theme_mod('busicare_scroll_iconhover_color', '#22a2c4'); ?>;
            }
        <?php endif; ?>
    </style>
    <style type="text/css">
        .sponsors {
            background-color: <?php echo $client_bg_clr; ?>;
        }
    </style>
    <style type="text/css">
        .site-footer {
            background-repeat: <?php echo get_theme_mod('footer_widget_reapeat', 'no-repeat'); ?>;
            background-position: <?php echo get_theme_mod('footer_widget_position', 'left top'); ?>;
            background-size: <?php echo get_theme_mod('footer_widget_bg_size', 'cover'); ?>;
            background-attachment: <?php echo get_theme_mod('footer_widget_bg_attachment', 'scroll'); ?>;
        }
    </style>

    <style type="text/css">
        
        <?php
        if (get_theme_mod('testimonial_image_overlay', true) != false) {
            $testimonial_overlay_section_color = get_theme_mod('testimonial_overlay_section_color', 'rgba(1, 7, 12, 0.8)');
            ?>
            .testimonial .overlay {
                background-color:<?php echo $testimonial_overlay_section_color; ?>;
            }
            
            <?php } ?>

        </style>
        <?php
    //Stick Header
        if (get_theme_mod('sticky_header_enable', false) == true):
            include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/sticky-header/sticky-with' . get_theme_mod('sticky_header_animation', '') . '-animation.php');
        endif;
}


// Add heder feature
add_action('busicare_plus_header_feaure_section_hook','busicare_plus_header_feaure_section_hook');
function busicare_plus_header_feaure_section_hook(){
 include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/topbar-header.php');
  busicare_plus_before_header_section_trigger();
            
//Header Preset
    if(get_theme_mod('header_logo_placing','left')):

     $header_logo_path=BUSICAREP_PLUGIN_DIR.'/inc/inc/header-preset/menu-with-'.get_theme_mod('header_logo_placing','left').'-logo.php';
     include_once($header_logo_path);
    endif;
    if(get_theme_mod('search_effect_style_setting','toggle')!='toggle'):?>
        <div id="searchbar_fullscreen" <?php if(get_theme_mod('search_effect_style_setting','popup_light')=='popup_light'):?> class="bg-light" <?php endif;?>>
             <button type="button" class="close">×</button>
             <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url( home_url( '/' ));?>"><label><input autofocus type="search" class="search-field" placeholder="Search Keyword" value="" name="s" id="s" autofocus></label><input type="submit" class="search-submit btn" value="<?php echo esc_html__('Search','busicare-plus');?>"></form>
          </div>
<?php endif;
busicare_plus_after_header_section_trigger(); 
}

// Client section hook
function busicare_plus_client_section_fn(){
$error_sponsors_enable=get_theme_mod('error_sponsors_enable', true);
if ($error_sponsors_enable == true) {
    include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}
}
add_action('busicare_plus_client_section_hook','busicare_plus_client_section_fn');

//Container Setting For Page
function busicare_container()
{
  if(get_theme_mod('page_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('page_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Container Setting For Blog Post
function busicare_blog_post_container()
{
  if(get_theme_mod('post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Conainer Setting For Single Post

function busicare_single_post_container()
{
  if(get_theme_mod('single_post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('single_post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Preloader feature section function
function busicare_plus_preloader_feaure_section_fn(){
global $template;
$col=explode("-",basename($template));
if (array_key_exists(1,$col)){
$column=$col[1];
}
else{
$column='';
}
//Preloader
if(get_theme_mod('preloader_enable',false)==true && ($column!='portfolio')):
 include_once(BUSICAREP_PLUGIN_DIR.'/inc/inc/preloader/preloader-'.get_theme_mod('preloader_style',1).'.php');
endif;
}
add_action('busicare_plus_preloader_feaure_section_hook','busicare_plus_preloader_feaure_section_fn');