<?php

//Callout Section
$wp_customize->add_section('home_cta2_page_section', array(
    'title' => esc_html__('Callout 2 section settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 13,
));

// Enable call to action section
$wp_customize->add_setting('cta2_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'cta2_section_enable',
                array(
            'label' => esc_html__('Enable Home Callout 2 section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'home_cta2_page_section',
                )
));

//cta2 Background Image
$wp_customize->add_setting('cta2_background', array(
    'default' => BUSICAREP_PLUGIN_URL .'inc/images/bg/bg-img.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'cta2_background', array(
            'label' => esc_html__('Background Image', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'settings' => 'cta2_background',
            'active_callback' => 'busicare_plus_cta2_callback'
        )));

// Image overlay
$wp_customize->add_setting('cta2_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
));

$wp_customize->add_control('cta2_image_overlay', array(
    'label' => esc_html__('Enable callout image overlay', 'busicare-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'checkbox',
    'active_callback' => 'busicare_plus_cta2_callback'
));


//callout Background Overlay Color
$wp_customize->add_setting('cta2_overlay_section_color', array(
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'default' => 'rgba(0, 11, 24, 0.8)',
));

$wp_customize->add_control(new busicare_plus_Customize_Alpha_Color_Control($wp_customize, 'cta2_overlay_section_color', array(
            'label' => esc_html__('cta2 image overlay color', 'busicare-plus'),
            'palette' => true,
            'active_callback' => 'busicare_plus_cta2_callback',
            'section' => 'home_cta2_page_section')
));




$wp_customize->add_setting(
        'home_cta2_title',
        array(
            'default' => __('<span>Easy & Simple</span> - No Coding Required!', 'busicare-plus'),
            'transport' => $selective_refresh,
        )
);
$wp_customize->add_control('home_cta2_title', array(
    'label' => __('Title', 'busicare-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_cta2_callback'
    ));

$wp_customize->add_setting(
        'home_cta2_desc',
        array(
            'default' => __('It is a long established fact that a reader will be distracted by the readable content of a page when looking <br> at its layout. The point of using Lorem ipsum dolor sit amet elit.', 'busicare-plus'),
            'transport' => $selective_refresh,
        )
);
$wp_customize->add_control('home_cta2_desc', array(
    'label' => esc_html__('Description', 'busicare-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'textarea',
    'active_callback' => 'busicare_plus_cta2_callback'
    ));

//button text 1
$wp_customize->add_setting(
        'home_cta2_btn1_text',
        array(
            'default' => esc_html__('Purchase Now', 'busicare-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_cta2_btn1_text',
        array(
            'label' => esc_html__('Button Text 1', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'busicare_plus_cta2_callback'
));

//Button link 1
$wp_customize->add_setting(
        'home_cta2_btn1_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_cta2_btn1_link',
        array(
            'label' => esc_html__('Button Link 1', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'busicare_plus_cta2_callback'
));

//button 1 target
$wp_customize->add_setting(
        'home_cta2_btn1_link_target',
        array('sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
));

$wp_customize->add_control(
        'home_cta2_btn1_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in new tab', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'active_callback' => 'busicare_plus_cta2_callback'
        )
);

//button text 2
$wp_customize->add_setting(
        'home_cta2_btn2_text',
        array(
            'default' => esc_html__('Get In Touch', 'busicare-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_cta2_btn2_text',
        array(
            'label' => esc_html__('Button Text 2', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'busicare_plus_cta2_callback'
));

//Button link 2
$wp_customize->add_setting(
        'home_cta2_btn2_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_cta2_btn2_link',
        array(
            'label' => esc_html__('Button Link 2', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'busicare_plus_cta2_callback'
));

//button 2 target
$wp_customize->add_setting(
        'home_cta2_btn2_link_target',
        array('sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
));

$wp_customize->add_control(
        'home_cta2_btn2_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in new tab', 'busicare-plus'),
            'section' => 'home_cta2_page_section',
            'active_callback' => 'busicare_plus_cta2_callback'
        )
);


/**
 * Add selective refresh for Front page pricing section controls.
 */
$wp_customize->selective_refresh->add_partial('home_cta2_title', array(
    'selector' => '.cta-2 .cta-block h2',
    'settings' => 'home_cta2_title',
    'render_callback' => 'busicare_plus_home_cta2_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_desc', array(
    'selector' => '.cta-2 .cta-block p',
    'settings' => 'home_cta2_desc',
    'render_callback' => 'busicare_plus_home_cta2_desc_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_btn1_text', array(
    'selector' => '.cta-2 .btn-default',
    'settings' => 'home_cta2_btn1_text',
    'render_callback' => 'busicare_plus_home_cta2_btn1_text_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_btn2_text', array(
    'selector' => '.cta-2 .btn-light',
    'settings' => 'home_cta2_btn2_text',
    'render_callback' => 'busicare_plus_home_cta2_btn2_text_render_callback',
));

function busicare_plus_home_cta2_title_render_callback() {
    return get_theme_mod('home_cta2_title');
}

function busicare_plus_home_cta2_desc_render_callback() {
    return get_theme_mod('home_cta2_desc');
}

function busicare_plus_home_cta2_btn1_text_render_callback() {
    return get_theme_mod('home_cta2_btn1_text');
}

function busicare_plus_home_cta2_btn2_text_render_callback() {
    return get_theme_mod('home_cta2_btn2_text');
}
?>