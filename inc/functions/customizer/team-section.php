<?php

//Team Section
$wp_customize->add_section('busicare_team_section', array(
    'title' => esc_html__('Team settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 19,
));

$wp_customize->add_setting('team_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'team_section_enable',
                array(
            'label' => esc_html__('Enable Home Team section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'busicare_team_section',
                )
));

// Team section title
$wp_customize->add_setting('home_team_section_title', array(
    'default' => esc_html__('The Team', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_team_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'busicare_team_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_team_callback'
));

//Team section discription
$wp_customize->add_setting('home_team_section_discription', array(
    'default' => esc_html__('Meet Our Experts', 'busicare-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_team_section_discription', array(
    'label' => esc_html__('Sub title', 'busicare-plus'),
    'section' => 'busicare_team_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_team_callback'
));

//Style Design
$wp_customize->add_setting('home_team_design_layout', array('default' => 1));
$wp_customize->add_control('home_team_design_layout',
        array(
            'label' => esc_html__('Design Style', 'busicare-plus'),
            'active_callback' => 'busicare_plus_team_callback',
            'section' => 'busicare_team_section',
            'type' => 'select',
            'choices' => array(
                1 => esc_html__('Design 1', 'busicare-plus'),
                2 => esc_html__('Design 2', 'busicare-plus'),
                3 => esc_html__('Design 3', 'busicare-plus'),
                4 => esc_html__('Design 4', 'busicare-plus')
            ),

));


if (class_exists('busicare_plus_Repeater')) {
    $wp_customize->add_setting(
            'busicare_team_content', array(
            )
    );

    $wp_customize->add_control(
            new busicare_plus_Repeater(
                    $wp_customize, 'busicare_team_content', array(
                'label' => esc_html__('Team content', 'busicare-plus'),
                'section' => 'busicare_team_section',
                'priority' => 15,
                'add_field_label' => esc_html__('Add new Team Member', 'busicare-plus'),
                'item_name' => esc_html__('Team Member', 'busicare-plus'),
                'customizer_repeater_member_name_control' => true,
                'customizer_repeater_designation_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_repeater_control' => true,
                'active_callback' => 'busicare_plus_team_callback'
                    )
            )
    );
}

// animation speed
$wp_customize->add_setting('team_animation_speed', array('default' => 3000));
$wp_customize->add_control('team_animation_speed',
        array(
            'label' => esc_html__('Animation speed', 'busicare-plus'),
            'section' => 'busicare_team_section',
            'type' => 'select',
            'priority' => 53,
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'busicare_plus_team_callback'
));

//Navigation Type
$wp_customize->add_setting('team_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('team_nav_style', array(
    'label' => esc_html__('Navigation Style', 'busicare-plus'),
    'section' => 'busicare_team_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => esc_html__('Bullets', 'busicare-plus'),
        'navigation' => esc_html__('Navigation', 'busicare-plus'),
        'both' => esc_html__('Both', 'busicare-plus'),
    ),
    'active_callback' => 'busicare_plus_team_callback'
));


// smooth speed
$wp_customize->add_setting('team_smooth_speed', array('default' => 1000));
$wp_customize->add_control('team_smooth_speed',
        array(
            'label' => esc_html__('Smooth speed', 'busicare-plus'),
            'section' => 'busicare_team_section',
            'type' => 'select',
            'priority' => 17,
            'active_callback' => 'busicare_plus_team_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));

/**
 * Add selective refresh for Front page team section controls.
 */
$wp_customize->selective_refresh->add_partial('home_team_section_title', array(
    'selector' => '.team-group h2, .team2 .section-title, .team3 .section-title, .team4 .section-title',
    'settings' => 'home_team_section_title',
    'render_callback' => 'busicare_plus_home_team_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_team_section_discription', array(
    'selector' => '.team-group h5, .team2 .section-subtitle, .team3 .section-subtitle, .team4 .section-subtitle',
    'settings' => 'home_team_section_discription',
    'render_callback' => 'busicare_plus_home_team_section_discription_render_callback',
));

function busicare_plus_home_team_section_title_render_callback() {
    return get_theme_mod('home_team_section_title');
}

function busicare_plus_home_team_section_discription_render_callback() {
    return get_theme_mod('home_team_section_discription');
}

?>