<?php

/* Testimonial Section */
$wp_customize->add_section('testimonial_section', array(
    'title' => esc_html__('Testimonials settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 17,
));

// Enable testimonial section
$wp_customize->add_setting('testimonial_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'testimonial_section_enable',
                array(
            'label' => esc_html__('Enable Home Testimonial section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'testimonial_section',
                )
));

//Style Design
$wp_customize->add_setting('home_testimonial_design_layout', array('default' => 1));
$wp_customize->add_control('home_testimonial_design_layout',
        array(
            'label' => __('Design Style', 'busicare-plus'),
            'active_callback' => 'busicare_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => esc_html__('Design 1', 'busicare-plus'),
                2 => esc_html__('Design 2', 'busicare-plus'),
                3 => esc_html__('Design 3', 'busicare-plus'),
            )
));

//Slide Item
$wp_customize->add_setting('home_testimonial_slide_item', array('default' => 1));
$wp_customize->add_control('home_testimonial_slide_item',
        array(
            'label' => esc_html__('Slide Item', 'busicare-plus'),
            'active_callback' => 'busicare_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => esc_html__('One', 'busicare-plus'),
                2 => esc_html__('Two', 'busicare-plus'),
                3 => esc_html__('Three', 'busicare-plus'),
            )
));

// testimonial section title
$wp_customize->add_setting('home_testimonial_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Our Clients Says', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_testimonial_callback'
));

if (class_exists('busicare_plus_Repeater')) {
    $wp_customize->add_setting('busicare_testimonial_content', array(
    ));

    $wp_customize->add_control(new busicare_plus_Repeater($wp_customize, 'busicare_testimonial_content', array(
                'label' => esc_html__('Testimonial content', 'busicare-plus'),
                'section' => 'testimonial_section',
                'add_field_label' => esc_html__('Add new Testimonial', 'busicare-plus'),
                'item_name' => esc_html__('Testimonial', 'busicare-plus'),
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_user_name_control' => true,
                'customizer_repeater_designation_control' => true,
                'active_callback' => 'busicare_plus_testimonial_callback'
            )));
}

//Testimonial Background Image
$wp_customize->add_setting('testimonial_callout_background', array(
    'default' => BUSICAREP_PLUGIN_URL .'inc/images/bg/testimonial-bg.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'testimonial_callout_background', array(
            'label' => esc_html__('Background Image', 'busicare-plus'),
            'section' => 'testimonial_section',
            'settings' => 'testimonial_callout_background',
            'active_callback' => 'busicare_plus_testimonial_callback'
        )));

// Image overlay
$wp_customize->add_setting('testimonial_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
));

$wp_customize->add_control('testimonial_image_overlay', array(
    'label' => esc_html__('Enable testimonial image overlay', 'busicare-plus'),
    'section' => 'testimonial_section',
    'type' => 'checkbox',
    'active_callback' => 'busicare_plus_testimonial_callback'
));


//Testimonial Background Overlay Color
$wp_customize->add_setting('testimonial_overlay_section_color', array(
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'default' => 'rgba(0, 11, 24, 0.8)',
));

$wp_customize->add_control(new busicare_plus_Customize_Alpha_Color_Control($wp_customize, 'testimonial_overlay_section_color', array(
            'label' => esc_html__('Testimonial image overlay color', 'busicare-plus'),
            'palette' => true,
            'active_callback' => 'busicare_plus_testimonial_callback',
            'section' => 'testimonial_section')
));

//Navigation Type
$wp_customize->add_setting('testimonial_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('testimonial_nav_style', array(
    'label' => esc_html__('Navigation Style', 'busicare-plus'),
    'section' => 'testimonial_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => __('Bullets', 'busicare-plus'),
        'navigation' => __('Navigation', 'busicare-plus'),
        'both' => __('Both', 'busicare-plus'),
    ),
    'active_callback' => 'busicare_plus_testimonial_callback'
));

// animation speed
$wp_customize->add_setting('testimonial_animation_speed', array('default' => 3000));
$wp_customize->add_control('testimonial_animation_speed',
        array(
            'label' => esc_html__('Animation speed', 'busicare-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'busicare_plus_testimonial_callback'
));

// smooth speed
$wp_customize->add_setting('testimonial_smooth_speed', array('default' => 1000));
$wp_customize->add_control('testimonial_smooth_speed',
        array(
            'label' => esc_html__('Smooth speed', 'busicare-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'busicare_plus_testimonial_callback'
));


/**
 * Add selective refresh for Front page testimonial section controls.
 */
$wp_customize->selective_refresh->add_partial('home_testimonial_section_title', array(
    'selector' => '.testimonial h2',
    'settings' => 'home_testimonial_section_title',
    'render_callback' => 'busicare_plus_home_testimonial_section_title_render_callback',
));

function busicare_plus_home_testimonial_section_title_render_callback() {
    return get_theme_mod('home_testimonial_section_title');
}

?>