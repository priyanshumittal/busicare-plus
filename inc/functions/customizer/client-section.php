<?php

//Client Section

$wp_customize->add_section('home_client_section', array(
    'title' => esc_html__('Clients settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 20,
));

// Enable client section
$wp_customize->add_setting('client_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'client_section_enable',
                array(
            'label' => esc_html__('Enable Home Client section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'home_client_section',
                )
));

// clients & partners section title
$wp_customize->add_setting('home_client_section_title', array(
    'default' => esc_html__('We Work With The Best Clients', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_client_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'home_client_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_sponsors_callback'
));

//clients & partners section discription
$wp_customize->add_setting('home_client_section_discription', array(
    'default' => esc_html__('It is a long established fact that a reader will be distracted by the readable content.', 'busicare-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_client_section_discription', array(
    'label' => esc_html__('Sub title', 'busicare-plus'),
    'section' => 'home_client_section',
    'type' => 'textarea',
    'active_callback' => 'busicare_plus_sponsors_callback'
));

//Testimonial Background Overlay Color
$wp_customize->add_setting('clt_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => '#e9e9e9',
));

$wp_customize->add_control(new busicare_plus_Customize_Alpha_Color_Control($wp_customize, 'clt_bg_color', array(
            'label' => esc_html__('Client Section Background Color', 'busicare-plus'),
            'palette' => true,
            'active_callback' => 'busicare_plus_sponsors_callback',
            'section' => 'home_client_section')
));

if (class_exists('busicare_plus_Repeater')) {
    $wp_customize->add_setting(
            'busicare_clients_content', array(
            )
    );

    $wp_customize->add_control(
            new busicare_plus_Repeater(
                    $wp_customize, 'busicare_clients_content', array(
                'label' => esc_html__('Clients content', 'busicare-plus'),
                'section' => 'home_client_section',
                'add_field_label' => esc_html__('Add new client', 'busicare-plus'),
                'item_name' => esc_html__('Client', 'busicare-plus'),
                'customizer_repeater_image_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'active_callback' => 'busicare_plus_sponsors_callback'
                    )
            )
    );
}

$wp_customize->selective_refresh->add_partial('home_client_section_title', array(
    'selector' => '.sponsors .section-header h2',
    'settings' => 'home_client_section_title',
    'render_callback' => 'busicare_plus_home_client_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_client_section_discription', array(
    'selector' => '.sponsors .section-header p',
    'settings' => 'home_client_section_discription',
    'render_callback' => 'busicare_plus_home_client_section_discription_render_callback'
));

function busicare_plus_home_client_section_title_render_callback() {
    return get_theme_mod('home_client_section_title');
}

function busicare_plus_home_client_section_discription_render_callback() {
    return get_theme_mod('home_client_section_discription');
}

?>