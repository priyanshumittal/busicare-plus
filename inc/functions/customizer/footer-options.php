<?php
function busicare_plus_footer_customizer($wp_customize) {

// Only For Footer Widgets  
    class busicare_Footer_Widgets_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>

            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>

            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>

            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">

                <?php foreach ($this->choices as $value => $args) : ?>

                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />

                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) { ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                            <?php
                        }
                        ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], get_template_directory_uri(), get_stylesheet_directory_uri())); ?>" 
                        <?php
                        if (!empty($args['label'])) {
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        }
                        ?>
                             />
                    </label>

                <?php endforeach; ?>

            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();
                });
            </script>

            <?php
        }
  /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>

            <style type="text/css" id="hybrid-customize-radio-image-css">
                #customize-control-footer_widgets_section label {
                    display: inline-block;
                    max-width: 20% !important;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }


                #customize-control-footer_widgets_section .ui-buttonset
                {
                    text-align: left !important;
                }


            </style>
            <?php
        }

    }
//Only For Footer Bar
    class busicare_Footer_Bar_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>

            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>

            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>

            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">

                <?php foreach ($this->choices as $value => $args) : ?>

                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />

                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) { ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                            <?php
                        }
                        ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], get_template_directory_uri(), get_stylesheet_directory_uri())); ?>" 
                        <?php
                        if (!empty($args['label'])) {
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        }
                        ?>
                             />
                    </label>

                <?php endforeach; ?>

            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();

                    //This Script for Home
                    if (jQuery('#_customize-input-home_portfolio_design_layout').val() == 1)
                    {
                        jQuery('#customize-control-portfolio_nav_style').show();
                    } else
                    {
                        jQuery('#customize-control-portfolio_nav_style').hide();
                    }
                    wp.customize('home_portfolio_design_layout', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == 1)
                                    {
                                        jQuery('#customize-control-portfolio_nav_style').show();
                                    } else
                                    {
                                        jQuery('#customize-control-portfolio_nav_style').hide();
                                    }
                                }
                        );
                    }
                    );

                    //Home page news section
                    if (jQuery('#_customize-input-home_news_design_layout').val() == 2)
                    {
                        jQuery('#customize-control-busicare_homeblog_layout').hide();
                    } else
                    {
                        jQuery('#customize-control-busicare_homeblog_layout').show();
                    }
                    wp.customize('home_news_design_layout', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == 2)
                                    {
                                        jQuery('#customize-control-busicare_homeblog_layout').hide();
                                    } else
                                    {
                                        jQuery('#customize-control-busicare_homeblog_layout').show();
                                    }
                                }
                        );
                    }
                    );


                    if ((jQuery('#_customize-input-footer_bar_sec1').val() == "custom_text"))
                    {
                        jQuery('#customize-control-footer_copyright').show();
                    } else
                    {
                        jQuery('#customize-control-footer_copyright').hide();
                    }
                    if ((jQuery('#_customize-input-footer_bar_sec2').val() == "custom_text"))
                    {
                        jQuery('#customize-control-footer_copyright_2').show();
                    } else
                    {
                        jQuery('#customize-control-footer_copyright_2').hide();
                    }


                    wp.customize('footer_bar_sec1', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == "custom_text")
                                    {
                                        jQuery('#customize-control-footer_copyright').show();
                                    } else
                                    {
                                        jQuery('#customize-control-footer_copyright').hide();
                                    }
                                }
                        );
                    }
                    );
                    wp.customize('footer_bar_sec2', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == "custom_text")
                                    {
                                        jQuery('#customize-control-footer_copyright_2').show();
                                    } else
                                    {
                                        jQuery('#customize-control-footer_copyright_2').hide();
                                    }
                                }
                        );
                    }
                    );
                });
            </script>

            <?php
        }

        /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>

            <style type="text/css" id="hybrid-customize-radio-image-css">
                #customize-control-footer_widgets_section label {
                    display: inline-block;
                    max-width: 20% !important;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }


                #customize-control-footer_widgets_section .ui-buttonset
                {
                    text-align: left !important;
                }


            </style>
            <?php
        }

    }

// Footer Widgets Section
    $wp_customize->add_section(
            'busicare_fwidgets_setting_section',
            array(
                'title' => esc_html__('Footer Widgets', 'busicare-plus'),
                'panel' => 'busicare_general_settings',
                'priority' => 200,
            )
    );

    //Enable/Disable Footer Widgets
    $wp_customize->add_setting('ftr_widgets_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'ftr_widgets_enable',
                    array(
                'label' => esc_html__('Enable / Disable Footer Widgets', 'busicare-plus'),
                'section' => 'busicare_fwidgets_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));

    if (class_exists('busicare_Footer_Widgets_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'footer_widgets_section', array(
            'default' => '4',
                )
        );

        $wp_customize->add_control(
                new busicare_Footer_Widgets_Customize_Control_Radio_Image(
                        $wp_customize, 'footer_widgets_section', array(
                    'label' => esc_html__('Footer widget layout', 'busicare-plus'),
                    'priority' => 199,
                    'section' => 'busicare_fwidgets_setting_section',
                    'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
                    'choices' => array(
                        '1' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/1.png',
                        ),
                        '2' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/2.png',
                        ),
                        '3' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/3.png',
                        ),
                        '4' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/4.png',
                        ),
                        '5' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/3-3-6.png',
                        ),
                        '6' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/3-6-3.png',
                        ),
                        '7' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/6-3-3.png',
                        ),
                        '8' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/8-4.png',
                        ),
                        '9' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-widgets/4-8.png',
                        ),
                    ),
                        )
                )
        );
    }

    //Footer Background Image
    $wp_customize->add_setting('ftr_wgt_background_img', array(
        'sanitize_callback' => 'esc_url_raw',
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'ftr_wgt_background_img', array(
                'label' => esc_html__('Footer Widgets Background Image', 'busicare-plus'),
                'priority' => 200,
                'section' => 'busicare_fwidgets_setting_section',
                'settings' => 'ftr_wgt_background_img',
                'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
    )));


    //Footer Widget Repeat
    $wp_customize->add_setting('footer_widget_reapeat', array('default' => 'no-repeat'));
    $wp_customize->add_control('footer_widget_reapeat',
            array(
                'label' => esc_html__('Background Image Repeat', 'busicare-plus'),
                'priority' => 201,
                'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
                'section' => 'busicare_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'no-repeat' => esc_html__('No Repeat', 'busicare-plus'),
                    'repeat' => esc_html__('Repeat All', 'busicare-plus'),
                    'repeat-x' => esc_html__('Repeat Horizontally', 'busicare-plus'),
                    'repeat-y' => esc_html__('Repeat Vertically', 'busicare-plus'),
                )
    ));

    //Footer Widget position
    $wp_customize->add_setting('footer_widget_position', array('default' => 'left top'));
    $wp_customize->add_control('footer_widget_position',
            array(
                'label' => esc_html__('Background Image Position', 'busicare-plus'),
                'priority' => 201,
                'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
                'section' => 'busicare_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'left top' => esc_html__('Left Top', 'busicare-plus'),
                    'left center' => esc_html__('Left Center', 'busicare-plus'),
                    'left bottom' => esc_html__('left bottom', 'busicare-plus'),
                    'right top' => esc_html__('Right Top', 'busicare-plus'),
                    'right center' => esc_html__('Right Center', 'busicare-plus'),
                    'right bottom' => esc_html__('Right Bottom', 'busicare-plus'),
                    'center top' => esc_html__('Center Top', 'busicare-plus'),
                    'center center' => esc_html__('Center Center', 'busicare-plus'),
                    'center bottom' => esc_html__('Center Bottom', 'busicare-plus'),
                )
    ));

    //Footer Widget Size
    $wp_customize->add_setting('footer_widget_bg_size', array('default' => 'cover'));
    $wp_customize->add_control('footer_widget_bg_size',
            array(
                'label' => esc_html__('Background Size', 'busicare-plus'),
                'priority' => 201,
                'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
                'section' => 'busicare_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'cover' => esc_html__('Cover', 'busicare-plus'),
                    'contain' => esc_html__('Contain', 'busicare-plus'),
                    'auto' => esc_html__('Auto', 'busicare-plus'),
                )
    ));

    //Footer Widget Background Attachment
    $wp_customize->add_setting('footer_widget_bg_attachment', array('default' => 'scroll'));
    $wp_customize->add_control('footer_widget_bg_attachment',
            array(
                'label' => esc_html__('Background Attachment', 'busicare-plus'),
                'description' => esc_html__('Note: Background Image Repeat and Background Image Position will not work with Background Attachment Fixed property', 'busicare-plus'),
                'priority' => 201,
                'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
                'section' => 'busicare_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'scroll' => esc_html__('Scroll', 'busicare-plus'),
                    'fixed' => esc_html__('Fixed', 'busicare-plus'),
                )
    ));

    // Image overlay
    $wp_customize->add_setting('busicare_fwidgets_image_overlay', array(
        'default' => true,
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    ));

    $wp_customize->add_control('busicare_fwidgets_image_overlay', array(
        'label' => esc_html__('Enable Footer Widgets image overlay', 'busicare-plus'),
        'priority' => 201,
        'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
        'section' => 'busicare_fwidgets_setting_section',
        'type' => 'checkbox',
    ));


    //Testimonial Background Overlay Color
    $wp_customize->add_setting('busicare_fwidgets_overlay_section_color', array(
        'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
        'default' => 'rgba(0, 0, 0, 0.7)',
    ));

    $wp_customize->add_control(new busicare_plus_Customize_Alpha_Color_Control($wp_customize, 'busicare_fwidgets_overlay_section_color', array(
                'label' => esc_html__('Footer Widgets image overlay color', 'busicare-plus'),
                'priority' => 202,
                'active_callback' => 'busicare_plus_ftr_widgets_hide_show_callback',
                'palette' => true,
                'section' => 'busicare_fwidgets_setting_section')
    ));


    $wp_customize->add_section('footer_section',
            array(
                'title' => esc_html__('Footer Bar', 'busicare-plus'),
                'priority' => 210,
                'panel' => 'busicare_general_settings',
            )
    );
  
  /*     * *********************** Eanble Footer ******************************** */


    //Enable/Disable Foot bar
    $wp_customize->add_setting('ftr_bar_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'ftr_bar_enable',
                    array(
                'label' => esc_html__('Enable / Disable Footer Bar', 'busicare-plus'),
                'section' => 'footer_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));


    if (class_exists('busicare_Footer_Bar_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'advance_footer_bar_section', array(
            'default' => '1',
                )
        );

        $wp_customize->add_control(
                new busicare_Footer_Bar_Customize_Control_Radio_Image(
                        $wp_customize, 'advance_footer_bar_section', array(
                    'label' => esc_html__('Footer Bar layout', 'busicare-plus'),
                    'priority' => 2,
                    'active_callback' => 'busicare_plus_footer_callback',
                    'section' => 'footer_section',
                    'choices' => array(
                        '1' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-bar/footer-layout-1-76x48.png',
                        ),
                        '2' => array(
                            'url' => trailingslashit(BUSICAREP_PLUGIN_URL) . 'inc/images/footer-bar/footer-layout-2-76x48.png',
                        ),
                    ),
                        )
                )
        );
    }

    //Footer bar section 1
    $wp_customize->add_setting('footer_bar_sec1', array('default' => 'custom_text'));
    $wp_customize->add_control('footer_bar_sec1',
            array(
                'label' => esc_html__('Section 1', 'busicare-plus'),
                'priority' => 3,
                'active_callback' => 'busicare_plus_footer_callback',
                'section' => 'footer_section',
                'type' => 'select',
                'choices' => array(
                    'none' => esc_html__('None', 'busicare-plus'),
                    'footer_menu' => esc_html__('Footer Menu', 'busicare-plus'),
                    'custom_text' => esc_html__('Copyright Text', 'busicare-plus'),
                    'widget' => esc_html__('Widget', 'busicare-plus')
                )
    ));


    /*     * *********************** Copyright Section 1******************************** */
    $wp_customize->add_setting('footer_copyright',
            array(
                'default' => '<span class="copyright">' . __('Copyright 2020 <a href="#">Spicethemes</a> All right reserved', 'busicare-plus') . ' </span>',
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            )
    );

    $wp_customize->add_control('footer_copyright',
            array(
                'label' => esc_html__('Copyright Section 1', 'busicare-plus'),
                'section' => 'footer_section',
                'type' => 'textarea',
                'priority' => 4,
                'active_callback' => 'busicare_plus_footer_callback'
            )
    );

    //Footer bar section 2
    $wp_customize->add_setting('footer_bar_sec2', array('default' => 'none'));
    $wp_customize->add_control('footer_bar_sec2',
            array(
                'label' => esc_html__('Section 2', 'busicare-plus'),
                'priority' => 5,
//          'active_callback'=> 'busicare_plus_footer_callback',
                'active_callback' => function($control) {
                    return (
                            busicare_plus_footer_callback($control) &&
                            busicare_footer_column_callback($control)
                            );
                },
                'section' => 'footer_section',
                'type' => 'select',
                'choices' => array(
                    'none' => esc_html__('None', 'busicare-plus'),
                    'footer_menu' => esc_html__('Footer Menu', 'busicare-plus'),
                    'custom_text' => esc_html__('Copyright Text', 'busicare-plus'),
                    'widget' => esc_html__('Widget', 'busicare-plus')
                )
    ));

    /*     * *********************** Copyright Section 2******************************** */
    $wp_customize->add_setting('footer_copyright_2',
            array(
            'default' => '<span class="copyright">' . __('Copyright 2020 <a href="#">Spicethemes</a> All right reserved', 'busicare-plus') . ' </span>',
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            )
    );

    $wp_customize->add_control('footer_copyright_2',
            array(
                'label' => esc_html__('Copyright Section 2', 'busicare-plus'),
                'section' => 'footer_section',
                'type' => 'textarea',
                /*'active_callback' => function($control) {
                    return (
                            busicare_plus_footer_callback($control) &&
                            busicare_footer_column_callback($control)
                            );
                },*/
                'priority' => 6,
            )
    );

    //Footer Bar Border
    $wp_customize->add_setting('footer_bar_border',
            array(
                'default' => 0,
                'capability' => 'edit_theme_options',
            )
    );

    $wp_customize->add_control(new busicare_plus_Slider_Control($wp_customize, 'footer_bar_border',
                    array(
                'label' => esc_html__('Footer Bar Border', 'busicare-plus'),
                'active_callback' => 'busicare_plus_footer_callback',
                'section' => 'footer_section',
                'type' => 'slider',
                'min' => 0,
                'max' => 100,
                    )
    ));

    //Footer Bar Border Color
    $wp_customize->add_setting(
            'busicare_footer_border_clr', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'busicare_footer_border_clr',
                    array(
                'label' => esc_html__('Border Color', 'busicare-plus'),
                'section' => 'footer_section',
                'settings' => 'busicare_footer_border_clr',
                'active_callback' => 'busicare_plus_footer_callback',
    )));

    //Footer Bar border Style
    $wp_customize->add_setting('footer_border_style', array('default' => 'solid'));
    $wp_customize->add_control('footer_border_style',
            array(
                'label' => esc_html__('Border Style', 'busicare-plus'),
                'active_callback' => 'busicare_plus_footer_callback',
                'section' => 'footer_section',
                'type' => 'select',
                'choices' => array(
                    'solid' => esc_html__('Solid', 'busicare-plus'),
                    'dotted' => esc_html__('Dotted', 'busicare-plus'),
                    'dashed' => esc_html__('Dashed', 'busicare-plus'),
                    'double' => esc_html__('Double', 'busicare-plus'),
                    'groove' => esc_html__('Groove', 'busicare-plus'),
                    'ridge' => esc_html__('Ridge', 'busicare-plus'),
                    'inset' => esc_html__('Inset', 'busicare-plus'),
                    'outset' => esc_html__('Outset', 'busicare-plus')
                )
    ));

    $wp_customize->selective_refresh->add_partial('footer_copyright', array(
        'selector' => '.site-footer .site-info p',
        'settings' => 'footer_copyright',
        'render_callback' => 'busicare_plus_footer_copyright_render_callback',
            )
    );

    function busicare_plus_footer_copyright_render_callback() {
        return get_theme_mod('footer_copyright');
    }

}

add_action('customize_register', 'busicare_plus_footer_customizer');
