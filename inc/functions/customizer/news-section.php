<?php

//Latest News Section
$wp_customize->add_section('busicare_latest_news_section', array(
    'title' => esc_html__('Latest News settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 16,
));


// Enable news section
$wp_customize->add_setting('latest_news_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'latest_news_section_enable',
                array(
            'label' => esc_html__('Enable Home News section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'busicare_latest_news_section',
                )
));

$wp_customize->add_setting('home_news_design_layout', array('default' => 1));
$wp_customize->add_control('home_news_design_layout',
        array(
            'label' => esc_html__('Design Style', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'active_callback' => 'busicare_plus_news_callback',
            'type' => 'select',
            'choices' => array(
                1 => esc_html__('Grid Style', 'busicare-plus'),
                2 => esc_html__('List Style', 'busicare-plus'),
                3 => esc_html__('Masonry Style', 'busicare-plus'),
            )
));


/* * ****************** Blog Content ****************************** */
$wp_customize->add_setting('busicare_homeblog_layout',
        array(
            'default' => 3,
            'sanitize_callback' => 'busicare_plus_sanitize_select'
        )
);

$wp_customize->add_control('busicare_homeblog_layout',
        array(
            'label' => esc_html__('Column Layout', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'type' => 'select',
            'choices' => array(
                2 => esc_html__('2 Column', 'busicare-plus'),
                3 => esc_html__('3 Column', 'busicare-plus'),
                4 => esc_html__('4 Column', 'busicare-plus'),
            ),
            'active_callback' => 'busicare_plus_news_callback'
        )
);

$wp_customize->add_setting('busicare_homeblog_counts',
        array(
            'default' => 3,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_sanitize_number_range',
        )
);
$wp_customize->add_control('busicare_homeblog_counts',
        array(
            'label' => esc_html__('No of Post', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'type' => 'number',
            'input_attrs' => array('min' => 2, 'max' => 20, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'busicare_plus_news_callback'
        )
);

// News section title
$wp_customize->add_setting('home_news_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Our Latest News', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'busicare_latest_news_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_news_callback'
));

//News section subtitle
$wp_customize->add_setting('home_news_section_discription', array(
    'default' => esc_html__('From our blog', 'busicare-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_discription', array(
    'label' => esc_html__('Sub title', 'busicare-plus'),
    'section' => 'busicare_latest_news_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_news_callback'
));

// Read More Button
$wp_customize->add_setting('home_news_button_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Read More', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_button_title', array(
    'label' => esc_html__('Read More Text', 'busicare-plus'),
    'section' => 'busicare_latest_news_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_news_callback'
));

// enable / disable meta section 
$wp_customize->add_setting(
        'home_meta_section_settings',
        array('capability' => 'edit_theme_options',
            'default' => true,
));
$wp_customize->add_control(
        'home_meta_section_settings',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Enable post meta in blog section', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'active_callback' => 'busicare_plus_news_callback'
        )
);




$wp_customize->add_setting(
        'home_blog_more_btn',
        array(
            'default' => esc_html__('View More', 'busicare-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_blog_more_btn',
        array(
            'label' => esc_html__('View More Button Text', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'type' => 'text',
            'active_callback' => 'busicare_plus_news_callback'
));

$wp_customize->add_setting(
        'home_blog_more_btn_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_blog_more_btn_link',
        array(
            'label' => esc_html__('View More Button Link', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'type' => 'text',
            'active_callback' => 'busicare_plus_news_callback'
));

$wp_customize->add_setting(
        'home_blog_more_btn_link_target',
        array('sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
));

$wp_customize->add_control(
        'home_blog_more_btn_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in new tab', 'busicare-plus'),
            'section' => 'busicare_latest_news_section',
            'active_callback' => 'busicare_plus_news_callback'
        )
);

/**
 * Add selective refresh for Front page news section controls.
 */
$wp_customize->selective_refresh->add_partial('home_news_section_title', array(
    'selector' => '.home-blog .section-header h2',
    'settings' => 'home_news_section_title',
    'render_callback' => 'busicare_plus_home_news_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_section_discription', array(
    'selector' => '.home-blog .section-header h5',
    'settings' => 'home_news_section_discription',
    'render_callback' => 'busicare_plus_home_news_section_discription_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_blog_more_btn', array(
    'selector' => '.home-blog .business-view-more-post',
    'settings' => 'home_blog_more_btn',
    'render_callback' => 'busicare_plus_home_blog_more_btn_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_button_title', array(
    'selector' => '.home-blog a.more-link',
    'settings' => 'home_news_button_title',
    'render_callback' => 'busicare_plus_home_news_button_title_render_callback',
));


function busicare_plus_home_news_section_title_render_callback() {
    return get_theme_mod('home_news_section_title');
}

function busicare_plus_home_news_section_discription_render_callback() {
    return get_theme_mod('home_news_section_discription');
}

function busicare_plus_home_blog_more_btn_render_callback() {
    return get_theme_mod('home_blog_more_btn');
}

function busicare_plus_home_news_button_title_render_callback() {
    return get_theme_mod('home_news_button_title');
}

?>