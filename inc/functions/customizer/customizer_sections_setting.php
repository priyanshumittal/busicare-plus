<?php
function busicare_plus_sections_settings($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';
    /* Sections Settings */
    $wp_customize->add_panel('section_settings', array(
        'priority' => 126,
        'capability' => 'edit_theme_options',
        'title' => esc_html__('Homepage Section Settings', 'busicare-plus'),
    ));
}

add_action('customize_register', 'busicare_plus_sections_settings');

function busicare_plus_home_page_sanitize_text($input) {
    return wp_kses_post(force_balance_tags($input));
}

function busicare_plus_sanitize_radio($input, $setting) {

    //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
    $input = sanitize_key($input);

    //get the list of possible radio box options 
    $choices = $setting->manager->get_control($setting->id)->choices;

    //return input if valid or return default option
    return ( array_key_exists($input, $choices) ? $input : $setting->default );
}
function busicare_plus_sanitize_checkbox($checked) {
    // Boolean check.
    return ( ( isset($checked) && true == $checked ) ? true : false );
}
function busicare_plus_sanitize_select($input, $setting) {

        //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
        $input = sanitize_key($input);

        //get the list of possible radio box options 
        $choices = $setting->manager->get_control($setting->id)->choices;

        //return input if valid or return default option
        return ( array_key_exists($input, $choices) ? $input : $setting->default );
    }

if (!function_exists('busicare_sanitize_number_range')) :

        /**
         * Sanitize number range.
         *
         * @since 1.0.0
         *
         * @see absint() https://developer.wordpress.org/reference/functions/absint/
         *
         * @param int  $input Number to check within the numeric range defined by the setting.
         * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
         * @return int|string The number, if it is zero or greater and falls within the defined range; otherwise, the setting default.
         */
        function busicare_plus_sanitize_number_range($input, $setting) {

            // Ensure input is an absolute integer.
            $input = absint($input);

            // Get the input attributes associated with the setting.
            $atts = $setting->manager->get_control($setting->id)->input_attrs;

            // Get min.
            $min = ( isset($atts['min']) ? $atts['min'] : $input );

            // Get max.
            $max = ( isset($atts['max']) ? $atts['max'] : $input );

            // Get Step.
            $step = ( isset($atts['step']) ? $atts['step'] : 1 );

            // If the input is within the valid range, return it; otherwise, return the default.
            return ( $min <= $input && $input <= $max && is_int($input / $step) ? $input : $setting->default );
        }

    endif;
/* * *********************** Different Stick Header Logo ******************************** */

function busicare_plus_sticky_header_logo_callback($control) {
    if (true == $control->manager->get_setting('sticky_header_logo')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Services Callback function ******************************** */

function busicare_plus_service_callback($control) {
    if (true == $control->manager->get_setting('home_service_section_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}

function busicare_footer_column_callback($control) {
    if ($control->manager->get_setting('advance_footer_bar_section')->value() == '1') {
        return false;
    }
    return true;
}

/* * *********************** Portfolio Callback function ******************************** */

function busicare_plus_portfolio_callback($control) {
    if (true == $control->manager->get_setting('portfolio_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}


/* * *********************** Funfact Callback function ******************************** */

function busicare_plus_funfact_callback($control) {
    if (true == $control->manager->get_setting('funfact_section_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}



/* * *********************** Team Callback function ******************************** */

function busicare_plus_team_callback($control) {
    if (true == $control->manager->get_setting('team_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}



/* * *********************** Sponsors Callback function ******************************** */

function busicare_plus_sponsors_callback($control) {
    if (true == $control->manager->get_setting('client_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}


/* * *********************** Related Post Callback function ******************************** */

function busicare_plus_rt_post_callback($control) {
    if (true == $control->manager->get_setting('busicare_enable_related_post')->value()) {
        return true;
    } else {
        return false;
    }
}

/************************* Search icon enable or disbale function *********************************/

    function search_icon_hide_show_callback ( $control ) 
    {
        if( true == $control->manager->get_setting ('search_btn_enable')->value()){
            return true;
        }
        else {
            return false;
        }       
    }
/* * *********************** WooProduct Callback function ******************************** */

function busicare_plus_wooproduct_callback($control) {
    if (true == $control->manager->get_setting('shop_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Testimonial Callback function ******************************** */

function busicare_plus_testimonial_callback($control) {
    if (true == $control->manager->get_setting('testimonial_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Slider Callback function ******************************** */

function busicare_plus_slider_callback($control) {
    if (true == $control->manager->get_setting('home_page_slider_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Latest News Callback function ******************************** */

function busicare_plus_news_callback($control) {
    if (true == $control->manager->get_setting('latest_news_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Footer Callback function ******************************** */

function busicare_plus_footer_callback($control) {
    if (true == $control->manager->get_setting('ftr_bar_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Footer WIdgets enable or disbale function ******************************** */

function busicare_plus_ftr_widgets_hide_show_callback($control) {
    if (true == $control->manager->get_setting('ftr_widgets_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** CTA2 Callback function ******************************** */

function busicare_plus_cta2_callback($control) {
    if (true == $control->manager->get_setting('cta2_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** CTA1 Callback function ******************************** */

function busicare_plus_cta1_callback($control) {
    if (true == $control->manager->get_setting('cta1_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
