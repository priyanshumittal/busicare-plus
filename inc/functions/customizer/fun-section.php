<?php

/* funfact section */
$wp_customize->add_setting('funfact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'busicare_sanitize_checkbox'
));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'funfact_section_enabled',
                array(
            'label' => esc_html__('Enable Funfact on homepage', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'funfact_section',
                )
));

$wp_customize->add_section('funfact_section', array(
    'title' => esc_html__('Funfact settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 15,
));

$wp_customize->add_setting('home_fun_section_title', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'default' => __('<span>Doing the right thing,</span><br> at the right time.', 'busicare-plus'),
));

$wp_customize->add_control('home_fun_section_title', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'funfact_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_funfact_callback'
));

if (class_exists('busicare_plus_Repeater')) {
    $wp_customize->add_setting('busicare_funfact_content', array());

    $wp_customize->add_control(new busicare_plus_Repeater($wp_customize, 'busicare_funfact_content', array(
                'label' => esc_html__('Funfact content', 'busicare-plus'),
                'section' => 'funfact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Funfact', 'busicare-plus'),
                'item_name' => esc_html__('Funfact', 'busicare-plus'),
                'customizer_repeater_icon_control' => false,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'active_callback' => 'busicare_plus_funfact_callback'
    )));
}

//FunFact Background Image
$wp_customize->add_setting('funfact_callout_background', array(
    'default' => BUSICAREP_PLUGIN_URL .'inc/images/bg/funfact-bg.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'funfact_callout_background', array(
            'label' => esc_html__('Background Image', 'busicare-plus'),
            'section' => 'funfact_section',
            'settings' => 'funfact_callout_background',
            'active_callback' => 'busicare_plus_funfact_callback'
        )));

// Image overlay
$wp_customize->add_setting('funfact_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
));

$wp_customize->add_control('funfact_image_overlay', array(
    'label' => esc_html__('Enable Funfact image overlay', 'busicare-plus'),
    'section' => 'funfact_section',
    'type' => 'checkbox',
    'active_callback' => 'busicare_plus_funfact_callback'
));

//Funfact Background Overlay Color
$wp_customize->add_setting('funfact_overlay_section_color', array(
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'default' => 'rgba(0, 11, 24, 0.8)',
));

$wp_customize->add_control(new busicare_plus_Customize_Alpha_Color_Control($wp_customize, 'funfact_overlay_section_color', array(
            'label' => esc_html__('Funfact image overlay color', 'busicare-plus'),
            'palette' => true,
            'active_callback' => 'busicare_plus_funfact_callback',
            'section' => 'funfact_section')
));

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('home_fun_section_title', array(
    'selector' => '.funfact .title',
    'settings' => 'home_fun_section_title',
    'render_callback' => 'home_fun_section_title_render_callback'
));

function busicare_plus_home_fun_section_title_render_callback() {
    return get_theme_mod('home_fun_section_title');
}

?>