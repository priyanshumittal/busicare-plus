<?php

// busicare slider content data
if (!function_exists('busicare_plus_slider_default_customize_register')) :
    add_action('customize_register', 'busicare_plus_slider_default_customize_register');

    function busicare_plus_slider_default_customize_register($wp_customize) {

        //busicare lite slider data
        if (get_theme_mod('home_slider_subtitle') != '' || get_theme_mod('home_slider_title') != '' || get_theme_mod('home_slider_discription') != '' || get_theme_mod('home_slider_image') != '') {

            $home_slider_subtitle = get_theme_mod('home_slider_subtitle');
            $home_slider_title = get_theme_mod('home_slider_title');
            $home_slider_discription = get_theme_mod('home_slider_discription');
            $home_slider_btn_target = get_theme_mod('home_slider_btn_target');
            $home_slider_btn_txt = get_theme_mod('home_slider_btn_txt');
            $home_slider_btn_link = get_theme_mod('home_slider_btn_link');
            $home_slider_btn2_target = get_theme_mod('home_slider_btn_target2');
            $home_slider_btn2_txt = get_theme_mod('home_slider_btn_txt2');
            $home_slider_btn2_link = get_theme_mod('home_slider_btn_link2');
            $home_slider_image = get_theme_mod('home_slider_image');


            if ($home_slider_btn_target == 1) {

                $home_slider_btn_target = true;
            }

            $busicare_slider_content_control = $wp_customize->get_setting('busicare_slider_content');
            if (!empty($busicare_slider_content_control)) {
                $busicare_slider_content_control->default = json_encode(array(
                    array(
                        'subtitle' => !empty($home_slider_subtitle) ? $home_slider_subtitle : 'Awesome Theme For All Your Needs',
                        'title' => !empty($home_slider_title) ? $home_slider_title : 'We Provide Quality Business <br> Solution',
                        'text' => !empty($home_slider_discription) ? $home_slider_discription : 'Welcome to BusiCare',
                        'abtsliderbutton_text' => !empty($home_slider_btn_txt) ? $home_slider_btn_txt :__('Learn More', 'busicare-plus'),
                        'abtsliderlink' => !empty($home_slider_btn_link) ? $home_slider_btn_link : '#',
                        'image_url' => !empty($home_slider_image) ? $home_slider_image : BUSICAREP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => !empty($home_slider_btn_target) ? $home_slider_btn_target : false,
                        'abtbutton_text' => !empty($home_slider_btn2_txt) ? $home_slider_btn2_txt : __('About Us', 'busicare-plus'),
                        'abtlink' => !empty($home_slider_btn2_link) ? $home_slider_btn2_link : '#',
                        'abtopen_new_tab' => !empty($home_slider_btn2_target) ? $home_slider_btn2_target : false,
                        'id' => 'customizer_repeater_56d7ea7f40b50',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                    ),
                        ));
            }
        } else {
            //busicare slider data
            $busicare_slider_content_control = $wp_customize->get_setting('busicare_slider_content');
            if (!empty($busicare_slider_content_control)) {
                $busicare_slider_content_control->default = json_encode(array(
                    array(
                        'subtitle' => __('Awesome Theme For All Your Needs', 'busicare-plus'),
                        'title' => __('We provide solutions to <br> grow your business', 'busicare-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'busicare-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'busicare-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'busicare-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',                        
                        'id' => 'customizer_repeater_56d7ea7f40b96',
                    ),
                    array(
                        'subtitle' => __('Awesome Theme For All Your Needs', 'busicare-plus'),
                        'title' => __('We create stunning <br>WordPress themes', 'busicare-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'busicare-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'busicare-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/slider/slide-2.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'busicare-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b97',
                    ),
                    array(
                        'subtitle' => __('Awesome Theme For All Your Needs', 'busicare-plus'),
                        'title' =>__('We provide solutions to <br> grow your business', 'busicare-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'busicare-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'busicare-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/slider/slide-3.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'busicare-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b98',
                    ),
                        ));
            }
        }
    }

endif;


// busicare default service data
if (!function_exists('busicare_plus_service_default_customize_register')) :

    function busicare_plus_service_default_customize_register($wp_customize) {

        $busicare_service_content_control = $wp_customize->get_setting('busicare_service_content');
        if (!empty($busicare_service_content_control)) {
            $busicare_service_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Unlimited Support', 'busicare-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-mobile',
                    'title' => esc_html__('Pixel Perfect Design', 'busicare-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa-cogs',
                    'title' => esc_html__('Powerful Options', 'busicare-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                    ));
        }
    }

    add_action('customize_register', 'busicare_plus_service_default_customize_register');

endif;


// busicare default Funfact data
if (!function_exists('busicare_plus_funfact_default_customize_register')) :

    function busicare_plus_funfact_default_customize_register($wp_customize) {

        $busicare_funfact_content_control = $wp_customize->get_setting('busicare_funfact_content');
        if (!empty($busicare_funfact_content_control)) {
            $busicare_funfact_content_control->default = json_encode(array(
                array(
                    'title' => '4050',
                    'text' => esc_html('Satisfied Clients', 'busicare-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'title' => '150',
                    'text' => esc_html('Finish Projects', 'busicare-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'title' => '90%',
                    'text' => esc_html('Business Growth', 'busicare-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'title' => '27',
                    'text' => esc_html('Consultants', 'busicare-plus'),
                    'id' => 'customizer_repeater_56d7ea7f58b86',
                ),
                    ));
        }
    }

    add_action('customize_register', 'busicare_plus_funfact_default_customize_register');

endif;

// busicare default Contact data
if (!function_exists('busicare_plus_contact_default_customize_register')) :

    function busicare_plus_contact_default_customize_register($wp_customize) {

        $busicare_contact_content_control = $wp_customize->get_setting('busicare_contact_content');
        if (!empty($busicare_contact_content_control)) {
            $busicare_contact_content_control->default = json_encode(array(
                        array(
                            'icon_value' => 'fa-solid fa-envelope',
                            'title'=>'Email Your Problem',
                            'text' => __('info@busicare.com <br/>support@busicare.com', 'busicare-plus'),
                            'id' => 'customizer_repeater_56d7ea7f40b60',
                        ),
                        array(
                            'icon_value' => 'fa fa-phone',
                            'title'=>'Call Us Any Time',
                            'text' => __('+(15) 718-999-3939, <br/>+(15) 781-254-8437', 'busicare-plus'),
                            'id' => 'customizer_repeater_56d7ea7f40b61',
                        ),
                        array(
                            
                            'icon_value' => 'fa-solid fa-location-dot',
                            'title'=>'Visit Our Office',
                            'text' => __('514 S. Magnolia St. Orlando, <br/>FL 32806', 'busicare-plus'),
                            'id' => 'customizer_repeater_56d7ea7f40b62',
                        ),
                    ));
        }
    }

    add_action('customize_register', 'busicare_plus_contact_default_customize_register');

endif;

// busicare default Contact data
if (!function_exists('busicare_plus_social_links_default_customize_register')) :

    function busicare_plus_social_links_default_customize_register($wp_customize) {

        $busicare_social_links_control = $wp_customize->get_setting('busicare_social_links');
        if (!empty($busicare_social_links_control)) {
            $busicare_social_links_control->default = json_encode(array(
        array(
                        'icon_value' => 'fa-brands fa-facebook-f',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b76',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-x-twitter',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b77',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-linkedin',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b78',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-instagram',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
    ));
        }
    }

    add_action('customize_register', 'busicare_plus_social_links_default_customize_register');

endif;


// busicare default Ribon data
if (!function_exists('busicare_plus_ribon_default_customize_register')) :

    function busicare_plus_ribon_default_customize_register($wp_customize) {

        $busicare_ribon_content_control = $wp_customize->get_setting('busicare_ribon_content');
        if (!empty($busicare_ribon_content_control)) {
            $busicare_ribon_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-brands fa-facebook-f',
                    'title' => esc_html__('Facebook', 'busicare-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                    'icon_value' => 'fa-brands fa-x-twitter',
                    'title' => esc_html__('Twitter', 'busicare-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                    'icon_value' => 'fa-brands fa-linkedin',
                    'title' => esc_html__('LinkedIn', 'busicare-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                    'icon_value' => 'fa-brands fa-instagram',
                    'title' => esc_html__('Instagram', 'busicare-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b80',
                ),
                    ));
        }
    }

    add_action('customize_register', 'busicare_plus_ribon_default_customize_register');

endif;


// busicare Testimonial content data
if (!function_exists('busicare_plus_testimonial_default_customize_register')) :
    add_action('customize_register', 'busicare_plus_testimonial_default_customize_register');

    function busicare_plus_testimonial_default_customize_register($wp_customize) {

               //busicare lite slider data
        if (get_theme_mod('home_testimonial_title') != '' || get_theme_mod('home_testimonial_desc') != '' || get_theme_mod('home_testimonial_name') != '' || get_theme_mod('home_testimonial_thumb') != '') {

            $home_testimonial_title = get_theme_mod('home_testimonial_title');
            $home_testimonial_discription = get_theme_mod('home_testimonial_desc');
            $home_testimonial_client_name = get_theme_mod('home_testimonial_name');
            $home_testimonial_designation = get_theme_mod('home_testimonial_designation');
            $home_testimonial_link = get_theme_mod('home_testimonial_link');
            $home_testimonial_image = get_theme_mod('home_testimonial_thumb');

            $busicare_testimonial_content_control = $wp_customize->get_setting('busicare_testimonial_content');
            if (!empty($busicare_testimonial_content_control)) {
            $busicare_testimonial_content_control->default = json_encode(array(
                array(
                    'title' => !empty($home_testimonial_title) ? $home_testimonial_title : 'Exellent Theme & Very Fast Support',
                    'text' => !empty($home_testimonial_discription) ? $home_testimonial_discription :'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => !empty($home_testimonial_client_name) ? $home_testimonial_client_name : esc_html__('Amanda Smith', 'busicare-plus'),
                    'designation' => !empty($home_testimonial_designation) ? $home_testimonial_designation : esc_html__('Developer', 'busicare-plus'),
                    'link' => !empty($home_testimonial_link) ? $home_testimonial_link : '#',
                    'image_url' =>  !empty($home_testimonial_image) ? $home_testimonial_image : BUSICAREP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                ));
            }
        } else {//busicare default testimonial data.
        $busicare_testimonial_content_control = $wp_customize->get_setting('busicare_testimonial_content');
        if (!empty($busicare_testimonial_content_control)) {
            $busicare_testimonial_content_control->default = json_encode(array(
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => esc_html__('Amanda Smith', 'busicare-plus'),
                    'designation' => esc_html__('Developer', 'busicare-plus'),
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => esc_html__('Travis Cullan', 'busicare-plus'),
                    'designation' => esc_html__('Team Leader', 'busicare-plus'),
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b97',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => esc_html__('Victoria Wills', 'busicare-plus'),
                    'designation' => esc_html__('Volunteer', 'busicare-plus'),
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                    'id' => 'customizer_repeater_56d7ea7f40b98',
                    'open_new_tab' => 'no',
                ),
                    ));
        }
    }
    }

endif;


// busicare Team content data
if (!function_exists('busicare_plus_team_default_customize_register')) :
    add_action('customize_register', 'busicare_plus_team_default_customize_register');

    function busicare_plus_team_default_customize_register($wp_customize) {
        //busicare default team data.
        $busicare_team_content_control = $wp_customize->get_setting('busicare_team_content');
        if (!empty($busicare_team_content_control)) {
            $busicare_team_content_control->default = json_encode(array(
                array(
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team1.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'busicare-plus'),
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb908674e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9148530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9150e1e89',
                                    'link' => 'linkedin.com',
                                    'icon' =>'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team2.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'busicare-plus'),
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9155a1072',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160ab683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916ddffc9',
                                    'link' => 'linkedin.com',
                                    'icon' =>'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916ddffc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team3.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'busicare-plus'),
                    'text' => 'Pok pok direct trade godard street art, poutine fam typewriter food truck narwhal kombucha wolf cardigan butcher whatever pickled you.',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' =>'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/team/team4.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'busicare-plus'),
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' =>'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                    ));
        }
    }

endif;

//Client section
if (!function_exists('busicare_plus_client_default_customize_register')) :
    add_action('customize_register', 'busicare_plus_client_default_customize_register');

    function busicare_plus_client_default_customize_register($wp_customize) {
        //busicare default client data.
        $busicare_client_content_control = $wp_customize->get_setting('busicare_clients_content');
        if (!empty($busicare_client_content_control)) {
            $busicare_client_content_control->default = json_encode(array(
                array(
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/sponsors/logo1.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                array(
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/sponsors/logo2.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b97',
                ),
                array(
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/sponsors/logo3.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b98',
                ),
                array(
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/sponsors/logo4.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b99',
                ),
                array(
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/sponsors/logo5.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b100',
                ),
                array(
                    'link' => '#',
                    'image_url' => BUSICAREP_PLUGIN_URL . '/inc/images/sponsors/logo1.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                    ));
        }
    }


endif;