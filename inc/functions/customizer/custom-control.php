<?php

if (!function_exists('busicare_plus_register_custom_controls')) :

    /**
     * Register Custom Controls
     */
    function busicare_plus_register_custom_controls($wp_customize) {
        require_once BUSICAREP_PLUGIN_DIR . '/inc/inc/customizer/repeater/class-repeater-setting.php';
        require_once BUSICAREP_PLUGIN_DIR . '/inc/inc/customizer/repeater/class-control-repeater.php';
        require_once BUSICAREP_PLUGIN_DIR . '/inc/inc/customizer/slider/class-slider-control.php';
        require_once BUSICAREP_PLUGIN_DIR . '/inc/inc/customizer/slider/class-opacity-control.php';
         $wp_customize->register_control_type('busicare_plus_Slider_Control');
         
    }

endif;
add_action('customize_register', 'busicare_plus_register_custom_controls');
