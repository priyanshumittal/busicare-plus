<?php

//Shop Section
$wp_customize->add_section('busicare_shop_section', array(
    'title' => esc_html__('Home Shop settings', 'busicare-plus'),
    'panel' => 'section_settings',
    'priority' => 18,
));

$wp_customize->add_setting('shop_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'busicare_plus_sanitize_checkbox'
    ));

$wp_customize->add_control(new busicare_Toggle_Control($wp_customize, 'shop_section_enable',
                array(
            'label' => esc_html__('Enable Home Shop section', 'busicare-plus'),
            'type' => 'toggle',
            'section' => 'busicare_shop_section',
                )
));

// Shop section title
$wp_customize->add_setting('home_shop_section_title', array(
    'default' => esc_html__('Featured Products', 'busicare-plus'),
    'sanitize_callback' => 'busicare_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_shop_section_title', array(
    'label' => esc_html__('Sub title', 'busicare-plus'),
    'section' => 'busicare_shop_section',
    'type' => 'text',
    'active_callback' => 'busicare_plus_wooproduct_callback'
));

//Shop section discription
$wp_customize->add_setting('home_shop_section_discription', array(
    'default' => esc_html__('Our amazing products', 'busicare-plus'),
));
$wp_customize->add_control('home_shop_section_discription', array(
    'label' => esc_html__('Title', 'busicare-plus'),
    'section' => 'busicare_shop_section',
    'type' => 'textarea',
    'active_callback' => 'busicare_plus_wooproduct_callback'
));

//Navigation Type
$wp_customize->add_setting('shop_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('shop_nav_style', array(
    'label' => esc_html__('Navigation Style', 'busicare-plus'),
    'section' => 'busicare_shop_section',
    'type' => 'radio',
    'priority' => 12,
    'choices' => array(
        'bullets' => esc_html__('Bullets', 'busicare-plus'),
        'navigation' => esc_html__('Navigation', 'busicare-plus'),
        'both' => esc_html__('Both', 'busicare-plus'),
    ),
    'active_callback' => 'busicare_plus_wooproduct_callback'
));

// animation speed
$wp_customize->add_setting('shop_animation_speed', array('default' => 3000));
$wp_customize->add_control('shop_animation_speed',
        array(
            'label' => esc_html__('Animation speed', 'busicare-plus'),
            'section' => 'busicare_shop_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'busicare_plus_wooproduct_callback'
));

// smooth speed
$wp_customize->add_setting('shop_smooth_speed', array('default' => 1000));
$wp_customize->add_control('shop_smooth_speed',
        array(
            'label' => esc_html__('Smooth speed', 'busicare-plus'),
            'section' => 'busicare_shop_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'busicare_plus_wooproduct_callback'
));

$wp_customize->selective_refresh->add_partial('home_shop_section_title', array(
    'selector' => '.shop .section-header h2',
    'settings' => 'home_shop_section_title',
    'render_callback' => 'busicare_plus_home_shop_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_shop_section_discription', array(
    'selector' => '.shop .section-subtitle',
    'settings' => 'home_shop_section_discription',
    'render_callback' => 'busicare_plus_home_shop_section_discription_render_callback',
));

function busicare_plus_home_shop_section_title_render_callback() {
    return get_theme_mod('home_shop_section_title');
}

function busicare_plus_home_shop_section_discription_render_callback() {
    return get_theme_mod('home_shop_section_discription');
}
?>