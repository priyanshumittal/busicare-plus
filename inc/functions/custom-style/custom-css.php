<?php

// define function for custom color setting
function busicare_plus_custom_light() {

    $link_color = get_theme_mod('link_color');
    list($r, $g, $b) = sscanf($link_color, "#%02x%02x%02x");
    $r = $r - 50;
    $g = $g - 25;
    $b = $b - 40;

    if ($link_color != '#ff0000') :
        ?>
        <style type="text/css">
            .title span {
                color: <?php echo $link_color; ?>;
            }

            a:hover {color: <?php echo $link_color; ?>;}
            .entry-meta a:hover {color: <?php echo $link_color; ?>;}

            dl dd a, dl dd a:hover, dl dd a:focus, ul li a:focus { color: <?php echo $link_color; ?>; }

            button,
            input[type="button"],
            input[type="submit"] {
                background-color: <?php echo $link_color; ?>;
            }

            .btn-default {
                background: <?php echo $link_color; ?>;
                border: 1px solid <?php echo $link_color; ?>; 
            }

            .btn-light:hover, .btn-light:focus {
                background: <?php echo $link_color; ?>; 
                border: 1px solid <?php echo $link_color; ?>; 
            }

            .btn-default-dark { background: <?php echo $link_color; ?>; }

            .btn-border {
                border: 2px solid <?php echo $link_color; ?>;
            }


            .btn-border:hover, .btn-border:focus {
                border: 2px solid <?php echo $link_color; ?>;
            }


            a:focus { 
                color: <?php echo $link_color; ?>;       
            }

            .custom-social-icons li > a:focus {
                background-color: <?php echo $link_color; ?>;
            }

            .custom-social-icons li > a {
                color: <?php echo $link_color; ?>;
            }

            .contact .custom-social-icons li > a:hover, .contact .custom-social-icons li > a:focus {
                background-color: <?php echo $link_color; ?>;
            }

            .custom-social-icons li > a:hover, 
            .custom-social-icons li > a:focus {
                background-color: <?php echo $link_color; ?>;
            }

            #searchbar_fullscreen .btn {
                background-color: <?php echo $link_color; ?>;
            }
            
            .screen-reader-text:focus {
                color: <?php echo $link_color; ?>;
            }

            .cart-header {
                background-color: <?php echo $link_color; ?>;
            }

            .cart-header > a .cart-total {
                background: <?php echo $link_color; ?>;
            }

            <?php
            if (!is_rtl()) {
                ?> 
                .slider-caption .heading {
                    border-left: 4px solid <?php echo $link_color; ?>;
                }
            <?php } else {
                ?>
                .slider-caption .heading {
                    border-right: 4px solid <?php echo $link_color; ?>;
                }
                <?php
            }
            ?>

            .slider-caption .heading {
                border-left: 4px solid <?php echo $link_color; ?>;
            }

            .pointer-scroll {
                background: <?php echo $link_color; ?>;
            }

            .owl-carousel .owl-prev:hover, 
            .owl-carousel .owl-prev:focus { 
                background-color: <?php echo $link_color; ?>;
            }

            .owl-carousel .owl-next:hover, 
            .owl-carousel .owl-next:focus { 
                background-color: <?php echo $link_color; ?>;
            }

            .cta_content, .cta_main {background-color: <?php echo $link_color; ?>; }

            .cta_content .btn-light {
                color: <?php echo $link_color; ?>;
            }

            .cta_content a:after {
                color: <?php echo $link_color; ?>;
            }

            .text-default { color: <?php echo $link_color; ?>; }

            .entry-header .entry-title a:focus {
                color: <?php echo $link_color; ?> !important; 
            }

            .services .post:hover .post-thumbnail i.fa {
                border: 1px solid <?php echo $link_color; ?>;
                background-color: <?php echo $link_color; ?>;
            }

            .filter-tabs .nav-item.active{
                background-color: <?php echo $link_color; ?>;
            }

            .filter-tabs li:focus, .filter-tabs li:hover{background-color: <?php echo $link_color; ?>;}

            .filter-tabs .nav-link:focus, .filter-tabs .nav-link:hover {
                background-color: <?php echo $link_color; ?>;
            }

            .funfact-icon .fa{
                color: <?php echo $link_color; ?>;
            }

            .page-breadcrumb li a:hover{
                color:<?php echo $link_color; ?>;
            }

            .blog .post .entry-date a {
                background-color: <?php echo $link_color; ?>;
            }

            .entry-meta a :hover{
                color:<?php echo $link_color; ?>;
            }

            .sidebar .widget .widget-title{
                background-color:<?php echo $link_color; ?>; 
            }

            .sidebar .input-group .form-control {
                border-color: <?php echo $link_color; ?> ;
            }

            .navbar .nav .nav-item a:focus{
                color: <?php echo $link_color; ?>;
            }

            .sidebar .widget_categories li::before , 
            .sidebar .widget_archive li::before ,
            .sidebar .widget_recent_entries li::before ,
            .sidebar .widget_meta li::before,
            .sidebar .widget_recent_comments li::before 
            {
                color: <?php echo $link_color; ?>;
            }
            
            .sidebar .widget_categories li a:hover , 
            .sidebar .widget_archive li a:hover ,
            .sidebar .widget_recent_entries li a:hover ,
            .sidebar .widget_meta li a:hover,
            .sidebar .widget_recent_comments li a:hover,
            .sidebar .widget li a:focus,
            .dropdown-item a:focus{
                color: <?php echo $link_color; ?>;
            }

            .testmonial-block .avatar img {
                box-shadow: <?php echo $link_color; ?> 0px 0px 0px 1px; 
            }

            .products .onsale {
                background: <?php echo $link_color; ?>;
                border: 2px solid <?php echo $link_color; ?>;
            }
            
            /*product page & product detail*/
            .added_to_cart.wc-forward:hover, .woocommerce .posted_in a:hover{
                color: <?php echo $link_color; ?>;
            }

            /*.woocommerce-loop-product__title a{color:<?php echo $link_color; ?>;}*/

            .woocommerce-loop-product__title:hover {color:<?php echo $link_color; ?>;}
            
            .woocommerce ul.product_list_widget li a:hover{
                        color: <?php echo $link_color; ?>;
            }
            
            .site-footer .woocommerce ul.product_list_widget li a{
                        color: <?php echo $link_color; ?>;
            }
            
            .sidebar .woocommerce ul.product_list_widget .star-rating{
                color: <?php echo $link_color; ?>;
            }

            .add-to-cart a {
                background: <?php echo $link_color; ?>;
            }

            .team-grid .position { 
                color: <?php echo $link_color; ?>; 
            }

            .team-grid .custom-social-icons li > a:hover {
                background-color: <?php echo $link_color; ?>;
            }

            .title_seprater {
                background-color: <?php echo $link_color; ?>;
            }

            .section-header .section-title-two span
            {
                color: <?php echo $link_color; ?>;
            }

            .footer-sidebar li a:hover {color:<?php echo $link_color; ?>;}

            .footer-sidebar address a:focus {color:<?php echo $link_color; ?>;}

            .footer-sidebar .widget .widget-title {
                color:<?php echo $link_color; ?>;
            }

            .site-info{
                background-color: <?php echo $link_color; ?>;
            }

            .footer-sidebar .widget li:hover::before{color:<?php echo $link_color; ?>}

            .blog .pagination a.active {
                background-color: <?php echo $link_color; ?>;
            }

            .blog .pagination a:hover:not(.active),
            .blog .pagination a:focus:not(.active) {background-color:<?php echo $link_color; ?>;}

            .portfolio .pagination a.active {
                background-color: <?php echo $link_color; ?>;
            }

            .portfolio .pagination a:hover:not(.active),
            .portfolio .pagination a:focus:not(.active) {background-color:<?php echo $link_color; ?>;}

            .portfolio .post:hover .click-view{
                background-color: <?php echo $link_color; ?>;
            }

            .contact .title h4{
                background-color: <?php echo $link_color; ?>;
            }

            .sidebar .contact .widget-cont-title {
                background-color: <?php echo $link_color; ?>;
            }
            input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="number"]:focus, input[type="tel"]:focus, input[type="range"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="week"]:focus, input[type="time"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="color"]:focus, textarea:focus {
                border-color: <?php echo $link_color; ?>;
            }

            .cont address > i.fa {
                color: <?php echo $link_color; ?>;
            }

            .cont a:hover, .cont a:focus {
                color: <?php echo $link_color; ?>;
            }

            .error-page .title {
                color: <?php echo $link_color; ?>;
            }

            .error-page h4 {
                color: <?php echo $link_color; ?>;
            }

            .error-page .not-found-btn a {
                background-color: <?php echo $link_color; ?>;
            }

            .scroll-up a {
                background: <?php echo $link_color; ?>;
            }

            .scroll-up a:hover,
            .scroll-up a:active {
                background:<?php echo $link_color; ?>;
            }
            .dark .scroll-up a:hover,
            .dark .scroll-up a:active {
                background:<?php echo $link_color; ?>;
                color:#ffffff;
            }
            /*SmartMenu Bootstrap*/
            .navbar .nav .nav-item:hover .nav-link, 
            .navbar .nav .nav-item.active .nav-link {
                color: <?php echo $link_color; ?>;
            }

            /*slider dot*/
            .owl-theme .owl-dots .owl-dot.active span {
                background-color: <?php echo $link_color; ?>;
            }


            /*WooCommerce Style*/
            .woocommerce-loop-product__title a{color:<?php echo $link_color; ?>;}
            .woocommerce ul.products li.product .onsale, .products span.onsale
            {
                background: <?php echo $link_color; ?>;
                border: 2px solid <?php echo $link_color; ?>;
            }
            .woocommerce ul.products li.product .onsale, .woocommerce span.onsale{
                background: <?php echo $link_color; ?> !important;
            }

            .woocommerce ul.products li.product .button, .owl-item .item .cart .add_to_cart_button {
                background: <?php echo $link_color; ?>;
            }

            .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current {
                background: <?php echo $link_color; ?> !important;
            }

            .woocommerce div.product form.cart .button, .woocommerce a.button, .woocommerce a.button:hover, .woocommerce a.button, .woocommerce .woocommerce-Button, .woocommerce .cart input.button, .woocommerce input.button.alt, .woocommerce button.button, .woocommerce #respond input#submit, .woocommerce .cart input.button:hover, .woocommerce .cart input.button:focus, .woocommerce input.button.alt:hover, .woocommerce input.button.alt:focus, .woocommerce input.button:hover, .woocommerce input.button:focus,  .woocommerce button.button:focus, .woocommerce #respond input#submit:focus, .woocommerce-cart .wc-proceed-to-checkout a.checkout-button {
                background: <?php echo $link_color; ?>;
            }
            .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce input.button.alt:hover {
                background: <?php echo $link_color; ?>;
            }
            .woocommerce-message, .woocommerce-info {
                border-top-color: <?php echo $link_color; ?>;
            }
            .woocommerce-message::before, .woocommerce-info::before {
                color: <?php echo $link_color; ?>;
            }
            .woocommerce div.product .stock {
                color: <?php echo $link_color; ?>;
            }
            .woocommerce p.stars a {
                color: <?php echo $link_color; ?>;
            }
            .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
                background: <?php echo $link_color; ?>;
            }
            /*Woocommerce style*/

            /*Contact SideBar*/
            .sidebar_contact .contact_search .widget-cont-title {
                background-color: <?php echo $link_color; ?>;
            }
            /*Contact SideBar*/

            .filter-tabs .nav-item.show .nav-link, .filter-tabs .nav-link.active, .filter-tabs .nav-link:hover {
                background-color: <?php echo $link_color; ?>;
            }

            /*Menu Search Effect*/
            .search-form input[type="submit"] {
                background: <?php echo $link_color; ?> repeat scroll 0 0;
                border: 1px solid <?php echo $link_color; ?>;
            }

            .navbar .search-box-outer .dropdown-menu,.header-sticky.five .search-box-outer .dropdown-menu{
                border-top: solid 1px <?php echo $link_color; ?>;
            }

            .btn-primary {
                background-color: <?php echo $link_color; ?>;
                border-color: <?php echo $link_color; ?>;
            }

            .btn-primary:hover {
                background-color: <?php echo $link_color; ?>;
                border-color: <?php echo $link_color; ?>;
            }

            .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show>.btn-primary.dropdown-toggle,.btn-primary:focus {
                background-color: <?php echo $link_color; ?>;
                border-color: <?php echo $link_color; ?>;
            }

            .pagination .nav-links .page-numbers.current {
                background-color: <?php echo $link_color; ?>;
            }

            .sidebar .widget_meta li a:hover{
                color: <?php echo $link_color; ?>;
            }

            .dropdown-item a:focus{
                color: <?php echo $link_color; ?>;
            }

            /*menu*/
            .dropdown-item.active, .dropdown-item:active,.dropdown-item:hover {
                background-color: transparent;
                color:<?php echo $link_color; ?>;
            }

            body .product-price h5 > a{
                color:<?php echo $link_color; ?>;
            }
            
            .related-posts .entry-title a:hover{
                color: <?php echo $link_color; ?>;
            }
            
            .section-space.services .entry-header .entry-title a:hover{
                        color: <?php echo $link_color; ?>;
            }
            
            .sidebar .widget_product_search .search-field , .site-footer .widget_product_search .search-field{
                border-color: <?php echo $link_color; ?>;
            }
            
            .sidebar .widget_nav_menu li a:hover ,
            .sidebar .widget_pages li a:hover ,
            .sidebar .widget_product_categories li a:hover ,
            .sidebar .widget_links li a:hover {
                color: <?php echo $link_color; ?>;
            }
            
            .sidebar .widget_nav_menu li::before ,
            .sidebar .widget_pages li::before ,
            .sidebar .widget_product_categories li::before ,
            .sidebar .widget_links li::before {
                color: <?php echo $link_color; ?>;
            }
            .sidebar .widget .custom-social-icons li > a:hover {background-color: <?php echo $link_color; ?>;}
            .sidebar .widget .head-contact-info a:hover {color: <?php echo $link_color; ?>;}
            
            .sidebar .widget .tagcloud a:hover, .site-footer .widget .tagcloud a:hover{
                background-color: <?php echo $link_color; ?>;
            }
            

            .widget_nav_menu li a:hover , 
            .widget_pages li a:hover ,
            .widget_product_categories li a:hover ,
            .widget_links li a:hover ,
            .widget_categories li a:hover , 
            .widget_archive li a:hover ,
            .widget_recent_entries li a:hover ,
            .widget_meta li a:hover,
            .widget_recent_comments li a:hover,
            .widget li a:focus,
            .dropdown-item a:focus,
            .navbar .nav .nav-item a:focus
            {
                color: <?php echo $link_color; ?>;
            }
            .widget_nav_menu li::before , 
            .widget_pages li::before ,
            .widget_product_categories li::before ,
            .widget_links li::before ,
            .widget_categories li::before , 
            .widget_archive li::before ,
            .widget_recent_entries li::before ,
            .widget_meta li::before,
            .widget_recent_comments li::before 
            {
                color: <?php echo $link_color; ?>;
            }

            .widget .input-group .form-control ,.woocommerce-product-search .search-field {
                border-color: <?php echo $link_color; ?>;
            }
            
            .woocommerce .widget_price_filter .ui-slider .ui-slider-range {background-color: <?php echo $link_color; ?>;}
            .woocommerce .widget_price_filter .ui-slider .ui-slider-handle {background-color: <?php echo $link_color; ?>;}

            blockquote:not(.testmonial-block){
                border-left: 4px solid <?php echo $link_color; ?> !important;
            }
            .blog .post .entry-content a{
                color: <?php echo $link_color; ?>;
            }
             .team4 .team-grid .list-inline.list-unstyled li > a:hover {
                color: <?php echo $link_color; ?>;
            }
            .services2 .post::before {
                 background-color: <?php echo $link_color; ?>;
            }
            .services2 .post-thumbnail i.fa {
                color: <?php echo $link_color; ?>;
            }
            .services3 .post-thumbnail i.fa, .services3 .post-thumbnail img { 
              background: <?php echo $link_color; ?>;
             box-shadow: <?php echo $link_color; ?> 0px 0px 0px 1px;
            }
            .services3 .post:hover .post-thumbnail i.fa, .services3 .post:hover .post-thumbnail img { 
                color: <?php echo $link_color; ?>;
            }
            .services4 .post-thumbnail i.fa, .services4 .post-thumbnail img { 
                color: <?php echo $link_color; ?>;
            }
            #testimonial-carousel2 .testmonial-block:before, .page-template-template-testimonial-5 .testmonial-block:before {
                border-top: 25px solid <?php echo $link_color; ?>;
            }
             .navbar .nav .nav-item:hover .nav-link, 
             .navbar .nav .nav-item.active .nav-link, 
             .dropdown-menu > li.active > a,.navbar .nav .nav-item.current_page_parent .nav-link {
                color: <?php echo $link_color; ?>;
            }
            .bg-light {color: <?php echo $link_color; ?> !important;}
            .navbar-nav a.bg-light:focus, 
            .navbar-nav a.bg-light:hover, 
            .navbar-nav button.bg-light:focus, 
            .navbar-nav button.bg-light:hover {color: <?php echo $link_color; ?> !important;}
            .contact-detail-area i {color:<?php echo $link_color; ?>;}
            .blog.page blockquote:before{
                color: <?php echo esc_html($link_color);?>26;
                border-left: 4px solid <?php echo esc_html($link_color); ?>;
            }
            .home-blog h3 a:hover{color:<?php echo esc_html($link_color); ?>;}

            /*Dark CSS*/

            .btn-default { background: <?php echo esc_html($link_color); ?>; color: #ffffff;border: 1px solid <?php echo esc_html($link_color); ?>; }

            .btn-light:hover, .btn-light:focus { background: <?php echo esc_html($link_color); ?>; color: #ffffff;border: 1px solid <?php echo esc_html($link_color); ?>; }

            .btn-default-dark { background: <?php echo esc_html($link_color); ?>; color: #ffffff; }

            .btn-border { background: #ffffff; color: #1c314c; border: 2px solid <?php echo esc_html($link_color); ?>; }
            .btn-border:hover, .btn-border:focus { color: #ffffff; border: 2px solid <?php echo esc_html($link_color); ?>;}
            .custom-social-icons li > a:focus {background-color: <?php echo esc_html($link_color); ?>;color:#fff;}
            .custom-social-icons li > a {color: <?php echo esc_html($link_color); ?>;}
            .contact .custom-social-icons li > a:hover, .contact .custom-social-icons li > a:focus {
                background-color: <?php echo esc_html($link_color); ?>;
            }
            .cart-header {background-color: <?php echo esc_html($link_color); ?>;}
            .slider-caption .heading { 
                border-left: 4px solid <?php echo esc_html($link_color); ?>;
            }
            .pointer-scroll {
                background: <?php echo esc_html($link_color); ?>;}
            .owl-carousel .owl-prev:focus { 
                background-color: <?php echo esc_html($link_color); ?>;
                color: #fff;
            }
            .owl-carousel .owl-next:focus { 
                background-color: <?php echo esc_html($link_color); ?>;
                color: #fff;
            }
            .cta_content, .cta_main {background-color: <?php echo esc_html($link_color); ?>; }
            .cta_content .btn-light {padding: 0.688rem 1.5rem;background-color: #fff;color: <?php echo esc_html($link_color); ?>;border:unset;}
            .cta_content a:after {content: "\f054";padding: 0.25rem 0.5rem;color: <?php echo esc_html($link_color); ?>;font-family: fontawesome;font-size: 0.688rem;font-weight: 400;} 
            .text-default { color: <?php echo esc_html($link_color); ?>; }
            .entry-header .entry-title a:hover, 
            .entry-header .entry-title a:focus { 
              color: <?php echo esc_html($link_color); ?> !important; 
            }
            .services .post:hover .post-thumbnail i.fa {
                border: 1px solid <?php echo esc_html($link_color); ?>;
                background-color: <?php echo esc_html($link_color); ?>;
                color:#fff;
            }
            .filter-tabs .nav-item.active{
              background-color: <?php echo esc_html($link_color); ?>;
            }

            .filter-tabs li:focus, .filter-tabs li:hover{background-color: <?php echo esc_html($link_color); ?>;}
            .filter-tabs .nav-link:focus, .filter-tabs .nav-link:hover {
                border-color: none;
                background-color: <?php echo esc_html($link_color); ?>;
            }
            .funfact-icon .fa{
                color: <?php echo esc_html($link_color); ?>;
            }
            .page-breadcrumb li a:hover{
              color:<?php echo esc_html($link_color); ?>
            }
            .entry-meta a :hover{color:<?php echo esc_html($link_color); ?>;}
            .blog .post .entry-date a { background-color: <?php echo esc_html($link_color); ?>;}
            .sidebar .widget .widget-title
            {
              background-color:<?php echo esc_html($link_color); ?>;}
            .sidebar .input-group .form-control {
              border-color: <?php echo esc_html($link_color); ?>;
            }
            .sidebar .widget_categories li::before , 
            .sidebar .widget_archive li::before ,
            .sidebar .widget_recent_entries li::before ,
            .sidebar .widget_meta li::before,
            .sidebar .widget_recent_comments li::before 
            {
              color: <?php echo esc_html($link_color); ?>;
              
            }
            .testmonial-block .avatar img { border: 5px solid #fff; box-shadow: <?php echo esc_html($link_color); ?> 0px 0px 0px 1px; margin: 0.125rem; }
            .team-grid .position { color: <?php echo esc_html($link_color); ?>;}
            .team-grid .custom-social-icons li > a:hover {
                 background-color: <?php echo esc_html($link_color); ?>;
               }
            .section-separator {
             background-color: <?php echo esc_html($link_color); ?>;
            } 
            .section-header .section-title-two span
            {
              color: <?php echo esc_html($link_color); ?>;
            }
            .footer-sidebar li a:hover{color:<?php echo esc_html($link_color); ?>;}
            .footer-sidebar address a:focus {color:<?php echo esc_html($link_color); ?>;}
            .footer-sidebar .widget .widget-title {
                color:<?php echo esc_html($link_color); ?>;
            }
            .site-info{background-color: <?php echo esc_html($link_color); ?>;}
            .footer-sidebar .widget li:hover::before{color:<?php echo esc_html($link_color); ?>}
            .blog .pagination a.active {
              background-color: <?php echo esc_html($link_color); ?>;
            }
            .blog .pagination a:focus:not(.active) {background-color:<?php echo esc_html($link_color); ?>;}
            .portfolio .pagination a.active {
              background-color: <?php echo esc_html($link_color); ?>;}
            .portfolio .pagination a:focus:not(.active) {background-color:<?php echo esc_html($link_color); ?>;}
            .contact .title h4{
                background-color: <?php echo esc_html($link_color); ?>;
              }
            .sidebar .contact .widget-cont-title {
              background-color: <?php echo esc_html($link_color); ?>;
              }
            input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="number"]:focus, input[type="tel"]:focus, input[type="range"]:focus, input[type="date"]:focus, input[type="month"]:focus, input[type="week"]:focus, input[type="time"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="color"]:focus, textarea:focus {
                border-color: <?php echo esc_html($link_color); ?>;
            }
            .cont .fa-map-marker ,
            .cont .fa-phone ,
            .cont .fa-envelope-o ,
            .cont .fa-whatsapp {
              color: <?php echo esc_html($link_color); ?>;
            }
            .cont a:hover, .cont a:focus {
              color: <?php echo esc_html($link_color); ?>;
            }
            .error-page .title {
                color: <?php echo esc_html($link_color); ?>;}
            .error-page h4 {
              color: <?php echo esc_html($link_color); ?>;}
            .scroll-up a {background: <?php echo esc_html($link_color); ?>;}
            .scroll-up a:active {
              background:<?php echo esc_html($link_color); ?>;}
            .search-form input[type="submit"] {
                background: <?php echo esc_html($link_color); ?> repeat scroll 0 0;
                border: 1px solid <?php echo esc_html($link_color); ?>;
            }
            .navbar .search-box-outer .dropdown-menu {
                border-top: solid 1px <?php echo esc_html($link_color); ?>;
            }
            .btn-primary {
                color: #fff;
                background-color: <?php echo esc_html($link_color); ?>;
                border-color: <?php echo esc_html($link_color); ?>;
            }
            .btn-primary:hover {
                color: #fff;
                background-color: <?php echo esc_html($link_color); ?>;
                border-color: <?php echo esc_html($link_color); ?>;
            }
            .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .show>.btn-primary.dropdown-toggle,.btn-primary:focus {
                color: #fff;
                background-color: <?php echo esc_html($link_color); ?>;
                border-color: <?php echo esc_html($link_color); ?>;
            }
            .services2 .post::before {
            background-color: <?php echo esc_html($link_color); ?>;}
            .services2 .post-thumbnail i.fa { 
                color: <?php echo esc_html($link_color); ?>;}
            .services3 .post-thumbnail i.fa { 
              background: <?php echo esc_html($link_color); ?>;
             box-shadow: <?php echo esc_html($link_color); ?> 0px 0px 0px 1px;}
            .services3 .post:hover .post-thumbnail i.fa { 
              color: <?php echo esc_html($link_color); ?>; }
            .services4 .post-thumbnail i.fa { 
                color: <?php echo esc_html($link_color); ?>;}
            .team2 .team-grid .card-body .list-inline li > a:hover {
                color: <?php echo esc_html($link_color); ?>;
                background-color: unset;
            }
             .team3 .team-grid .card-body .list-inline li > a:hover {
                color: <?php echo esc_html($link_color); ?>;
                background-color: unset;
            }
            .team4 .horizontal-nav .owl-prev, .team4 .horizontal-nav .owl-next {
              border: 1px solid <?php echo esc_html($link_color); ?>;
              color: <?php echo esc_html($link_color); ?>;
            }
             .team4 .team-grid .list-inline li > a:hover {
                color: <?php echo esc_html($link_color); ?>;
                background-color: unset;
            }
            #testimonial-carousel2 .testmonial-block {
                border-left: 4px solid <?php echo esc_html($link_color); ?>;
            }
            #testimonial-carousel2 .testmonial-block:before {border-top: 25px solid <?php echo esc_html($link_color); ?>;}
            .navbar.navbar4 .navbar-nav > li.active > a:after, .navbar.navbar4 ul li > a:hover:after {
                background: <?php echo esc_html($link_color); ?>;}
            .navbar5.navbar .nav .nav-item:hover .nav-link, .navbar5.navbar .nav .nav-item.active .nav-link {
                color: #ffffff;
                background: <?php echo esc_html($link_color); ?>;
            }
            .navbar6 .header-lt::before {
               background-color: <?php echo esc_html($link_color); ?>;}
            .dark .services .post .post-thumbnail i.fa {color: <?php echo esc_html($link_color); ?>; border: 2px solid <?php echo esc_html($link_color); ?>;}
            .dark .entry-content p .more-link {color: <?php echo esc_html($link_color); ?>;}
            .dark .owl-theme .owl-dots .owl-dot.active span {
                background-color: <?php echo esc_html($link_color); ?>;}
            .dark .busicare_header_btn { background-color: <?php echo esc_html($link_color); ?>;}
            .serv .post i.fa {
                color: <?php echo esc_html($link_color); ?>;
            }
            .dark .navbar .nav .nav-item:hover .nav-link, .navbar .nav .nav-item.active .nav-link {
                color: <?php echo esc_html($link_color); ?>;
            }
            .dark .dropdown-item.active, .dropdown-item:active, .dropdown-item:hover {
                background-color: transparent;
                color: <?php echo esc_html($link_color); ?>;
            }
            .dark a:hover {
                color: <?php echo esc_html($link_color); ?>;
            }

            .dark .blog .post .entry-content a.more-link:hover{
                    color: <?php echo esc_html($link_color); ?>;
                }
            .hp-preloader-cube .hp-cube:before {background: <?php echo esc_html($link_color); ?>;}
#preloader2 .square{background: <?php echo esc_html($link_color); ?>;}
            .navbar .nav .html a:hover{
                color: <?php echo esc_html($link_color); ?>;
            }
            .navbar6 .header-lt::after {
                background-color:<?php echo esc_html($link_color); ?>;
            }
            .navbar.navbar4 .navbar-nav > li.active > a:after, .navbar.navbar4 ul li > a:hover:after {
    background: <?php echo esc_html($link_color); ?>;
    }
    @media (max-width: 991px) {
  .navbar6 .header-lt {
    background-color: <?php echo esc_html($link_color); ?>;
    }
  }
  
.busicare_header_btn {background-color: <?php echo esc_html($link_color); ?>;}
body .sidebar .wp-block-search__button {
    background-color: <?php echo esc_html($link_color); ?>;
}
.portfolio .pagination a:hover{
    color:#FFF;
}
    .sidebar .widget.widget_block .wp-block-search__label,
    .sidebar .widget.widget_block .wc-block-product-search__label,
    .footer-sidebar .widget .wp-block-search .wp-block-search__button,
    .sidebar .widget.widget_block h1,
    .sidebar .widget.widget_block h2,
    .sidebar .widget.widget_block h3,
    .sidebar .widget.widget_block h4,
    .sidebar .widget.widget_block h5,
    .sidebar .widget.widget_block h6 {
        background-color: <?php echo esc_html($link_color); ?>;
    }
    .sidebar .widget li a:hover {
        color: <?php echo esc_html($link_color); ?>;
    }
    .sidebar .widget li:before,.sidebar .widget li a:hover,.sidebar .widget li a:focus, .footer-sidebar .widget.widget_block li:before{
        color:<?php echo esc_html($link_color); ?>;
    }
    .sidebar .widget.woocommerce.widget_shopping_cart li a:hover,
    .sidebar .widget.woocommerce.widget_top_rated_products li a:hover,
    .sidebar .woocommerce ul.product_list_widget li a:hover{
    color: <?php echo esc_html($link_color); ?>;
     }
    .footer-sidebar .widget.widget_block .wp-block-search__label,
    .footer-sidebar .widget.widget_block h1,
    .footer-sidebar .widget.widget_block h2,
    .footer-sidebar .widget.widget_block h3,
    .footer-sidebar .widget.widget_block h4,
    .footer-sidebar .widget.widget_block h5,
    .footer-sidebar .widget.widget_block h6{
        color: <?php echo esc_html($link_color); ?>;
    }
.dark nav.rank-math-breadcrumb a:hover { color: <?php echo esc_html($link_color); ?>; }
 nav.rank-math-breadcrumb a:hover { color: <?php echo esc_html($link_color); ?>; }
.navxt-breadcrumb.text-center span a:hover {
    color: <?php echo esc_html($link_color); ?>;
}
        </style>
        <?php
    endif;
}
?>
