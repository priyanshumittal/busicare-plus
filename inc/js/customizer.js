/**
 * Main customize js file
 *
 */

/* global initializeAllElements */
(function ($) {

    //Slider title
    wp.customize(
            'home_slider_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.caption-content .title').text(newval);
                        }
                );
            }
    );

    //Slider description
    wp.customize(
            'home_slider_discription', function (value) {
                value.bind(
                        function (newval) {
                            $('.caption-content .subtitle').text(newval);
                        }
                );
            }
    );

    //Slider button
    wp.customize(
            'home_slider_btn_txt', function (value) {
                value.bind(
                        function (newval) {
                            $('.main-slider .btn-small').text(newval);
                        }
                );
            }
    );


    // Service Title
    wp.customize(
            'home_service_section_title', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.services .section-title').hide();
                                $('.services .title_seprater').hide();
                            } else
                            {
                                $('.services .section-title').show();
                                $('.services .title_seprater').show();
                            }
                            $('.services .section-title').text(newval);
                        }
                );
            }
    );

    // Service Description
    wp.customize(
            'home_service_section_discription', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.services .section-subtitle').hide();
                            } else
                            {
                                $('.services .section-subtitle').show();
                            }
                            $('.services .section-subtitle').text(newval);
                        }
                );
            }
    );

    // Funfact section
    wp.customize(
            'home_fun_section_title', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.section-space.funfact .funfact-text h2').hide();
                                $('.section-space.funfact .funfact-icon').hide();
                            } else
                            {
                                $('.section-space.funfact .funfact-text h2').show();
                                $('.section-space.funfact .funfact-icon').show();
                            }
                            $('.section-space.funfact .funfact-text h2').text(newval);
                        }
                );
            }
    );

    // project or portfolio Heading
    wp.customize(
            'home_portfolio_section_title', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.home_project_title').hide();
                                $('.home-portfolio .title_seprater').hide();
                            } else
                            {
                                $('.home_project_title').show();
                                $('.home-portfolio .title_seprater').show();
                            }
                            $('.home-portfolio .home_project_title').text(newval);
                        }
                );
            }
    );

    // project or portfolio Description
    wp.customize(
            'home_portfolio_section_subtitle', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.home_project_subtitle').hide();
                            } else
                            {
                                $('.home_project_subtitle').show();
                            }
                            $('.home-portfolio .home_project_subtitle').text(newval);
                        }
                );
            }
    );

    //portfolio category
    wp.customize(
            'porfolio_category_page_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.portfoliocat h2').text(newval);
                        }
                );
            }
    );

    wp.customize(
            'porfolio_category_page_desc', function (value) {
                value.bind(
                        function (newval) {
                            $('.portfoliocat h5').text(newval);
                        }
                );
            }
    );

    //portfolio column page
    wp.customize(
            'porfolio_page_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.portfolio-template-col h2').text(newval);
                        }
                );
            }
    );

    wp.customize(
            'porfolio_page_subtitle', function (value) {
                value.bind(
                        function (newval) {
                            $('.portfolio-template-col h5').text(newval);
                        }
                );
            }
    );

    // CTA1 Heading
    wp.customize(
            'home_cta1_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.cta_main .cta_content h1').text(newval);
                        }
                );
            }
    );

    wp.customize(
            'home_cta1_btn_text', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.cta_main .btn-light').hide();
                            } else
                            {
                                $('.cta_main .btn-light').show();
                            }
                            $('.cta_main .cta_content a').text(newval);
                        }
                );
            }
    );
    
    wp.customize(
            'home_cta1_btn_link', function (value) {
                value.bind(
                        function (newval) {
                            $(".cta_main a.btn-small.btn-light.cta_btn").attr("href", newval);
                        }
                );
            }
    );

    // CTA2 Heading
    wp.customize(
            'home_cta2_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.cta-2 .cta-block h2').text(newval);
                        }
                );
            }
    );


    wp.customize(
            'home_cta2_desc', function (value) {
                value.bind(
                        function (newval) {
                            $('.cta-2 .cta-block p').text(newval);
                        }
                );
            }
    );

    wp.customize(
            'home_cta2_btn1_text', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.cta-2 .btn-default').hide();
                            } else
                            {
                                $('.cta-2 .btn-default').show();
                            }
                            $('.cta-2 .btn-default').text(newval);
                        }
                );
            }
    );
    
    wp.customize(
            'home_cta2_btn1_link', function (value) {
                value.bind(
                        function (newval) {
                            $(".cta-2 a.btn-small.btn-default").attr("href", newval);
                        }
                );
            }
    );
    
    wp.customize(
            'home_cta2_btn2_text', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.cta-2 .btn-light').hide();
                            } else
                            {
                                $('.cta-2 .btn-light').show();
                            }
                            $('.cta-2 .btn-light').text(newval);
                        }
                );
            }
    );
    
    wp.customize(
            'home_cta2_btn2_link', function (value) {
                value.bind(
                        function (newval) {
                            $(".cta-2 a.btn-small.btn-light").attr("href", newval);
                        }
                );
            }
    );
    //CTA2 END


    // Testimonial Heading
    wp.customize(
            'home_testimonial_section_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.testimonial-wrapper .section-subtitle').text(newval);
                        }
                );
            }
    );

    // Testimonial Description
    wp.customize(
            'home_testimonial_section_discription', function (value) {
                value.bind(
                        function (newval) {
                            $('.testimonial-wrapper .section-title').text(newval);
                        }
                );
            }
    );


    // Feature Title
    wp.customize(
            'home_feature_section_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.features .features-content h1').text(newval);
                        }
                );
            }
    );


    // Team Heading
    wp.customize(
            'home_team_section_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.team-members .section-subtitle, .team2 .section-title, .team3 .section-title, .team4 .section-title').text(newval);
                        }
                );
            }
    );

    // Team Description
    wp.customize(
            'home_team_section_discription', function (value) {
                value.bind(
                        function (newval) {
                            $('.team-members .section-title, .team2 .section-subtitle, .team3 .section-subtitle, .team4 .section-subtitle').text(newval);
                        }
                );
            }
    );

    // Home Shop Section Title
    wp.customize(
            'home_shop_section_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.shop .section-header h2').text(newval);
                        }
                );
            }
    );

    // Home Shop Section Title
    wp.customize(
            'home_shop_section_discription', function (value) {
                value.bind(
                        function (newval) {
                            $('.shop .section-subtitle').text(newval);
                        }
                );
            }
    );


    // Latest News Heading
    wp.customize(
            'home_news_section_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.home-blog .section-header h2').text(newval);
                        }
                );
            }
    );

    // Latest News Description
    wp.customize(
            'home_news_section_discription', function (value) {
                value.bind(
                        function (newval) {
                            $('.home-blog .section-header h5').text(newval);
                        }
                );
            }
    );

    // Latest News button
    wp.customize(
            'home_news_button_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.home-blog a.more-link').text(newval);
                        }
                );
            }
    );

    // Latest News button
    wp.customize(
            'home_blog_more_btn', function (value) {
                value.bind(
                        function (newval) {
                            if (newval == '')
                            {
                                $('.index_extend_class').hide();
                            } else
                            {
                                $('.index_extend_class').show();
                            }
                            //	$( '.homeblog .post .more-link' ).text( newval );
                            $('.business-view-more-post').text(newval);
                        }
                );
            }
    );
    
//    blog read more link
    wp.customize(
            'home_blog_more_btn_link', function (value) {
                value.bind(
                        function (newval) {
                            $(".home-blog a.business-view-more-post").attr("href", newval);
                        }
                );
            }
    );

    // Client & partner Heading
    wp.customize(
            'home_client_section_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.sponsors .section-header h2').text(newval);
                        }
                );
            }
    );

    // Client & partner Description
    wp.customize(
            'home_client_section_discription', function (value) {
                value.bind(
                        function (newval) {
                            $('.sponsors .section-header p').text(newval);
                        }
                );
            }
    );

    // Contact Form Heading One
    wp.customize(
            'contact_cf7_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.section-space-page.contact .contant-form .title h4').text(newval);
                        }
                );
            }
    );

    // Contact Form Heading Two
    wp.customize(
            'contact_info_title', function (value) {
                value.bind(
                        function (newval) {
                            $('.section-space-page.contact .sidebar_contact h4').text(newval);
                        }
                );
            }
    );

    // Footer Copyright
    wp.customize(
            'footer_copyright', function (value) {
                value.bind(
                        function (newval) {
                            $('.site-footer .site-info p').text(newval);
                        }
                );
            }
    );
})(jQuery);