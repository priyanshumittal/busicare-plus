// OWL SLIDER CUSTOM JS
(function ($) {

  /* Preloader */
    jQuery(window).on('load', function() {
        setTimeout(function(){
            jQuery('body').addClass('loaded');
       }, 1500);

    });
    /* ---------------------------------------------- /*
     * Home section height
     /* ---------------------------------------------- */
         $(".search-icon").click(function(e){
            e.preventDefault();
            //console.log('wad');
            $('.header-logo .search-box-outer ul.dropdown-menu').toggle('addSerchBox');
         });

    function buildHomeSection(homeSection) {
        if (homeSection.length > 0) {
            if (homeSection.hasClass('home-full-height')) {
                homeSection.height($(window).height());
            } else {
                homeSection.height($(window).height() * 0.85);
            }
        }
    }
$("a").keypress(function() {
     this.blur();
     this.hideFocus = false;
     this.style.outline = null;
      });
      $("a").mousedown(function() {
           this.blur();
           this.hideFocus = true;
           this.style.outline = 'none';
      });

    /* ---------------------------------------------- /*
     * Scroll top
     /* ---------------------------------------------- */

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-up').fadeIn();
        } else {
            $('.scroll-up').fadeOut();
        }
    });

    $('a[href="#totop"]').click(function () {
        $('html, body').animate({scrollTop: 0}, 'slow');
        return false;
    });


    // Tooltip Js
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    // Accodian Js
    function toggleIcon(e) {
        $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus-square-o fa-minus-square-o');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);

    $("#blog-masonry").mpmansory(
            {
                childrenClass: 'item', // default is a div
                columnClasses: 'padding', //add classes to items
                breakpoints: {
                    xl: 6, //Change masonry column here like 2, 3, 4 column
                    lg: 6,
                    md: 6,
                    sm: 12,
                    xs: 12


                },
                distributeBy: {order: false, height: false, attr: 'data-order', attrOrder: 'asc'}, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
                onload: function (items) {
                    //make somthing with items
                }
            }
    );
    $("#blog-masonry2").mpmansory(
            {
                childrenClass: 'item', // default is a div
                columnClasses: 'padding', //add classes to items
                breakpoints: {
                    xl: 6, //Change masonry column here like 2, 3, 4 column
                    lg: 6,
                    md: 6,
                    sm: 6,
                    xs: 12


                },
                distributeBy: {order: false, height: false, attr: 'data-order', attrOrder: 'asc'}, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
                onload: function (items) {
                    //make somthing with items
                }
            }
    );
    $("#blog-masonry3").mpmansory(
            {
                childrenClass: 'item', // default is a div
                columnClasses: 'padding', //add classes to items
                breakpoints: {
                    xl: 4, //Change masonry column here like 2, 3, 4 column
                    lg: 4,
                    md: 4,
                    sm: 4,
                    xs: 12

                },
                distributeBy: {order: false, height: false, attr: 'data-order', attrOrder: 'asc'}, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
                onload: function (items) {
                    //make somthing with items
                }
            }
    );
    
    $("#blog-masonry4").mpmansory(
                {
                    childrenClass: 'item', // default is a div
                    columnClasses: 'padding', //add classes to items
                    breakpoints:{                   
                    xl: 3,   //Change masonry column here like 2, 3, 4 column
                    lg: 3,
                    md: 3,
                    sm: 3,
                    xs: 12              
                        
                    },
                    distributeBy: { order: false, height: false, attr: 'data-order', attrOrder: 'asc' }, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
                    onload: function (items) {
                        //make somthing with items
                    } 
                }
            );


    $(function () {
        $('a[href="#searchbar_fullscreen"]').on("click", function (event) {
            event.preventDefault();
            $("#searchbar_fullscreen").addClass("open");
            $('#searchbar_fullscreen input[type="search"]').focus();
        });
        $("#searchbar_fullscreen, #searchbar_fullscreen button.close").on("click keyup", function (event) {
            if (
                    event.target == this ||
                    event.target.className == "close" ||
                    event.keyCode == 27
                    ) {
                $(this).removeClass("open");
            }
        });
    });
})(jQuery);