<?php
/**
 * Getting started template
 */
$customizer_url = admin_url() . 'customize.php' ;
?>

<div id="imp_articles" class="busicare-plus-tab-pane active" style="display: none;">
	<div class="container-fluid">			
			<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
				<h3 class="busicare-plus-info-title text-center">Important Articles</h3>
			<div class="col-md-12" style="padding-left: 50px;">
			<h4> <a target="_blank" href="https://helpdoc.spicethemes.com/busicare/how-to-install-busicare-theme/"> <?php echo esc_html__('1. How to install the Plus Plugin','busicare-plus'); ?></a></h4><br>

			<h4><a target="_blank" href="https://helpdoc.spicethemes.com/busicare-plus/how-to-import-the-demo-data/"><?php echo esc_html__('2. How to import Demo Data','busicare-plus'); ?>  </a></h4><br>


			<h4><a target="_blank" href="https://helpdoc.spicethemes.com/busicare-plus/how-to-customize-busicare-plus/"><?php echo esc_html__('3. How to customize the theme','busicare-plus'); ?></a></h4><br>

			<h4><a target="_blank" href="https://helpdoc.spicethemes.com/busicare-plus/how-to-customize-front-page-sections-in-busicare-plus/"><?php echo esc_html__('4. How to customize the front page sections','busicare-plus'); ?></a></h4><br>
			</div>

			<a style="margin-left: 50px;" href="https://helpdoc.spicethemes.com/category/busicare-plus/" target="_blank" class="button button-primary button-hero"><?php echo esc_html__('More Help Articles','busicare-plus'); ?></a>
		</div>
		
	</div>
</div>	