<?php
/**
 * Getting started template
 */
?>
<div id="getting_started" class="busicare-plus-tab-pane active">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="busicare-plus-info-title text-center"><?php echo esc_html__('BusiCare Plus Configuration','busicare-plus'); ?><?php if( !empty($busicare['Version']) ): ?> <sup id="busicare-plus-theme-version"><?php echo esc_html( $busicare['Version'] ); ?> </sup><?php endif; ?></h1>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-12">			
			    <div class="busicare-plus-page">
			    	<div class="mockup">
			    		<img src="<?php echo BUSICAREP_PLUGIN_URL.'/inc/admin/assets/img/mockup-pro.png';?>"  width="100%">
			    	</div>				
				</div>	
			</div>	

		</div>

		<div class="row" style="margin-top: 20px;">			

			<div class="col-md-6">
				<div class="busicare-plus-page">
					<div class="busicare-plus-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'busicare-plus' ); ?></div>
					<div class="busicare-plus-page-content">
						<ul class="busicare-plus-page-list-flex">
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=title_tagline' ) ); ?>" target="_blank"><?php esc_html_e('Site Logo','busicare-plus'); ?></a>
							</li>
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=busicare_theme_panel' ) ); ?>" target="_blank"><?php esc_html_e('Blog options','busicare-plus'); ?></a>
							</li>
							 <li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=nav_menus' ) ); ?>" target="_blank"><?php esc_html_e('Menus','busicare-plus'); ?></a>
							</li>
							 <li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=theme_style' ) ); ?>" target="_blank"><?php esc_html_e('Layout & Color scheme','busicare-plus'); ?></a>
							</li>
							 <li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=widgets' ) ); ?>" target="_blank"><?php esc_html_e('Widgets','busicare-plus'); ?></a>
							</li>
							 <li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=busicare_general_settings' ) ); ?>" target="_blank"><?php esc_html_e('General settings','busicare-plus'); ?></a><?php esc_html_e(' ( Preloader, After Menu, Header Presets, Sticky Header, Container settings, Post Navigation Styles, Scroll to Top settings ) ','busicare-plus' ); ?>
							</li>
                                                        <li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=section_settings' ) ); ?>" target="_blank"><?php esc_html_e('Homepage sections','busicare-plus'); ?></a>
							</li>
                                                        <li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=busicare_template_settings' ) ); ?>" target="_blank"><?php esc_html_e('Page template settings','busicare-plus'); ?></a>
							</li>
			
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=busicare_typography_setting' ) ); ?>" target="_blank"><?php esc_html_e('Typography','busicare-plus'); ?></a>
							</li>
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'themes.php?page=BusiCare_plus_Hooks_Settings' ) ); ?>" target="_blank"><?php esc_html_e('Hooks','busicare-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=frontpage_layout' ) ); ?>" target="_blank"><?php esc_html_e('Sections order manager','busicare-plus'); ?></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="col-md-6"> 
				<div class="busicare-plus-page">
					<div class="busicare-plus-page-top busicare-plus-demo-import"><?php esc_html_e( 'One Click Demo Import', 'busicare-plus' ); ?></div>
					<p style="padding:10px 20px; font-size: 16px;"><?php _e( 'To import the demo data, you need to activate the <b>One Click Demo Import</b> and <b>BusiCare Demo Importer</b> plugins. <a class="busicare-plus-custom-class" href="#one_click_demo" target="_self">Click Here</a>', 'busicare-plus' ); ?></p>
				</div>
				<div class="busicare-plus-page">
					<div class="busicare-plus-page-top"><?php esc_html_e( 'Useful Links', 'busicare-plus' ); ?></div>
					<div class="busicare-plus-page-content">
						<ul class="busicare-plus-page-list-flex">
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo 'https://busicare-pro.spicethemes.com/'; ?>" target="_blank"><?php echo esc_html__('BusiCare Plus Demo','busicare-plus'); ?></a>
							</li>

							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo 'https://spicethemes.com/busicare-plus'; ?>" target="_blank"><?php echo esc_html__('BusiCare Plus Details','busicare-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo 'https://support.spicethemes.com/index.php?p=/categories/busicare-plus'; ?>" target="_blank"><?php echo esc_html__('BusiCare Plus Support','busicare-plus'); ?></a>
							</li>
							
						    <li class=""> 
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo 'https://wordpress.org/support/theme/busicare/reviews/#new-post'; ?>" target="_blank"><?php echo esc_html__('Your feedback is valuable to us','busicare-plus'); ?></a>
							</li>
							
							<li class=""> 
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo 'https://helpdoc.spicethemes.com/category/busicare-plus/'; ?>" target="_blank"><?php echo esc_html__('BusiCare Plus Documentation','busicare-plus'); ?></a>
							</li>
							
						    <li class=""> 
								<a class="busicare-plus-page-quick-setting-link" href="<?php echo 'https://spicethemes.com/contact'; ?>" target="_blank"><?php echo esc_html__('Pre-sales enquiry','busicare-plus'); ?></a>
							</li> 
							<li> 
								<a class="busicare-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/busicare-free-vs-pro/'); ?>" target="_blank"><?php echo esc_html__('Free vs Plus','busicare'); ?></a>
							</li> 

							<li> 
								<a class="busicare-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/busicare-plus-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','busicare'); ?></a>
							</li> 
						</ul>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>	