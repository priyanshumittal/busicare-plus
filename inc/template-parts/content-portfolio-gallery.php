<?php
$post_type = 'busicare_portfolio';
$tax = 'portfolio_categories';
$term_args = array('hide_empty' => true, 'orderby' => 'id');
$posts_per_page = get_theme_mod('portfolio_numbers_options', 4);
$tax_terms = get_terms($tax, $term_args);
$defualt_tex_id = get_option('busicare_default_term_id');
$j = 1;
if(isset($_GET['tab'])):
    $tab = $_GET['tab'];

endif;
if (isset($_GET['div'])) {
    $tab = $_GET['div'];
}
$porfolio_page_title = get_theme_mod('porfolio_page_title', __('Our Portfolio', 'busicare-plus'));
$porfolio_page_subtitle = get_theme_mod('porfolio_page_subtitle', __('Our Recent Works', 'busicare-plus'));
?>
<section class="section-space-page portfolio <?php if (is_page_template('template-portfolio-2-col-gallery.php') || is_page_template('template-portfolio-3-col-gallery.php') || is_page_template('template-portfolio-4-col-gallery.php')) { ?>portfolio-gallery<?php } ?>">
    <div class="container<?php echo busicare_container();?>">
        <?php if (!empty($porfolio_page_title) || !empty($porfolio_page_subtitle)): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if (!empty($porfolio_page_title)):?><h2 class="section-title"><?php echo $porfolio_page_title; ?></h2>
                        <div class="title_seprater"></div><?php endif;?>
                        <?php if (!empty($porfolio_page_subtitle)):?><h5 class="section-subtitle"><?php echo $porfolio_page_subtitle; ?></h5><?php endif;?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- /Portfolio Filter -->

        <div id="content" class="tab-content" role="tablist">
            <?php
            global $paged;
            $curpage = $paged ? $paged : 1;
            
                    $args = array(
                        'post_type' => $post_type,
                        'post_status' => 'publish',
                        'posts_per_page' => $posts_per_page,
                        'paged' => $curpage,
                        // 'orderby' => 'menu_order',
                    );
                    $portfolio_query = null;
                    $portfolio_query = new WP_Query($args);
                    if ($portfolio_query->have_posts()):
                        ?>
                       <div class="row">
                                <?php
                                while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
                                    $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                                    $portfolio_sub_title = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_sub_title', true));
                                    $portfolio_title_description = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_title_description', true));
                                    if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                        $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                                    } else {
                                        $portfolio_link = '';
                                    }
                                    $class = '';
                                    if (is_page_template('template-portfolio-2-col-gallery.php')) {
                                    $class = 'col-md-6 col-sm-6 col-xs-12 portfolio-gallery';
                                    }
                                    if (is_page_template('template-portfolio-3-col-gallery.php')) {
                                        $class = 'col-md-4 col-sm-4 col-xs-12 portfolio-gallery';
                                    }
                                    if (is_page_template('template-portfolio-4-col-gallery.php')) {
                                        $class = 'col-md-3 col-sm-3 col-xs-12 portfolio-gallery';
                                    }
                                    if (is_page_template('template-portfolio-2-col-non-filter.php')) {
                                        $class = 'col-md-6 col-sm-6 col-xs-12';
                                    }
                                    if (is_page_template('template-portfolio-3-col-non-filter.php')) {
                                        $class = 'col-md-4 col-sm-4 col-xs-12';
                                    }
                                    if (is_page_template('template-portfolio-4-col-non-filter.php')) {
                                        $class = 'col-md-3 col-sm-3 col-xs-12';
                                    }
                                    echo '<div class="' . $class . '">';
                                    ?>
                                    <article class="post">
                                        <figure class="portfolio-thumbnail">
                                            <?php
                                            the_post_thumbnail('full', array('class' => 'img-fluid'));
                                            if (has_post_thumbnail()) {
                                                $post_thumbnail_id = get_post_thumbnail_id();
                                                $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                            }
                                            ?>
                                            <div class="click-view">
                                                <div class="view-content">

                                                    <a href="<?php echo $post_thumbnail_url; ?>" data-lightbox="image" title="<?php echo the_title(); ?>" class="mr-2"><i class="fa-solid fa-image"></i></a>
                                                    <?php if (!empty($portfolio_link)) { ?>
                                                        <a href="<?php echo $portfolio_link; ?>" <?php
                                                           if (!empty($portfolio_target)) {
                                                               echo 'target="_blank"';
                                                           }
                                                           ?> ><i class="fa-solid fa-link"></i></a>
                <?php } ?>                                             

                                                </div>
                                            </div>
                                        </figure>   
                                        <figcaption>
                                            <h4 class="portfolio_title"><?php echo the_title(); ?></h4>
                                        </figcaption>
                                    </article>
                                <?php echo '</div>'; ?>
                                <?php $norecord = 1; ?>
                            <?php endwhile; ?>
                            </div>
                            <div class="row justify-content-center">
                            <?php
                            $total = $portfolio_query->found_posts;
                            $spicethemes_pagination = new spicethemes_pagination();
                            $spicethemes_pagination->Webriti_page($curpage, $portfolio_query, $total, $posts_per_page);
                            ?>
                            </div>
                        </div>
                             <?php
                             wp_reset_query();
                            endif;?>  
     </div>      
</section>
<script type="text/javascript">
    jQuery('.lightbox').hide();
    jQuery('#lightbox').hide();
    jQuery(".tab .nav-link ").click(function (e) {
        jQuery("#lightbox").remove();
        var h = decodeURI(jQuery(this).attr('href').replace(/#/, ""));
        var tjk = "<?php the_title(); ?>";
        var str1 = tjk.replace(/\s+/g, '-').toLowerCase();
        var pageurl = "<?php
$structure = get_option('permalink_structure');
if ($structure == '') {
    echo get_permalink() . "&tab=";
} else {
    echo get_permalink() . "?tab=";
}
?>" + h;
        jQuery.ajax({url: pageurl, beforeSend: function () {
                jQuery(".tab-content").hide();
                jQuery("#myDiv").show();
            }, success: function (data) {
                jQuery(".tab-content").show();
                jQuery('.lightbox').remove();
                jQuery('#lightbox').remove();
                jQuery('#wrapper').html(data);
            }, complete: function (data) {
                jQuery("#myDiv").hide();
            }
        });
        if (pageurl != window.location) {
            window.history.pushState({path: pageurl}, '', pageurl);
        }
        return false;
    });
</script>
<?php
get_footer();
