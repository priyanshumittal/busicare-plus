<article  id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>	
	<?php if(has_post_thumbnail()):?>
		<figure class="post-thumbnail mb-4">
			<a href="<?php the_permalink();?>">
				<?php the_post_thumbnail('full',array('class'=>'img-fluid'));?>
			</a>			
		</figure>	
	<?php endif;?>
    <?php if(get_theme_mod('busicare_enable_blog_date',true) || get_theme_mod('busicare_enable_blog_author',true) || get_theme_mod('busicare_enable_blog_category',true)): ?>
	<div class="post-content">
		<?php
		if(get_theme_mod('busicare_enable_single_post_date',true)===true):?>
		<div class="entry-date <?php if (!has_post_thumbnail()) {echo 'remove-image';} ?>">
			<a href="<?php echo esc_url( home_url() ); ?>/<?php echo esc_html(date( 'Y/m' , strtotime( get_the_date() )) ); ?>"><time><?php echo get_the_date();?></time></a>
		</div>
		<?php endif;?>
    <?php if(get_theme_mod('busicare_enable_blog_author',true) || get_theme_mod('busicare_enable_blog_category',true)): ?>
	<div class="entry-meta">
		<?php
		if(get_theme_mod('busicare_enable_single_post_admin',true)===true):?>
		<span class="author"><?php _e('By','busicare-plus');?>
			<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"> <?php echo get_the_author();?></a>
		</span>
		<?php 
	if(get_theme_mod('busicare_enable_single_post_category',true)==true):
	if(has_category()):
		echo '<span class="cat-links"> in ';
		the_category( ', ');
		echo '</span>';		
	endif; endif;?>	
		<?php
	endif;?>		
	</div>
        <?php endif;?>
        </div>
    <?php endif;?>
	<header class="entry-header">
		<h3 class="entry-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	</header>

	<div class="entry-content">
		<?php the_excerpt(); wp_link_pages( );busicare_button_title(); ?>
	</div>										
</article>