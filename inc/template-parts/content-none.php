<article  id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
	<header class="entry-header">
		<h3 class="entry-title"><?php esc_html_e('Nothing found', 'busicare-plus'); ?></a></h3>
		<p><?php esc_html_e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.','busicare-plus'); ?></p>
	</header>
</article>