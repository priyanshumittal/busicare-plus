<?php
/*
Plugin Name: BusiCare Plus
Description: Enhances BusiCare theme with extra functionality.
Version: 1.3.3
Author: spicethemes
Author URI: https://spicethemes.com
Text Domain: busicare-plus
*/

//added freemius code snippet
if ( ! function_exists( 'bp_fs' ) ) {
    // Create a helper function for easy SDK access.
    function bp_fs() {
        global $bp_fs;

        if ( ! isset( $bp_fs ) ) {
            // Include Freemius SDK.
            require_once dirname(__FILE__) . '/freemius/start.php';

            $bp_fs = fs_dynamic_init( array(
                'id'                  => '10312',
                'slug'                => 'busicare',
                'premium_slug'        => 'busicare-plus',
                'type'                => 'plugin',
                'public_key'          => 'pk_c4c59bdae596d987277f058ef93fd',
                'is_premium'          => true,
				'is_premium_only'          => true,
                //'premium_suffix'      => 'BusiCare Plus Plugin',
                // If your plugin is a serviceware, set this option to false.
                //'has_premium_version' => true,
                'has_addons'          => false,
                'has_paid_plans'      => true,
                 'menu'                => array(
                    'slug'           => 'busicare-plus-welcome',
                    'support'        => false,
                    'parent'         => array(
                        'slug' => 'themes.php',
                    ),
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                'secret_key'          => 'undefined',
            ) );
        }

        return $bp_fs;
    }

    // Init Freemius.
    bp_fs();
    // Signal that SDK was initiated.
    do_action( 'bp_fs_loaded' );
}

define( 'BUSICAREP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'BUSICAREP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once('inc/widgets/wdl_social_icon.php');
require_once('inc/widgets/wdl_topbar_info.php');
require_once('inc/widgets/wdl_address_info.php');
require_once('inc/widgets/add_sidebar.php');

class PageTemplater {

	/**
	 * A reference to an instance of this class.
	 */
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	 */
	protected $templates;

	/**
	 * Returns an instance of this class.
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new PageTemplater();
		}

		return self::$instance;

	}

	/**
	 * Initializes the plugin by setting filters and administration functions.
	 */
	private function __construct() {

		$this->templates = array();


		// Add a filter to the attributes metabox to inject template into the cache.
		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

			// 4.6 and older
			add_filter(
				'page_attributes_dropdown_pages_args',
				array( $this, 'register_project_templates' )
			);

		} else {

			// Add a filter to the wp 4.7 version attributes metabox
			add_filter(
				'theme_page_templates', array( $this, 'add_new_template' )
			);

		}

		// Add a filter to the save post to inject out template into the page cache
		add_filter(
			'wp_insert_post_data',
			array( $this, 'register_project_templates' )
		);


		// Add a filter to the template include to determine if the page has our
		// template assigned and return it's path
		add_filter(
			'template_include',
			array( $this, 'view_project_template')
		);


		// Add your templates to this array.
		$this->templates = array(
			'template-aboutus.php' => 'About Us',
			//Add template Blog
			'template-blog-full-width.php' => 'Blog Full Width',
			'template-blog-grid-view-sidebar.php' => 'Blog grid view sidebar',
			'template-blog-grid-view.php' => 'Blog grid view',
			'template-blog-left-sidebar.php' => 'Blog left sidebar',
			'template-blog-list-view-sidebar.php' => 'Blog list view sidebar',
			'template-blog-list-view.php' => 'Blog list view',
			'template-blog-masonry-four-column.php' => 'Blog Masonry 4 Column',
			'template-blog-masonry-three-column.php' => 'Blog Masonry 3 Column',
			'template-blog-masonry-two-column.php' => 'Blog Masonry 2 Column',
			'template-blog-right-sidebar.php' => 'Blog right sidebar',
			//Add template Conatct
			'template-contact-us.php' => 'Contact',
			'template-contact-us-2.php' => 'Contact Two',
			'template-contact-us-3.php' => 'Contact Three',
			'template-contact-us-4.php' => 'Contact Four',
			'template-contact-us-5.php' => 'Contact Five',
			//Add template Portfolio
			'template-portfolio-2-col-gallery.php' => 'Two Column Gallery Style Portfolio',
			'template-portfolio-2-col-left-sidebar.php' => 'Two Column Left Sidebar Portfolio',
			'template-portfolio-2-col-non-filter.php' => 'Two Column Without Filter Portfolio',
			'template-portfolio-2-col-right-sidebar.php' => 'Two Column Right Sidebar Portfolio',
			'template-portfolio-2-col.php' => 'Portfolio 2 Columns',

			'template-portfolio-3-col-gallery.php' => 'Three Column Gallery Style Portfolio',
			'template-portfolio-3-col-left-sidebar.php' => 'Three Column Left Sidebar Portfolio',
			'template-portfolio-3-col-non-filter.php' => 'Three Column Without Filter Portfolio',
			'template-portfolio-3-col-right-sidebar.php' => 'Three Column Right Sidebar Portfolio',
			'template-portfolio-3-col.php' => 'Portfolio 3 Columns',

			'template-portfolio-4-col-gallery.php' => 'Four Column Gallery Style Portfolio',
			'template-portfolio-4-col-left-sidebar.php' => 'Four Column Left Sidebar Portfolio',
			'template-portfolio-4-col-non-filter.php' => 'Four Column Without Filter Portfolio',
			'template-portfolio-4-col-right-sidebar.php' => 'Four Column Right Sidebar Portfolio',
			'template-portfolio-4-col.php' => 'Portfolio 4 Columns',


			//Add template Services
			'template-service.php' => 'Service',
			'template-service-2.php' => 'Service Two',
			'template-service-3.php' => 'Service Three',
			'template-service-4.php' => 'Service Four',
			//Add template Team
			'template-team-content-1.php' => 'Team Carousel Style One',
			'template-team-content-2.php' => 'Team Carousel Style Two',
			'template-team-content-3.php' => 'Team Carousel Style Three',
			'template-team-content-4.php' => 'Team Carousel Style Four',
			'template-team-content-5.php' => 'Team Grid Style One',
			'template-team-content-6.php' => 'Team Grid Style Two',
			'template-team-content-7.php' => 'Team Grid Style Three',
			'template-team-content-8.php' => 'Team Grid Style Four',
			//Add template Testiomonial
			'template-testimonial-1.php' => 'Testimonial Carousel Style One',
			'template-testimonial-2.php' => 'Testimonial Carousel Style Two',
			'template-testimonial-3.php' => 'Testimonial Carousel Style Three',
			'template-testimonial-4.php' => 'Testimonial Grid Style One',
			'template-testimonial-5.php' => 'Testimonial Grid Style Two',
			'template-testimonial-6.php' => 'Testimonial Grid Style Three',


		);

	}

	/**
	 * Adds our template to the page dropdown for v4.7+
	 *
	 */
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doens't really exist.
	 */
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list.
		// If it doesn't exist, or it's empty prepare an array
		$templates = wp_get_theme()->get_page_templates();
		if ( empty( $templates ) ) {
			$templates = array();
		}

		// New cache, therefore remove the old one
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	}

	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_project_template( $template ) {
		// Return the search template if we're searching (instead of the template for the first result)
		if ( is_search() ) {
			return $template;
		}

		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( ! isset( $this->templates[get_post_meta(
			$post->ID, '_wp_page_template', true
		)] ) ) {
			return $template;
		}

		// Allows filtering of file path
		$filepath = apply_filters( 'page_templater_plugin_dir_path', plugin_dir_path( __FILE__ ).'/inc/template/' );

		$file =  $filepath . get_post_meta(
			$post->ID, '_wp_page_template', true
		);

		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

}
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );

function busicarep_activate() {
	$theme = wp_get_theme(); // gets the current theme

	// checking for innofit theme
	if ( 'BusiCare' == $theme->name || 'BusiCare Child' == $theme->name || 'BusiCare Dark' == $theme->name){
		//load innofit plus functions

        require_once('inc/functions/functions.php');
		//registers innofit plus scripts
		require_once('inc/functions/scripts/script.php');
		require_once('inc/inc/custom-header.php');

		register_nav_menus( array(

		'footer_menu' => esc_html__( 'Footer Menu', 'busicare-plus' ),

	) );

	/**
	* Load the localisation file.
	*/

		//load_plugin_textdomain( 'busicare-plus', false, BUSICAREP_PLUGIN_DIR . '/languages/' );

	//About Theme
		if ( is_admin() )
        {
            require BUSICAREP_PLUGIN_DIR . '/inc/admin/admin-init.php';
        }

	}

}
add_action( 'init', 'busicarep_activate' );

$theme = wp_get_theme();

// Checking for innofit theme
if ( 'BusiCare' == $theme->name || 'BusiCare Child' == $theme->name || 'BusiCare Dark' == $theme->name){

	//including admin files
	require_once('inc/functions/admin/functions.php');
	register_activation_hook( __FILE__, 'busicarep_install_function');
	function busicarep_install_function()
	{

	    // set defulat data on plugin activation
        $item_details_page = get_option('item_details_page');
		if(!$item_details_page){
		require_once('inc/default-pages/upload-media.php');
		require_once('inc/default-pages/home-page.php');
		require_once('inc/default-widgets/default-widget.php');

		update_option( 'item_details_page', 'Done' );
		}

		function busicare_plus_activation_redirect( $plugin ) {
            if( $plugin == plugin_basename( __FILE__ ) ) {
                exit( wp_redirect( admin_url( 'themes.php?page=busicare-plus-welcome' ) ) );
            }
        }
        add_action( 'activated_plugin', 'busicare_plus_activation_redirect' );

    }

    function busicare_plus_file_replace(){

	    $plugin_dir = plugin_dir_path( __FILE__ ) . 'inc/template/taxonomy-portfolio_categories.php';
	    $theme_dir = get_stylesheet_directory() . '/taxonomy-portfolio_categories.php';

	    $plugin_dir_xml = plugin_dir_path( __FILE__ ) . 'wpml-config.xml';
	    $theme_dir_xml = get_stylesheet_directory() . '/wpml-config.xml';

	    if (!copy($plugin_dir, $theme_dir)) {
	        echo "failed to copy $plugin_dir to $theme_dir...\n";
	    }
	    if (!copy($plugin_dir_xml, $theme_dir_xml)) {
	        echo "failed to copy $plugin_dir to $theme_dir_xml...\n";
	    }
	}
	
	add_action( 'wp_head', 'busicare_plus_file_replace' );

}